<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoliHari extends Model
{
  protected $table = 'poli_hari';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function hari()
  {
      return $this->belongsTo('App\Hari', 'hari_id', 'id');
  }
  public function poli()
  {
      return $this->belongsTo('App\Poli', 'poli_id', 'id');
  }
  public function sesi()
  {
      return $this->hasMany('App\Sesi', 'poli_hari_id', 'id');
  }
}
