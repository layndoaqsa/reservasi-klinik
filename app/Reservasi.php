<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
  protected $table = 'reservasi';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function calonPasien()
  {
      return $this->belongsTo('App\CalonPasien', 'pasien_id', 'id');
  }

  public function jamSesi()
  {
      return $this->belongsTo('App\JamSesi', 'waktu', 'id');
  }

  public function poli()
  {
      return $this->belongsTo('App\Poli', 'poli_id', 'id');
  }

  public function updatedBy()
  {
      return $this->belongsTo('App\User', 'status_updated_by', 'id');
  }

  public function jawabanCalonPasien()
  {
      return $this->hasMany('App\jawabanCalonPasien', 'reservasi_id', 'id');
  }

  public function next(){
      return $this->where('antrian', '>', $this->antrian)->where('counter', '>', $this->counter)->orderBy('id','asc')->first();
  }

  public function previous(){
      return $this->where('id', '<', $this->id)->orderBy('id','desc')->first();
  }

  public function screening()
  {
      return $this->hasOne('App\HasilScreening', 'reservasi_id', 'id');
  }
}
