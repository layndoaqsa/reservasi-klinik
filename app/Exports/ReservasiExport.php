<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;

class ReservasiExport implements FromCollection, WithHeadings, WithEvents
{
    private $tanggal, $poli;

    public function __construct($data, $tanggal, $poli)
    {
        $this->data = $data;
        $this->tanggal = $tanggal;
        $this->poli = $poli;
    }
    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
          'Pasien',
          'BPJS',
          'Poli',
          'Dokter',
          'Status',
        ];
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function(BeforeSheet $event){
              $event->sheet->setCellValue('A1', 'LAPORAN RESERVASI');
              $event->sheet->setCellValue('A2', 'PUSKESMAS KALIBAWANG');
              $event->sheet->setCellValue('A4', 'Poli:');
              $event->sheet->setCellValue('B4', $this->poli);
              $event->sheet->setCellValue('A5', 'Tanggal:');
              $event->sheet->setCellValue('B5', $this->tanggal);
              $event->sheet->mergeCells('A1:E1');
              $event->sheet->mergeCells('A2:E2');
              $event->sheet->getStyle('A1:A2')->applyFromArray([
                  'font' => [
                      'bold' => true,
                      'size' => 16
                  ],
                  'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ]
              ]);
              $event->sheet->getStyle('A4:A5')->applyFromArray([
                  'font' => [
                      'bold' => true
                  ]
              ]);
            },
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A6:E6')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $fillData = 6 + $this->data->count();
                $event->sheet->getStyle('A6:E'.$fillData)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ]);
            },
        ];
    }
}
