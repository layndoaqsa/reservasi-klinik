<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poli extends Model
{
  protected $table = 'poli';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function poliHari()
  {
    return $this->hasMany('App\PoliHari', 'poli_id', 'id');
  }
}
