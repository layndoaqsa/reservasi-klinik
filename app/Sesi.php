<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesi extends Model
{
  protected $table = 'sesi';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function poliHari()
  {
      return $this->belongsTo('App\PoliHari', 'poli_hari_id', 'id');
  }

  public function dokterSesi()
  {
      return $this->hasMany('App\DokterSesi', 'sesi_id', 'id');
  }
}
