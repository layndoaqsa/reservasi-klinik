<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalonPasien extends Model
{
  protected $table = 'calon_pasien';
  protected $guarded = ['created_at','updated_at'];
  protected $fillable = ['no_telepon','nama','nik','no_bpjs','tipe','created_by','created_at','updated_at'];
  public $timestamps = true;
}
