<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilSkor extends Model
{
  protected $table = 'hasil_skors';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;
}
