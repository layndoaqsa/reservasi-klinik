<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JamSesi extends Model
{
  protected $table = 'jam_sesi';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function jam()
  {
      return $this->belongsTo('App\Jam', 'jam_id', 'id');
  }
  public function sesi()
  {
      return $this->belongsTo('App\Sesi', 'sesi_id', 'id');
  }
}
