<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilScreening extends Model
{
  protected $table = 'hasil_screening';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function hasilSkor(){
      return $this->belongsTo('App\HasilSkor', 'hasil_skors_id', 'id');
  }
}
