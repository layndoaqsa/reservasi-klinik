<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use App\CalonPasien;
use App\Poli;
use App\Reservasi;
use App\DokterSesi;
use DB;
use Auth;
use Datatables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }

    public function getData(Request $request)
    {
        $data = User::whereNotIn('id',[Auth::user()->id])->get();
        return datatables()->of($data)->addColumn('action', function($row){
            $btn = '<a href="'.route('user.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::where('name','!=','Calon Pasien')->get();
        $poli = Poli::all();
        return view('user.create', compact('role','poli'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),
        [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'role' => 'required',
        ]
        );

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $user->poli_id = $request->poli;
        $user->save();
        $user->roles()->sync($request->role);

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::find($id);
        if (Auth::user()->hasAnyRole(['Super Admin','Dokter','Layanan Reservasi'])) {
          return view('user.profile',compact('data'));
        } elseif (Auth::user()->hasRole('Calon Pasien')) {
          return view('pasien.profile',compact('data'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        $poli = Poli::all();
        $role = Role::where('name','!=','Calon Pasien')->get();
        return view('user.edit',compact('data','role','poli'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(),
        [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'confirmed',
            'role' => 'required',
            'poli' => 'required',
        ]
        );

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        if (Role::find($request->role)->name == 'Super Admin') {
            $poli = NULL;
        } else {
            $poli = $request->poli;
        }
        $user->poli_id = $poli;
        if ($request->password) {
        $user->password = \Hash::make($request->password);
        }
        $user->save();
        $user->roles()->sync($request->role);
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $cek_reservasi = Reservasi::with('calonPasien')
                        ->whereHas('calonPasien', function ($query) use ($id){
                            $query->where('created_by', $id);
                        })->first();
        $cek_dokter_sesi = DokterSesi::where('dokter_id',$id)->first();
        if (!empty($cek_reservasi) || !empty($cek_dokter_sesi)) {
          return response()->json(['status' => 'failed','message' => 'Data terkait dengan data lain.'],200);
        }
        $poli->delete();
        $user->delete();
        return response()->json(['status' => 'success', 'data'=>'success delete data']);
    }

    public function updateProfil(Request $request, $id)
    {
        $this->validate(request(),
        [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id,
        ]
        );

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        return redirect()->route('user.show',$id);
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate(request(),
        [
            'password' => 'confirmed'
        ]
        );

        $user = User::find($id);
        if ($request->password) {
        $user->password = \Hash::make($request->password);
        }
        $user->save();
        return redirect()->route('user.show',$id);
    }


    // START OF API


    public function api_register(Request $request)
    {
     $request->validate([
       'nama' => 'required|string|max:20',
       'email' => 'required|string|email|unique:users|max:50',
       'password' => 'required|string|max:191',
     ]);
     DB::beginTransaction();
     $user = User::create([
       'name'=>$request->nama,
       'email'=>$request->email,
       'password'=>bcrypt($request->password),
     ])->assignRole('Calon Pasien');
     $calonPasien = CalonPasien::create([
       'nama' => $user->name,
       'no_bpjs' => $request->nomor_bpjs_atau_kartu_sehat,
       'tipe' => 'root',
       'created_by' => $user->id
     ]);
     DB::commit();
     $user = User::where('id', $user->id)->first();
     return response()->json([
       'status'=>'success',
       'user'=>$user
     ]);
    }

    public function api_login(Request $request){
      if(Auth::attempt([
        'email' => request('email'),
        'password' => request('password'),

      ]))
      {
          $user = Auth::user();
          if ($user->hasRole('Calon Pasien')) {
            $email = $request->get('email');
            $password = $request->get('password');

            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['email'] = $email;
            $success['password'] = $password;

            $pasien = User::where('email', $success['email'])->first();
            $pasien->token = $success['token'];
            // $pasien->foto= asset('images/'.$pasien->foto.'');

            return response()->json([
                'status'=>'success',
                'user' => $pasien
            ]);
          }
          else {
            $success['status'] = 'failed';
            $success['error'] = 'Unauthorized';
            $success['message'] = 'Email yang anda masukkan salah.';
            return response()->json($success,401);
          }
      }
      else{
          $success['status'] = 'failed';
          $success['error'] = 'Unauthorized';
          $success['message'] = 'Email atau password salah.';
          return response()->json($success,401);
      }
    }
    public function api_logout(Request $request)
    {
       $request->user()->token()->revoke();
       return response()->json([
         'message' => 'Berhasil Keluar'
       ]);
    }
}
