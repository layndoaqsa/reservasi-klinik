<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warga;
use App\Pertanyaan;
use App\Models\Province;
use App\JawabanWarga;
use App\HasilSkor;
use Carbon\Carbon;
use DB;

class WargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($telepon)
    {
        $warga = Warga::where('no_telepon',$telepon)->first();
        return view('warga.list', compact('warga'));
    }

    public function getData(Request $request)
    {
        $warga = Warga::where('no_telepon',$request->no_telepon)->first();
        $data = Warga::where('created_by',$warga->created_by)->get();
        return datatables()->of($data)->addColumn('action', function($row){
            $btn = '<a href="'.route('warga.lapor',$row->id).'" class="btn border-success btn-xs text-success-600 btn-flat btn-icon"><i class="icon-pencil5"></i> Laporan Harian</a>';
            $btn = $btn.'  <a href="'.route('warga.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            return $btn;
        })
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($telepon)
    {
      $provinsi = Province::all();
      $warga = Warga::where('no_telepon',$telepon)->first();
      return view('warga.create', compact('warga','provinsi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validate(request(),
        ['no_telepon' => 'required|between:10,12']);

        $warga = Warga::where('no_telepon',$request->no_telepon)->first();;
        if (!empty($warga)) {
          if (!empty($warga->nik)) {
            return redirect('/list/'.$warga->no_telepon);
          } else {
            return view('warga.isi-data', compact('warga'));
          }
        } else {
          $warga = Warga::create([
            'no_telepon' => request('no_telepon')
          ]);
          $warga->created_by = $warga->id;
          $warga->save();

          return redirect()->route('warga.isiData',$warga->no_telepon);
        }
    }

    public function store($telepon)
    {
        $pendaftar = Warga::where('no_telepon',$telepon)->first();
        $this->validate(request(),
        [
            'nomor_telepon' => 'required|between:10,12',
            'nik' => 'required|digits:16',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'provinsi' => 'required',
            'kota_kabupaten' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'status_kependudukan' => 'required',
        ]);
        $warga = Warga::create([
          'no_telepon' => request('nomor_telepon'),
          'nik' => request('nik'),
          'nama' => request('nama'),
          'jenis_kelamin' => request('jenis_kelamin'),
          'province_id' => request('provinsi'),
          'regency_id' => request('kota_kabupaten'),
          'district_id' => request('kecamatan'),
          'village_id' => request('kelurahan'),
          'status_kependudukan' => request('status_kependudukan'),
          'created_by' => $pendaftar->created_by,
        ]);
        return redirect('/list/'.$telepon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function isiData($telepon)
    {
      $warga = Warga::where('no_telepon',$telepon)->first();
      $provinsi = Province::all();
      if (!empty($warga->nik)) {
        return redirect('/list/'.$warga->no_telepon);
      } else {
        return view('warga.isi-data', compact('warga','provinsi'));
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(),
      [
          'nama' => 'required',
          'nik' => 'required|digits:16',
          'jenis_kelamin' => 'required',
          'provinsi' => 'required',
          'kota_kabupaten' => 'required',
          'kecamatan' => 'required',
          'kelurahan' => 'required',
          'status_kependudukan' => 'required',
      ]
      );
      $warga = Warga::where('no_telepon',$id)->first();
      $warga->nama = $request->nama;
      $warga->nik = $request->nik;
      $warga->jenis_kelamin = $request->jenis_kelamin;
      $warga->province_id = $request->provinsi;
      $warga->regency_id = $request->kota_kabupaten;
      $warga->district_id = $request->kecamatan;
      $warga->village_id = $request->kelurahan;
      $warga->status_kependudukan = $request->status_kependudukan;
      $warga->save();
      return redirect('/list/'.$warga->no_telepon);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $warga = Warga::find($id)->delete();
         return response()->json(['data'=>'success delete data']);
     }

     public function lapor($id)
     {
         $warga = Warga::find($id);
         $jawabanToday = JawabanWarga::where('warga_id',$id)
                                ->whereDate('created_at', Carbon::today())
                                ->first();
         if (!empty($jawabanToday)) {
            return redirect('laporan/detail/'.$warga->id);
         }
         if ($warga->created_by) {
            $trigTel = Warga::find($warga->created_by)->no_telepon;
         } else {
            $trigTel = $warga->no_telepon;
         }
         $pertanyaan = Pertanyaan::all()->sortBy('id');
         return view('warga.lapor', compact('warga','pertanyaan','trigTel'));
     }

     public function laporStore(Request $request, $id)
     {
         $warga = Warga::find($id);
         $array = array_values($request->all());
         for ($i=0; $i < sizeof($array[1]) ; $i++) {
             $jawaban = new JawabanWarga;
             $jawaban->warga_id = $warga->id;
             $jawaban->pertanyaan_id = $array[1][$i];
             $jawaban->jawaban = $array[2][$i];
             $pertanyaan = Pertanyaan::find($array[1][0]);
             if ($array[2][$i] == 'ya') {
               $jawaban->skor = $pertanyaan->skor_ya;
             } else {
               $jawaban->skor = $pertanyaan->skor_tidak;
             }
             $jawaban->save();
         }
         return redirect('laporan/detail/'.$warga->id);
     }

     public function laporDetail($id)
     {
         $warga = Warga::find($id);
         $skor = JawabanWarga::where('warga_id',$id)
                                       ->whereDate('created_at', Carbon::today())
                                       ->select([DB::raw('sum(skor) as skor'),DB::raw('DATE(created_at) as day')])
                                       ->groupBy('day')
                                       ->first();
         $hasil = HasilSkor::where('batas_bawah','<=',$skor->skor)
                            ->where('batas_atas','>=',$skor->skor)
                            ->first();
         if (!$hasil) {
           $hasil = '(*terjadi kesalahan*)';
         } else {
           $hasil = $hasil->hasil;
         }

         return view('warga.lapor-detail', compact('warga','skor','hasil'));
     }

     public function laporEdit($id)
     {
         $warga = Warga::find($id);
         $pertanyaan = Pertanyaan::all()->sortBy('id');
         $jawaban = JawabanWarga::where('warga_id',$id)->whereDate('created_at', Carbon::today())->get()->sortBy('pertanyaan_id');
         // dd($jawaban,$pertanyaan);
         return view('warga.lapor-edit', compact('warga','pertanyaan','jawaban'));
     }

     public function laporUpdate(Request $request, $id)
     {
         $warga = Warga::find($id);
         $array = array_values($request->all());
         for ($i=0; $i < sizeof($array[1]) ; $i++) {
             $jawaban = JawabanWarga::where('pertanyaan_id',$array[1][$i])->whereDate('created_at', Carbon::today())->get()->sortBy('pertanyaan_id')->first();
             $jawaban->pertanyaan_id = $array[1][$i];
             $jawaban->jawaban = $array[2][$i];
             $pertanyaan = Pertanyaan::find($array[1][0]);
             if ($array[2][$i] == 'ya') {
               $jawaban->skor = $pertanyaan->skor_ya;
             } else {
               $jawaban->skor = $pertanyaan->skor_tidak;
             }
             $jawaban->save();
         }
         return redirect('laporan/detail/'.$warga->id);
     }

     public function edit($id)
     {
         $provinsi = Province::all();
         $warga = Warga::find($id);
         if ($warga->created_by) {
            $trigTel = Warga::find($warga->created_by)->no_telepon;
         } else {
            $trigTel = $warga->no_telepon;
         }
         return view('warga.edit', compact('warga','trigTel','provinsi'));
     }
     public function updateWarga(Request $request, $id)
     {
       $this->validate(request(),
       [
           'nomor_telepon' => 'required|between:10,12',
           'nik' => 'required|digits:16',
           'nama' => 'required',
           'jenis_kelamin' => 'required',
           'provinsi' => 'required',
           'kota_kabupaten' => 'required',
           'kecamatan' => 'required',
           'kelurahan' => 'required',
           'status_kependudukan' => 'required',
       ]
       );
       $warga = Warga::find($id);
       $warga->no_telepon = $request->nomor_telepon;
       $warga->nik = $request->nik;
       $warga->nama = $request->nama;
       $warga->jenis_kelamin = $request->jenis_kelamin;
       $warga->province_id = $request->provinsi;
       $warga->regency_id = $request->kota_kabupaten;
       $warga->district_id = $request->kecamatan;
       $warga->village_id = $request->kelurahan;
       $warga->status_kependudukan = $request->status_kependudukan;
       $warga->save();
       if (!empty($warga->created_by)) {
          $trigTel = Warga::find($warga->created_by)->no_telepon;
          return redirect('/list/'.$trigTel);
       } else {
          return redirect('/list/'.$warga->no_telepon);
       }
     }

     public function chart($id)
      {
        $chart = JawabanWarga::where('warga_id',$id)
                              ->leftJoin('wargas','jawabanwargas.warga_id','=','wargas.id')
                              ->select([DB::raw('sum(skor) as skor'),DB::raw('DATE(jawabanwargas.created_at) as day'),DB::raw('wargas.nama')])
                              ->groupBy('day', 'wargas.nama')
                              ->get();
        $result = [];
        $result[] = $chart[0]->nama;
        foreach ($chart as $key => $value) {
            $result[$key+1] = $value->skor;
        }
        $result = [array_values($result)];
        foreach ($result as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if ($key1 == 0) {
                    $number[] = "x";
                } else {
                    $number[] = $key1;
                }
            }
        }
        $number = array_combine($number,$number);
        $number = array_values($number);
        array_unshift($result , $number);
        return response()->json($result);
      }
}
