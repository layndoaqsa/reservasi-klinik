<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservasi;

class LaporanController extends Controller
{
    public function index()
    {
        $reservasi = Reservasi::where('tanggal',date('Y-m-d'))->where('status','Reservasi')->count();
        $tertangani = Reservasi::where('tanggal',date('Y-m-d'))->where('status','Selesai Pemeriksaan')->count();
        $batal = Reservasi::where('tanggal',date('Y-m-d'))->where('status','Batal')->count();
        $terindikasi = Reservasi::whereDate('tanggal',date('Y-m-d'))->whereHas('screening.hasilSkor', function($query){
            $query->whereIn('kategori_hasil',['odp','pdp']);
        })->count();
        return view('laporan.index',compact('reservasi','tertangani','batal','terindikasi'));
    }

    public function cari(Request $request)
    {
        $tanggal = date('Y-m-d', strtotime($request->tanggal));
        $tipe = $request->tipe;
        $poli = $request->poli;

        switch ($tipe) {
            case 'all':
                $reservasi = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Reservasi')->count();
                $tertangani = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Selesai Pemeriksaan')->count();
                $batal = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Batal')->count();
                $terindikasi = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->whereHas('screening.hasilSkor', function($query){
                    $query->whereIn('kategori_hasil',['odp','pdp']);
                })->count();
                break;
            case 'bpjs':
                $reservasi = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Reservasi')->whereHas('calonPasien', function($query){
                    $query->whereNotNull('no_bpjs');
                })->count();
                $tertangani = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Selesai Pemeriksaan')->whereHas('calonPasien', function($query){
                    $query->whereNotNull('no_bpjs');
                })->count();
                $batal = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Batal')->whereHas('calonPasien', function($query){
                    $query->whereNotNull('no_bpjs');
                })->count();
                $terindikasi = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->whereHas('screening.hasilSkor', function($query){
                    $query->whereIn('kategori_hasil',['odp','pdp']);
                })->whereHas('calonPasien', function($query){
                    $query->whereNotNull('no_bpjs');
                })->count();
                break;
            case 'non-bpjs':
                $reservasi = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Reservasi')->whereHas('calonPasien', function($query){
                    $query->whereNull('no_bpjs');
                })->count();
                $tertangani = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Selesai Pemeriksaan')->whereHas('calonPasien', function($query){
                    $query->whereNull('no_bpjs');
                })->count();
                $batal = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->where('status','Batal')->whereHas('calonPasien', function($query){
                    $query->whereNull('no_bpjs');
                })->count();
                $terindikasi = Reservasi::whereDate('tanggal',$tanggal)->whereHas('poli', function($query) use($poli){
                  if (!empty($poli)) {
                    $query->where('id',$poli);
                  }
                })->whereHas('screening.hasilSkor', function($query){
                    $query->whereIn('kategori_hasil',['odp','pdp']);
                })->whereHas('calonPasien', function($query){
                    $query->whereNull('no_bpjs');
                })->count();
                break;
        }

        return response()->json(['reservasi'=>$reservasi,'tertangani'=>$tertangani,'terindikasi'=>$terindikasi,'batal'=>$batal]);
    }
}
