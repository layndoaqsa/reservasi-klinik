<?php

namespace App\Http\Controllers;

use App\Events\InfoAntrianPoliEvent;
use App\Events\PanggilAntrianEvent;
use App\Events\InfoAntrianEvent;
use App\Events\DisplayAntrianEvent;

use Illuminate\Http\Request;
use App\Reservasi;
use App\CalonPasien;
use App\Poli;
use App\Hari;
use App\PoliHari;
use App\Sesi;
use Auth;
use DB;
use Redirect;

class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layanan.index');
    }
    public function pasienLama()
    {
        return view('layanan.pasien-lama');
    }
    public function pasienBaru()
    {
        $poli = Poli::all();
        return view('layanan.pasien-baru', compact('poli'));
    }

    public function getData(Request $request)
    {
        $tanggal = date('Y-m-d', strtotime($request->tanggal));
        $data = Reservasi::orderBy('perkiraan_jam_pelayanan','ASC')
                           ->where(function ($query) use ($request, $tanggal){
                                  $query->whereDate('tanggal', $tanggal);
                                  if (!empty($request->status)) {
                                    $query->where('status', $request->status);
                                  }
                                  if (!empty($request->poli)) {
                                    $query->where('poli_id', $request->poli);
                                  }
                              })
                           ->with('calonPasien','poli')
                           ->get();
        return datatables()->of($data)->addColumn('action', function($row){
            $btn = '<a href="'.route('reservasiadmin.edit',$row->id).'" class="btn border-primary btn-xs text-primary-600 btn-flat btn-icon"><i class="icon-enter3"> Tindakan</i></a>';
            return $btn;
        })
        ->addColumn('status', function($row){
           switch ($row->status) {
               case 'Reservasi':
                   $label = '<a id="reservasi" class="label bg-warning label-rounded">'.$row->status.'</a>';
                   break;
               case 'Menunggu Pemeriksaan':
                   $label = '<span class="label bg-warning label-rounded">'.$row->status.'</span>';
                   break;
               case 'Sedang Pemeriksaan':
                   $label = '<span class="label bg-info label-rounded">'.$row->status.'</span>';
                   break;
               case 'Selesai Pemeriksaan':
                   $label = '<span class="label bg-success label-rounded">'.$row->status.'</span>';
                   break;
               case 'Batal':
                   $label = '<span class="label bg-danger label-rounded">'.$row->status.'</span>';
                   break;
               case 'Terlewat Pemanggilan':
                   $label = '<span class="label bg-warning label-rounded">'.$row->status.'</span>';
                   break;
           }
           return $label;
        })
        ->addColumn('peserta', function($row){
            if ($row->calonPasien->no_bpjs == null) {
               $ket = 'Non BPJS';
            } else {
                $ket = 'BPJS';
            }
            return $ket;
        })
        ->rawColumns(['action','status'])
        ->make(true);
    }

    public function getDataPasienLama(Request $request)
    {
        $data = CalonPasien::all();
        return datatables()->of($data)
        ->addColumn('action', function($row){
            $btn = '<a href="'.route('calon-pasien.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            if ($row->tipe == 'child') {
                $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            }
            return $btn;
        })
        ->addColumn('no_bpjs', function($row){
            if ($row->no_bpjs == null) {
               $ket = 'Non BPJS';
            } else {
                $ket = $row->no_bpjs;
            }
            return $ket;
        })
        ->addColumn('antrian', function($row){
            $btn = '<a href="'.route('layanan.pasienLamaCreate',$row->id).'" class="btn border-primary btn-xs text-primary-600 btn-flat btn-icon"><i class="icon-add-to-list"></i> Daftarkan</a>';
            return $btn;
        })
        ->rawColumns(['antrian','action'])
        ->make(true);
    }

    public function pasienLamaCreate(Request $request,$id)
    {
      $data = CalonPasien::find($id);
      $lastReservasi = Reservasi::where('pasien_id', $data->id)->orderBy('created_at', 'DESC')->first();
      $poli = Poli::all();
      return view('layanan.pasien-lama-create', compact('data','poli','lastReservasi'));
    }

    public function statusUpdate($id)
    {
      $reservasi = Reservasi::find($id);
      $reservasi->status = 'Menunggu Pemeriksaan';
      $reservasi->status_updated_by = Auth::user()->id;
      $reservasi->save();

      $queryOrder = "CASE WHEN status = 'Menunggu Pemeriksaan' THEN 1 ";
      $queryOrder .= "ELSE 2 END";
      $update_antrian = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$reservasi->poli_id)
                        ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                        ->whereNull('counter')
                        ->orderByRaw($queryOrder)
                        ->orderBy('antrian','asc')
                        ->first();

      $infoAntrianPoli = app('App\Http\Controllers\ReservasiAdminController')->infoAntrianPoli($reservasi->poli_id);
      $antrian = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$reservasi->poli_id)->get();
      $info_antrian = [
          'total_pasien' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count(),
          'pasien_sdh_diperiksa' => $antrian->where('status','Selesai Pemeriksaan')->count(),
          'pasien_blm_diperiksa' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count()
      ];
      event(new InfoAntrianEvent($info_antrian));
      event(new InfoAntrianPoliEvent($infoAntrianPoli));
      event(new DisplayAntrianEvent());
      event(new PanggilAntrianEvent($update_antrian));

      return response()->json(['data'=>'success update data']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pasienLamaStore(Request $request, $id)
    {
        $this->validate(request(),
        [
            'keluhan' => 'required',
            'poli'=>'required'
        ]);
        DB::beginTransaction();
        $today = date('Y-m-d', strtotime(' +0 day'));
        $cek = Reservasi::where('pasien_id',$id)->where('tanggal',$today)->whereNotIn('status',['Batal'])->first();
        if ($cek) {
          return Redirect::back()->withErrors(['Anda sudah melakukan pendaftaran.']);
        }
        $poli = Poli::find($request->poli);
        $antrian = Reservasi::where('tanggal',$today)->where('poli_id',$request->poli)->count() + 1;
        $reservasi = new Reservasi;
        $reservasi->pasien_id = $id;
        $reservasi->keluhan = $request->keluhan;
        $reservasi->kekhawatiran = $request->kekhawatiran;
        $reservasi->poli_id = $request->poli;
        $reservasi->riwayat_penyakit_menahun = $request->riwayat_penyakit_menahun;
        $reservasi->upaya_pengobatan = $request->upaya_pengobatan;
        $reservasi->status = 'Menunggu Pemeriksaan';
        $reservasi->tanggal = $today;
        $reservasi->antrian = $poli->kode.$antrian;
        $reservasi->kode = $poli->kode.'RSV'.strtoupper(substr(md5(microtime()),rand(0,26),5));
        $reservasi->save();

        // dd($this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id));
        $reservasi->perkiraan_jam_pelayanan = $this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id);
        $reservasi->save();
        $infoAntrianPoli = app('App\Http\Controllers\ReservasiAdminController')->infoAntrianPoli($reservasi->poli_id);
        $antrian_selanjutnya = $reservasi->with('calonPasien')->first();

        DB::commit();

        if (Reservasi::where('tanggal',$today)->where('poli_id',$request->poli)->count() == 1) {
          event(new PanggilAntrianEvent($antrian_selanjutnya));
        }

        $antrian = Reservasi::where('tanggal',$today)->where('poli_id',$request->poli)->get();
        $info_antrian = [
          'total_pasien' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count(),
          'pasien_sdh_diperiksa' => $antrian->where('status','Selesai Pemeriksaan')->count(),
          'pasien_blm_diperiksa' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count()
        ];

        event(new InfoAntrianEvent($info_antrian));
        event(new DisplayAntrianEvent());
        event(new InfoAntrianPoliEvent($infoAntrianPoli));
        return redirect()->route('screening.create',$reservasi->id);
    }

    public function pasienBaruStore(Request $request)
    {
        $this->validate(request(),
        [
            'nama' => 'required',
            'nomor_telepon' => 'required|between:10,14',
            'nik' => 'required|digits:16',
            'keluhan' => 'required',
            'poli'=>'required'
        ]);
        DB::beginTransaction();
        $pasien = new CalonPasien();
        $pasien->nama = $request->nama;
        $pasien->no_telepon = $request->nomor_telepon;
        $pasien->nik = $request->nik;
        $pasien->no_bpjs = $request->nomor_bpjs_atau_kartu_sehat;
        $pasien->tipe = 'child';
        $pasien->created_by = Auth::user()->id;
        $pasien->save();

        $today = date('Y-m-d', strtotime(' +0 day'));
        $poli = Poli::find($request->poli);
        $antrian = Reservasi::where('tanggal',$today)->where('poli_id',$request->poli)->count() + 1;
        $reservasi = new Reservasi;
        $reservasi->pasien_id = $pasien->id;
        $reservasi->keluhan = $request->keluhan;
        $reservasi->kekhawatiran = $request->kekhawatiran;
        $reservasi->poli_id = $request->poli;
        $reservasi->riwayat_penyakit_menahun = $request->riwayat_penyakit_menahun;
        $reservasi->upaya_pengobatan = $request->upaya_pengobatan;
        $reservasi->status = 'Menunggu Pemeriksaan';
        $reservasi->tanggal = $today;
        $reservasi->antrian = $poli->kode.$antrian;
        $reservasi->kode = $poli->kode.'RSV'.strtoupper(substr(md5(microtime()),rand(0,26),5));
        $reservasi->save();

        $reservasi->perkiraan_jam_pelayanan = $this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id);

        $reservasi->save();
        $infoAntrianPoli = app('App\Http\Controllers\ReservasiAdminController')->infoAntrianPoli($reservasi->poli_id);
        $antrian_selanjutnya = $reservasi->with('calonPasien')->first();
        DB::commit();

        if (Reservasi::where('tanggal',$today)->where('poli_id',$request->poli)->count() == 1) {
          event(new PanggilAntrianEvent($antrian_selanjutnya));
        }

        $antrian = Reservasi::where('tanggal',$today)->where('poli_id',$request->poli)->get();
        $info_antrian = [
          'total_pasien' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count(),
          'pasien_sdh_diperiksa' => $antrian->where('status','Selesai Pemeriksaan')->count(),
          'pasien_blm_diperiksa' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count()
        ];

        event(new InfoAntrianEvent($info_antrian));
        event(new InfoAntrianPoliEvent($infoAntrianPoli));
        event(new DisplayAntrianEvent());
        return redirect()->route('screening.create',$reservasi->id);
    }

    public static function perkiraan_pelayanan($antrian,$tanggal,$poli)
    {
        $hari = Hari::where('day',date('l', strtotime($tanggal)))->first();
        $cek_jam = PoliHari::where('poli_id',$poli)->where('hari_id',$hari->id)->pluck('id');
        $cek_perkiraan_pelayanan = Sesi::whereIn('poli_hari_id',$cek_jam)->orderBy('mulai')->get();

        $sorting_reservasi = Reservasi::where('tanggal',$tanggal)->where('poli_id',$poli)
                            ->whereNotIn('status',['Batal','Selesai Pemeriksaan'])
                            ->orderBy('antrian','asc')->orderBy('counter','asc')
                            ->pluck('antrian')->toArray();
        //sorting nomor antrian dari paling kecil
        //sort($sorting_reservasi, SORT_FLAG_CASE);

        $jumlah_max_pasien = [];
        $jumlah_pasien = 0;
        $antrian_ke_index = array_search($antrian, $sorting_reservasi); //cari antrian pada index berapa di array

        $waktu = Poli::find($poli);
        //cari jumlah pasien yg dilayani tiap sesi
        foreach ($cek_perkiraan_pelayanan as $key => $value) {
            $jumlah_pasien += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
            $jumlah_max_pasien[$value->sesi] = $jumlah_pasien;
        }

        //dibalik arraynya dari yg paling banyak valuenya
        $reverse_jumlah_max_pasien = array_reverse($jumlah_max_pasien);

        //cek index antrian yang didapat pada sesi berapa
        foreach ($reverse_jumlah_max_pasien as $key => $value) {
          if ($antrian_ke_index < $value ) {
            $cek_antrian_pada_sesi = $key;
            unset($reverse_jumlah_max_pasien[$key]);
            $urutan = $antrian_ke_index - array_sum($reverse_jumlah_max_pasien);
          }
        }

        $jam_buka = Sesi::whereIn('poli_hari_id',$cek_jam)->where('sesi',$cek_antrian_pada_sesi)->first();

        if (strtotime(date('H:i')) > strtotime($jam_buka->mulai) && strtotime(date('H:i')) < strtotime($jam_buka->selesai)) {
          if ($urutan < $jam_buka->jumlah_dokter) {
              $menit = 0 * $waktu->waktu / $jam_buka->jumlah_dokter;
              $perkiraan = date('H:i', strtotime('+'.$menit.' minutes', strtotime(date('H:i'))));
          } else {
              $sebelumnya = $sorting_reservasi[$urutan - 1];
              $cek_antrian_sebelumnya = Reservasi::where('antrian',$sebelumnya)->whereDate('tanggal',$tanggal)->first();
              $perkiraan = date('H:i', strtotime('+'.$waktu->waktu.' minutes', strtotime($cek_antrian_sebelumnya->perkiraan_jam_pelayanan)));
          }
        } else {
          if ($urutan < $jam_buka->jumlah_dokter) {
            $menit = 0 * $waktu->waktu / $jam_buka->jumlah_dokter;
          } else {
            $menit = $urutan * $waktu->waktu / $jam_buka->jumlah_dokter;
          }
          $perkiraan = date('H:i', strtotime('+'.$menit.' minutes', strtotime($jam_buka->mulai)));
        }

        return $perkiraan;
    }

    public function poliCek($poli)
    {
      $now = date('Y-m-d', strtotime(' +0 day'));
      $reservasi_now = Reservasi::where('poli_id',$poli)->where('tanggal',$now)->whereNotIn('status',['Batal','Selesai Pemeriksaan'])->get();
      $data = [];

      $cek_hari_now = Hari::where('day',date('l',strtotime($now)))->first();

      $cek_jadwal_now = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari_now->id)->pluck('id');

      $cek_pelayanan_now = Sesi::whereIn('poli_hari_id',$cek_jadwal_now)->orderBy('mulai','ASC')->get();

      $sesi_now = Sesi::where('poli_hari_id',$cek_jadwal_now)->orderBy('mulai','ASC')->get();

      $waktu = Poli::find($poli);
      $max_pelayanan_now = 0;
      foreach ($sesi_now as $value) {
        if (strtotime(date('H:i')) >= strtotime($value->mulai)) {
          if (strtotime(date('H:i')) >= strtotime($value->selesai)) {
            // kalau sesi mulai udah kelewat sampai selesai, nggak diitung
            // nothing
          } else {
            // kalau sesi mulai udah kelewat tapi belum selesai, pakai jam sekarang
            $max_pelayanan_now += (strtotime($value->selesai) - strtotime(date('H:i'))) / 60 / $waktu->waktu * $value->jumlah_dokter;
          }
        } else {
          // kalau sesi mulai belum kelewat, pakai jam sesi mulai
          $max_pelayanan_now += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
        }
      }
      if (!$cek_pelayanan_now->isEmpty()) {
        if ($reservasi_now->count() < floor($max_pelayanan_now)) {
          $data[] = ['total_antrian'=>$reservasi_now->count(),'slot_antrian'=>floor($max_pelayanan_now-$reservasi_now->count())];
        } else {
          if ($max_pelayanan_now < 1) {
            $data = 'Mohon maaf, pelayanan pada poli sudah selesai.';
          } else {
            $data = 'Mohon maaf, pelayanan pada poli sudah penuh.';
          }
        }
      } else {
          $data = 'Mohon maaf, tidak ada pelayanan untuk hari ini.';
      }
      // return $reservasi_now->count();
      return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
