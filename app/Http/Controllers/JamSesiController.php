<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JamSesi;
use App\Jam;
use App\Sesi;
use App\Reservasi;
use Carbon\Carbon;
use DB;

class JamSesiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getData(Request $request)
     {
        $data = JamSesi::all();
        return datatables()->of($data)->addColumn('action', function($row){
            $btn = '<a href="'.route('jamsesi.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            return $btn;
        })
        ->addColumn('jam', function($row){
          return Jam::find($row->jam_id)->jam;
        })
        ->addColumn('sesi', function($row){
        return Sesi::find($row->sesi_id)->sesi;
        })
        ->rawColumns(['action'])
        ->make(true);
     }
    public function index()
    {
      return view('jamsesi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $jam = Jam::all();
      $sesi = Sesi::all();
      return view('jamsesi.create',compact('jam','sesi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate(request(),
      [
          'jam' => 'required',
          'sesi' => 'required',
      ]
      );

      DB::beginTransaction();
      for ($i=0; $i < count($request->sesi); $i++) {
        $jamSesi = new JamSesi();
        $jamSesi->jam_id = $request->jam;
        $jamSesi->sesi_id = $request->sesi[$i];
        $jamSesi->save();
      }
      DB::commit();


      return redirect()->route('jamsesi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = JamSesi::find($id);
        return view('jamsesi.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(),
      [
          'jam' => 'required',
          'sesi' => 'required',
      ]
      );

      $jamSesi = JamSesi::find($id);
      $jamSesi->jam = $request->jam;
      $jamSesi->sesi = $request->sesi;
      $jamSesi->save();

      return redirect()->route('jamsesi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $jamSesi = JamSesi::find($id);
      $jamSesi->delete();
      return response()->json(['data'=>'success delete data']);
    }

    public function api_tanggal()
    {
      $tanggal = [date('Y-m-d', strtotime(' +1 day')),date('Y-m-d', strtotime(' +2 day'))];
      return response()->json([
        'status'      => 'success',
        'result'   => $tanggal
      ]);
    }

    public function api_jam($tanggal)
    {
        $reservasi = Reservasi::where('tanggal',$tanggal)->pluck('waktu');
        if ($reservasi->count() == JamSesi::count()) {
          return response()->json([
            'status'      => 'failed',
            'message'   => 'Reservasi untuk tanggal ' .$tanggal. ' sudah tidak tersedia.',
          ]);
        } else {
            $data = JamSesi::whereNotIn('id',$reservasi)->distinct()->get(['jam_id']);
            $data = Jam::whereIn('id',$data)->get();
            return response()->json([
              'status'      => 'success',
              'result'   => $data
            ]);
        }
    }

    public function api_sesi($tanggal,$jam)
    {
        $reservasi = Reservasi::where('tanggal',$tanggal)->pluck('waktu');
        $data = JamSesi::whereNotIn('id',$reservasi)->where('jam_id',$jam)->distinct()->get(['sesi_id']);
        $data = Sesi::whereIn('id',$data)->get();
        return response()->json([
          'status'      => 'success',
          'result'   => $data
        ]);
    }
}
