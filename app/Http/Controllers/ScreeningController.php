<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;
use App\CalonPasien;
use App\Reservasi;
use App\JawabanCalonPasien;
use App\HasilSkor;
use App\HasilScreening;
use Auth;
use DB;

class ScreeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($reservasi_id)
    {
        $reservasi = Reservasi::with('calonPasien')->find($reservasi_id);
        $pertanyaan = Pertanyaan::all()->sortBy('id');
        if ($reservasi->calonPasien->created_by == 0) {
          return view('pasien.no-login.screening', compact('pertanyaan','reservasi'));
        } else {
          return view('pasien.screening.create', compact('pertanyaan','reservasi'));
        }
    }

    public function createUmum()
    {
        $pertanyaan = Pertanyaan::all()->sortBy('id');
        return view('pasien.screening.create-umum', compact('pertanyaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $reservasi_id)
    {
       $reservasi = Reservasi::find($reservasi_id);
       DB::beginTransaction();
       // $array = array_values($request->all());
       // for ($i=0; $i < sizeof($array[1]) ; $i++) {
       //     $jawaban = new JawabanCalonPasien;
       //     $jawaban->reservasi_id = $reservasi_id;
       //     $jawaban->calon_pasien_id = $reservasi->pasien_id;
       //     $jawaban->pertanyaan_id = $array[1][$i];
       //     $jawaban->jawaban = $array[2][$i];
       //     $pertanyaan = Pertanyaan::find($array[1][0]);
       //     if ($array[2][$i] == 'ya') {
       //       $jawaban->skor = $pertanyaan->skor_ya;
       //     } else {
       //       $jawaban->skor = $pertanyaan->skor_tidak;
       //     }
       //     $jawaban->save();
       // }

        $array = array_values($request->all());
        switch ($array) {
    /*1 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'sehat';
                    break;
    /*2 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'wajib_lapor';
                    break;
    /*3 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'wajib_lapor';
                    break;
    /*4 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'wajib_lapor';
                    break;
    /*5 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'otg';
                    break;
    /*6 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'otg';
                    break;
    /*7 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'otg';
                    break;
    /*8 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'otg';
                    break;
    /*9 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'waspada';
                    break;
    /*10*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'odp';
                    break;
    /*11*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'odp';
                    break;
    /*12*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'odp';
                    break;
    /*13*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'odp';
                    break;
    /*14*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'odp';
                    break;
    /*15*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'odp';
                    break;
    /*16*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'odp';
                    break;
    /*17*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'waspada';
                    break;
    /*18*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'odp';
                    break;
    /*19*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'odp';
                    break;
    /*20*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'odp';
                    break;
    /*21*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'pdp';
                    break;
    /*22*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'pdp';
                    break;
    /*23*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'pdp';
                    break;
    /*24*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'pdp';
                    break;
    /*25*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'waspada';
                    break;
    /*26*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'pdp';
                    break;
    /*27*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'pdp';
                    break;
    /*28*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'pdp';
                    break;
    /*29*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                    $kode = 'pdp';
                    break;
    /*30*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                    $kode = 'pdp';
                    break;
    /*31*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                    $kode = 'pdp';
                    break;
    /*32*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                    $kode = 'pdp';
                    break;
                default:
                  $kode = 'belum_diketahui';
                  break;
        }

         $hasil_skor = HasilSkor::where('kategori_hasil',$kode)->first();
         $hasil_screening = new HasilScreening;
         $hasil_screening->reservasi_id = $reservasi->id;
         $hasil_screening->hasil_skors_id = $hasil_skor->id;
         $hasil_screening->save();

         for ($i=0; $i < sizeof($array[1]) ; $i++) {
             $jawaban = new JawabanCalonPasien;
             $jawaban->reservasi_id = $reservasi->id;
             $jawaban->calon_pasien_id = $reservasi->pasien_id;
             $jawaban->pertanyaan_id = $array[1][$i];
             if ($array[2][$i] == NULL) {
              $jawaban->jawaban = 'tidak ada';
             } else {
              $jawaban->jawaban = $array[2][$i];
             }
             $pertanyaan = Pertanyaan::find($array[1][0]);
             if ($array[2][$i] == 'ya' || ($array[2][$i] != NULL && $array[2][$i] != 'tidak')) {
               $jawaban->skor = $pertanyaan->skor_ya;
             } else {
               $jawaban->skor = $pertanyaan->skor_tidak;
             }
             $jawaban->save();
         }
       DB::commit();
       if ($reservasi->calonPasien->created_by == 0) {
         return redirect('pasien/reservasi/'.$reservasi->kode);
       } else {
         return redirect('reservasi/'.$reservasi->id);
       }
    }

    public function storeUmum(Request $request)
    {
       $array = array_values($request->all());
       switch ($array) {
   /*1 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'sehat';
                   break;
   /*2 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'wajib_lapor';
                   break;
   /*3 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'wajib_lapor';
                   break;
   /*4 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'wajib_lapor';
                   break;
   /*5 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'otg';
                   break;
   /*6 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'otg';
                   break;
   /*7 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'otg';
                   break;
   /*8 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'otg';
                   break;
   /*9 */      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'waspada';
                   break;
   /*10*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'odp';
                   break;
   /*11*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'odp';
                   break;
   /*12*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'odp';
                   break;
   /*13*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'odp';
                   break;
   /*14*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'odp';
                   break;
   /*15*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'odp';
                   break;
   /*16*/      case ($array[2][0] == 'tidak' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'odp';
                   break;
   /*17*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'waspada';
                   break;
   /*18*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'odp';
                   break;
   /*19*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'odp';
                   break;
   /*20*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'odp';
                   break;
   /*21*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'pdp';
                   break;
   /*22*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'pdp';
                   break;
   /*23*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'pdp';
                   break;
   /*24*/      case ($array[2][0] == 'ya' && $array[2][1] == 'tidak' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'pdp';
                   break;
   /*25*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'waspada';
                   break;
   /*26*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'pdp';
                   break;
   /*27*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'pdp';
                   break;
   /*28*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'tidak' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'pdp';
                   break;
   /*29*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'tidak'):
                   $kode = 'pdp';
                   break;
   /*30*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'tidak' && $array[2][4] == 'ya'):
                   $kode = 'pdp';
                   break;
   /*31*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'tidak'):
                   $kode = 'pdp';
                   break;
   /*32*/      case ($array[2][0] == 'ya' && $array[2][1] == 'ya' && $array[2][2] == 'ya' && $array[2][3] == 'ya' && $array[2][4] == 'ya'):
                   $kode = 'pdp';
                   break;
               default:
                 $kode = 'belum_diketahui';
                 break;
       }
       $hasil = HasilSkor::where('kategori_hasil',$kode)->first();
       $gambar= asset('images/emoticon/'.$hasil->gambar.'');
       return view('pasien.screening.hasil-umum', compact('hasil','gambar'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // START OF API

    public function api_index()
    {
        $data = Pertanyaan::all()->sortBy('id');
        foreach ($data as $key => $value) {
          $value->jawaban = 'ya';
        }
        return response()->json([
          'status'      => 'success',
          'result'      => $data
        ]);
    }

    public function api_store(Request $request, $reservasi_id)
    {
       $reservasi = Reservasi::with('calonPasien','poli')->find($reservasi_id);
       $answer = json_decode($request->getContent(), true);

       // dd($answer['result'][0]['jawaban']);
       DB::beginTransaction();
       foreach ($answer['result'] as $key => $value) {
            $jawaban = new JawabanCalonPasien;
            $jawaban->reservasi_id = $reservasi_id;
            $jawaban->calon_pasien_id = $reservasi->pasien_id;
            $jawaban->pertanyaan_id = $value['id'];
            $jawaban->jawaban = $value['jawaban'];
            $pertanyaan = Pertanyaan::find($value['id']);
            if (!$value['jawaban']) {
              return response()->json([
                'status'      => 'failed',
                'message'      => 'Semua pertanyaan harus dijawab.',
              ]);
            } else {
              if ($value['jawaban'] == 'ya') {
                $jawaban->skor = $value['skor_ya'];
              } else {
                $jawaban->skor = $value['skor_tidak'];
              }
            }
            $jawaban->save();
       }
       $skor = JawabanCalonPasien::where('calon_pasien_id',$reservasi->pasien_id)
                                 ->where('reservasi_id', $reservasi->id)
                                 ->groupBy('reservasi_id','skor')
                                 ->selectRaw('reservasi_id, sum(skor) as skor')
                                 ->first();
       $hasil = HasilSkor::where('batas_bawah','<=',$skor->skor)
                          ->where('batas_atas','>=',$skor->skor)
                          ->first();
       DB::commit();
       return response()->json([
         'status'      => 'success',
         'reservasi'      => $reservasi,
         'screening'      => [
              'skor' => $skor->skor,
              'hasil' => $hasil->hasil,
         ],

       ]);
    }

    public function api_storeUmum(Request $request)
    {
       $answer = json_decode($request->getContent(), true);
       $totalSkor = 0;
       foreach ($answer['result'] as $key => $value) {
            if (!$value['jawaban']) {
              return response()->json([
                'status'      => 'failed',
                'message'      => 'Semua pertanyaan harus dijawab.',
              ]);
            } else {
              $pertanyaan = Pertanyaan::find($value['id']);
              if ($value['jawaban'] == 'ya') {
                $value['jawaban'] = $value['skor_ya'];
              } else {
                $value['jawaban'] = $value['skor_tidak'];
              }
              $totalSkor = $totalSkor+$value['jawaban'];
            }
       }
       $hasil = HasilSkor::where('batas_bawah','<=',$totalSkor)
                          ->where('batas_atas','>=',$totalSkor)
                          ->first()->hasil;
       return response()->json([
         'status'      => 'success',
         'screening'      => [
              'skor' => $totalSkor,
              'hasil' => $hasil,
         ],

       ]);
    }
}
