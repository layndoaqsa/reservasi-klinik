<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PoliHari;
use App\Sesi;
use App\DokterSesi;
use App\Poli;
use DB;

class SesiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $poliHari = PoliHari::find($id);
        $poliHariAll = PoliHari::where('poli_id',$poliHari->poli_id)->get();
        return view('sesi.index', compact('poliHari','poliHariAll'));
    }

    public function getData(Request $request)
    {
        $data = Sesi::where('poli_hari_id',$request->poli_hari_id)->get();
        return datatables()->of($data)->addColumn('action', function($row){
            $btn = '<a href="'.route('sesi.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            return $btn;
        })
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $poliHari = PoliHari::find($request->sesi_id);
        return view('sesi.create', compact('poliHari'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),
        [
          'sesi' => 'required',
          'waktu_mulai' => 'required',
          'waktu_selesai' => 'required',
          'jumlah_dokter' => 'required',
        ]
        );

        DB::beginTransaction();
        $sesi = new Sesi();
        $sesi->sesi = $request->sesi;
        $sesi->poli_hari_id = $request->poli_hari_id;
        $sesi->mulai = $request->waktu_mulai;
        $sesi->selesai = $request->waktu_selesai;
        $sesi->jumlah_dokter = $request->jumlah_dokter;
        $sesi->save();
        foreach ($request->dokter as $key => $value) {
          $dokter_sesi = new DokterSesi();
          $dokter_sesi->sesi_id = $sesi->id;
          $dokter_sesi->dokter_id = $value;
          $dokter_sesi->save();
        }
        DB::commit();

        return redirect()->route('sesi.index',$sesi->poli_hari_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Sesi::with('dokterSesi')->find($id);
        $poliHari = PoliHari::find($data->poli_hari_id);
        return view('sesi.edit',compact('data','poliHari'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(),
      [
        'sesi' => 'required',
        'waktu_mulai' => 'required',
        'waktu_selesai' => 'required',
        'jumlah_dokter' => 'required',
      ]
      );

      DB::beginTransaction();
      $sesi = Sesi::find($id);
      $sesi->sesi = $request->sesi;
      $sesi->mulai = $request->waktu_mulai;
      $sesi->selesai = $request->waktu_selesai;
      $sesi->jumlah_dokter = $request->jumlah_dokter;
      $sesi->save();

      $dokter_sesi = DokterSesi::where('sesi_id',$sesi->id)->get();
      foreach ($dokter_sesi as $key => $value) {
        $value->delete();
      }
      foreach ($request->dokter as $key => $value) {
        $dokter_sesi = new DokterSesi();
        $dokter_sesi->sesi_id = $sesi->id;
        $dokter_sesi->dokter_id = $value;
        $dokter_sesi->save();
      }

      DB::commit();

      return redirect()->route('sesi.index',$sesi->poli_hari_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sesi = Sesi::find($id)->delete();
        $dokter_sesi = DokterSesi::where('sesi_id',$id)->get();
        foreach ($dokter_sesi as $key => $value) {
          $value->delete();
        }
        return response()->json(['data'=>'success delete data']);
    }

    public function jadwal()
    {
        return view('pasien.jadwal');
    }
    public function getJadwal(Request $request)
    {
        $data = PoliHari::where('hari_id',$request->hari_id)->with('poli')->get();
        // foreach ($data as $key => $value) {
        //   foreach (Sesi::where('poli_hari_id',$value->id)->get() as $key2 => $value2) {
        //     dd($value2);
        //   }
        // }
        // dd($data);

        // return $data;
        return datatables()->of($data)->addColumn('action', function($row){
            $btn = '<a href="'.route('sesi.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            return $btn;
        })->addColumn('waktu_sesi', function($row) {
            if (Sesi::where('poli_hari_id',$row->id)->get()->count() < 1) {
              $btn = 'Tidak ada jadwal';
            } else {
              $btn = '';
              foreach (Sesi::where('poli_hari_id',$row->id)->get() as $key => $value) {
                $btn = $btn.'<strong>'.$value->sesi.' ('.$value->mulai.' - '.$value->selesai.')</strong><br>';
                foreach (DokterSesi::where('sesi_id',$value->id)->get() as $key2 => $value2) {
                  $btn = $btn.' <li> ' . $value2->dokter['name'] . ' </li>';
                }
              }
            }
            return $btn;
        })
        ->addIndexColumn()
        ->rawColumns(['action','waktu_sesi','dokter'])
        ->make(true);
    }
}
