<?php

namespace App\Http\Controllers;

use App\Events\PanggilAntrianEvent;
use App\Events\DisplayAntrianEvent;
use App\Events\InfoAntrianEvent;
use App\Events\InfoAntrianPoliEvent;
use Illuminate\Http\Request;
use App\Reservasi;
use App\Poli;
use Carbon\Carbon;
use Auth;
use DB;
use App\Exports\ReservasiExport;
use Maatwebsite\Excel\Facades\Excel;

// Imports the Cloud Client Library
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;

class ReservasiAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getData(Request $request)
     {
         $data = Reservasi::orderBy('perkiraan_jam_pelayanan','ASC')
                            ->where(function ($query) use ($request){
                                   $tanggal = date('Y-m-d',strtotime($request->tanggal));
                                   $query->whereDate('tanggal', $tanggal);
                                   if (!empty($request->status)) {
                                     $query->where('status', $request->status);
                                   }
                                   if (!empty($request->poli)) {
                                     $query->where('poli_id', $request->poli);
                                   }
                                   if (Auth::user()->hasRole('Dokter')) {
                                     $query->where('poli_id', Auth::user()->poli_id);
                                   }
                               })
                            ->with('calonPasien','poli')
                            ->get();
         return datatables()->of($data)->addColumn('action', function($row){
             $btn = '<a href="'.route('reservasiadmin.edit',$row->id).'" class="btn border-primary btn-xs text-primary-600 btn-flat btn-icon"><i class="icon-enter3"> Tindakan</i></a>';
             // $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
             return $btn;
         })
         ->addColumn('status', function($row){
            switch ($row->status) {
                case 'Reservasi':
                    $label = '<a id="reservasi" class="label bg-warning label-rounded">'.$row->status.'</a>';
                    break;
                case 'Menunggu Pemeriksaan':
                    $label = '<span class="label bg-warning label-rounded">'.$row->status.'</span>';
                    break;
                case 'Sedang Pemeriksaan':
                    $label = '<span class="label bg-info label-rounded">'.$row->status.'</span>';
                    break;
                case 'Selesai Pemeriksaan':
                    $label = '<span class="label bg-success label-rounded">'.$row->status.'</span>';
                    break;
                case 'Batal':
                    $label = '<span class="label bg-danger label-rounded">'.$row->status.'</span>';
                    break;
                case 'Terlewat Pemanggilan':
                    $label = '<span class="label bg-warning label-rounded">'.$row->status.'</span>';
                    break;
            }
            return $label;
         })
         ->addColumn('peserta', function($row){
             if ($row->calonPasien->no_bpjs == null) {
                $ket = 'Non BPJS';
             } else {
                 $ket = 'BPJS';
             }
             return $ket;
         })
         ->rawColumns(['action','status'])
         ->make(true);
     }

    public function index()
    {
        $poli = Poli::all();
        if (Auth::user()->hasRole('Dokter')) {
            $poli_id = Auth::user()->poli_id;
            $queryOrder = "CASE WHEN status = 'Menunggu Pemeriksaan' THEN 1 ";
            $queryOrder .= "ELSE 2 END";
            $reservasi = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)->get();
            $antrian_selanjutnya = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                ->whereNull('counter')
                                ->orderByRaw($queryOrder)->orderBy('antrian','asc')->first();

            return view('reservasi.dokter-index', compact('poli','reservasi','antrian_selanjutnya'));
        } else {
            return view('reservasi.index', compact('poli'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Reservasi::find($id);
      return view('reservasi.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(),
      [
          'perubahan_status' => 'required',
          // 'nomor_bpjs' => 'required',
      ]
      );
      $data = Reservasi::find($id);
      $data->status = $request->perubahan_status;
      $data->save();
      return redirect()->route('reservasiadmin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function panggilAntrian($antrian,$kode)
    {
        $pasien = Reservasi::with('calonPasien','screening.hasilSkor')->where('antrian',$antrian)->where('kode',$kode)->first();
        $poli_id = Auth::user()->poli_id;

        if ($pasien->counter < 3) {
            $pasien->counter += 1;
            $pasien->status_updated_by = Auth::id();
            $pasien->save();

        } else {
            $pasien->status = "Terlewat Pemanggilan";
            $pasien->status_updated_by = Auth::id();
            $pasien->counter = NULL;
            $pasien->save();
            event(new DisplayAntrianEvent());
            $infoAntrianPoli = $this->infoAntrianPoli($pasien->poli_id);
            event(new InfoAntrianPoliEvent($infoAntrianPoli));
        }

        if (!$pasien->calonPasien->bpjs) {
            $pasien->bpjs = 'Non BPJS';
        } else {
            $pasien->bpjs = $pasien->calonPasien->bpjs;
        }

        if (!$pasien->kekhawatiran) {
            $pasien->kekhawatiran = 'Tidak ada';
        } else {
            $pasien->kekhawatiran = $pasien->kekhawatiran;
        }

        if (!$pasien->riwayat_penyakit_menahun) {
            $pasien->riwayat_penyakit_menahun = 'Tidak ada';
        } else {
            $pasien->riwayat_penyakit_menahun = $pasien->riwayat_penyakit_menahun;
        }

        if (!$pasien->upaya_pengobatan) {
            $pasien->upaya_pengobatan = 'Tidak ada';
        } else {
            $pasien->bpjs = $pasien->upaya_pengobatan;
        }

        if ($pasien->counter == 1) {
            $queryOrder = "CASE WHEN status = 'Menunggu Pemeriksaan' THEN 1 ";
            $queryOrder .= "ELSE 2 END";
            
            $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                            ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                            ->whereNull('counter')
                                            ->where('antrian','>',$pasien->antrian)
                                            ->orderByRaw($queryOrder)
                                            ->orderBy('antrian','asc')->first();
            if ($antrian_selanjutnya == NULL) {
                $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                            ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                            ->whereNull('counter')
                                            ->where('antrian','<',$pasien->antrian)
                                            ->orderByRaw($queryOrder)
                                            ->orderBy('antrian','asc')->first();
            }
            event(new PanggilAntrianEvent($antrian_selanjutnya));
            event(new DisplayAntrianEvent());
            $infoAntrianPoli = $this->infoAntrianPoli($pasien->poli_id);
            event(new InfoAntrianPoliEvent($infoAntrianPoli));
        } else if ($pasien->counter == null) {
            if (Reservasi::whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)->count() == 1) {
                $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                                ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                                ->whereNull('counter')
                                                ->where('antrian',$pasien->antrian)
                                                ->orderBy('antrian','asc')->first();
            } else {
              $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                            ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                            ->whereNull('counter')
                                            ->where('antrian','>',$pasien->antrian)
                                            ->orderBy('antrian','asc')->first();
              if ($antrian_selanjutnya == NULL) {
                $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                            ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                            ->whereNull('counter')
                                            ->where('antrian','<',$pasien->antrian)
                                            ->orderBy('antrian','asc')->first();
                if ($antrian_selanjutnya == NULL) {
                  $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
                                            ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                                            ->whereNull('counter')
                                            ->where('antrian',$pasien->antrian)
                                            ->orderBy('antrian','asc')->first();
                }
              }
            }
            // $antrian_selanjutnya = Reservasi::with('calonPasien')->where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)
            //                                 ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
            //                                 ->whereNull('counter')
            //                                 ->where('antrian',$pasien->antrian)
            //                                 ->orderBy('antrian','asc')->first();
            event(new PanggilAntrianEvent($antrian_selanjutnya));
            event(new DisplayAntrianEvent());
            $infoAntrianPoli = $this->infoAntrianPoli($pasien->poli_id);
            event(new InfoAntrianPoliEvent($infoAntrianPoli));
        }
        return response()->json($pasien);
    }

    public function ubahStatusPasien($tipe,$antrian,$kode)
    {
        $pasien = Reservasi::with('calonPasien')->where('antrian',$antrian)->where('kode',$kode)->first();
        switch ($tipe) {
            case 'datang':
                $pasien->status = 'Sedang Pemeriksaan';
                $pasien->status_updated_by = Auth::id();
                $pasien->save();
                $infoAntrianPoli = $this->infoAntrianPoli($pasien->poli_id);
                event(new InfoAntrianPoliEvent($infoAntrianPoli));
                break;
            case 'selesai':
                $pasien->status = 'Selesai Pemeriksaan';
                $pasien->status_updated_by = Auth::id();
                $pasien->save();

                $poli_id = Auth::user()->poli_id;
                $antrian = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$poli_id)->get();
                $info_antrian = [
                    'total_pasien' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count(),
                    'pasien_sdh_diperiksa' => $antrian->where('status','Selesai Pemeriksaan')->count(),
                    'pasien_blm_diperiksa' => $antrian->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count()
                ];

                event(new InfoAntrianEvent($info_antrian));
                break;
        }
    }

    public function antrian(){
      $poli = Poli::all();
      foreach ($poli as $key => $value) {
        $value->antrian = Reservasi::where('poli_id',$value->id)
                      ->whereDate('tanggal', Carbon::today())
                      ->get()->count();
        $value->sisa = Reservasi::where('poli_id',$value->id)
                      ->whereDate('tanggal', Carbon::today())
                      ->whereIn('status',['Reservasi','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                      ->get()->count();
        $value->dipanggil = Reservasi::where('poli_id',$value->id)
                      ->whereDate('tanggal', Carbon::today())
                      ->whereNotNull('counter')
                      ->whereIn('status',['Sedang Pemeriksaan','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                      ->select('antrian','kode','pasien_id')
                      ->orderBy('updated_at','DESC')
                      ->get()->take(2);
      }
      $display = Reservasi::with('poli','calonPasien','updatedBy')->whereDate('tanggal', Carbon::today())
                      ->whereNotNull('counter')
                      ->whereIn('status',['Sedang Pemeriksaan','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                      ->orderBy('updated_at','DESC')
                      ->get()->take(2);
      return view('antrian', compact('poli','display'));
    }

    public function infoAntrianPoli($id)
    {
      $poli = Poli::find($id);
      $poli->antrian = Reservasi::where('poli_id',$poli->id)
                    ->whereDate('tanggal', Carbon::today())
                    ->whereNotIn('status',['Batal'])
                    ->get()->count();
      $poli->sisa = Reservasi::where('poli_id',$poli->id)
                    ->whereDate('tanggal', Carbon::today())
                    ->whereIn('status',['Reservasi','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                    ->get()->count();
      $poli->dipanggil1 = Reservasi::where('poli_id',$poli->id)
                    ->whereDate('tanggal', Carbon::today())
                    ->whereNotNull('counter')
                    ->whereIn('status',['Sedang Pemeriksaan','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                    ->select('antrian','kode','pasien_id')
                    ->orderBy('updated_at','DESC')
                    ->first();
      $poli->dipanggil2 = Reservasi::where('poli_id',$poli->id)
                    ->whereDate('tanggal', Carbon::today())
                    ->whereNotNull('counter')
                    ->whereIn('status',['Sedang Pemeriksaan','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                    ->select('antrian','kode','pasien_id')
                    ->orderBy('updated_at','DESC')
                    ->skip(1)->first();
      return response()->json($poli);
    }

    public function voiceCaller()
    {
        // instantiates a client
        $client = new TextToSpeechClient([
            'credentials' => 'D:\Master\Service Account Key Google\bayar'
        ]);

        // sets text to be synthesised
        $synthesisInputText = (new SynthesisInput())
            ->setText('nomor antrian A1 silahkan masuk ke ruang perawatan');

        // build the voice request, select the language code ("en-US") and the ssml
        // voice gender
        $voice = (new VoiceSelectionParams())
            ->setLanguageCode('id')
            ->setSsmlGender(SsmlVoiceGender::FEMALE);

        // Effects profile
        $effectsProfileId = "telephony-class-application";

        // select the type of audio file you want returned
        $audioConfig = (new AudioConfig())
            ->setAudioEncoding(AudioEncoding::MP3)
            ->setEffectsProfileId(array($effectsProfileId));

        // perform text-to-speech request on the text input with selected voice
        // parameters and audio file type
        $response = $client->synthesizeSpeech($synthesisInputText, $voice, $audioConfig);
        $audioContent = $response->getAudioContent();

        // the response's audioContent is binary
        file_put_contents('output.mp3', $audioContent);
        echo 'Audio content written to "output.mp3"' . PHP_EOL;
    }

    public function infoAntrian(Request $request)
    {
        $poli = Poli::all();
        if (!$request->kode_poli) {
          $request->kode_poli = $poli[0]['kode'];
        }
        $data = Poli::where('kode',$request->kode_poli)->first();
        $data->total_pasien = Reservasi::where('poli_id',$data->id)
        ->whereDate('tanggal', Carbon::today())
        ->whereNotIn('status',['Batal','Reservasi'])
        ->get()->count();
        $data->total_pasien_terlayani = Reservasi::where('poli_id',$data->id)
        ->whereDate('tanggal', Carbon::today())
        ->whereIn('status',['Selesai Pemeriksaan'])
        ->get()->count();
        $data->diperiksa = Reservasi::where('poli_id',$data->id)
        ->whereDate('tanggal', Carbon::today())
        ->whereIn('status',['Sedang Pemeriksaan'])
        ->orderBy('updated_at','DESC')
        ->first();
        $queryOrder = "CASE WHEN status = 'Menunggu Pemeriksaan' THEN 1 ";
        $queryOrder .= "ELSE 2 END";
        $data->antrian_selanjutnya = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$data->id)
        ->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])
        ->orderByRaw($queryOrder)->orderBy('antrian','asc')->get();

        $data->antrian_terlewati = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$data->id)
        ->whereIn('status',['Terlewat Pemanggilan'])
        ->whereNull('counter')
        ->orderByRaw($queryOrder)->orderBy('antrian','asc')->get();
        $data->antrian_belum_datang = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$data->id)
        ->whereIn('status',['Reservasi'])
        ->orderBy('antrian','asc')->get();
        $data->antrian_selesai = Reservasi::where('tanggal',date('Y-m-d'))->where('poli_id',$data->id)
        ->whereIn('status',['Selesai Pemeriksaan'])
        ->orderBy('antrian','asc')->get();
        if (Auth::user()) {
          return view('pasien.info-antrian', compact('poli','data'));
        } else {
          return view('pasien.no-login.info-antrian', compact('poli','data'));
        }
    }

    public function export(Request $request)
    {
        $data = Reservasi::leftJoin('calon_pasien','reservasi.pasien_id','calon_pasien.id')
        ->leftJoin('poli','reservasi.poli_id','poli.id')
        ->leftJoin('users','reservasi.status_updated_by','users.id')
        ->where(function ($query) use ($request){
            if (!empty($request->download_tanggal)) {
              $tanggal = date('Y-m-d',strtotime($request->download_tanggal));
              $query->whereDate('reservasi.tanggal', $tanggal);
            } else {
              $query->whereDate('reservasi.tanggal', Carbon::today());
            }
            if ($request->download_bpjs == 'bpjs') {
              $query->whereNotNull('calon_pasien.no_bpjs');
            } elseif ($request->download_bpjs == 'non-bpjs') {
              $query->whereNull('calon_pasien.no_bpjs');
            }
            if (!empty($request->download_poli)) {
              $query->where('reservasi.poli_id', $request->download_poli);
            }
        })
        ->select('calon_pasien.nama','calon_pasien.no_bpjs','poli.poli','users.name','reservasi.status')->get();
          if (!empty($request->download_tanggal)) {
            $tanggal = date('Y-m-d',strtotime($request->download_tanggal));
          } else {
            $tanggal = Carbon::today()->format('Y-m-d');
          }
          $poli = 'Semua Poli';
          if (!empty($request->download_poli)) {
            $poli = Poli::find($request->download_poli)->poli;
          }
        return Excel::download(new ReservasiExport($data,$tanggal,$poli), 'Laporan Reservasi-'.$poli.'-'.$tanggal.'.xlsx');
    }
}
