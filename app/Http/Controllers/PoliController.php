<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poli;
use App\Hari;
use App\PoliHari;
use App\Reservasi;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use File;

class PoliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('poli.index');
    }

    public function getData(Request $request)
    {
        $data = Poli::all();
        return datatables()->of($data)
        ->addIndexColumn()
        ->addColumn('action', function($row){
            $btn = '<a href="'.route('poli.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            return $btn;
        })
        ->addColumn('jadwal', function($row){
            $poliHari = PoliHari::where('poli_id',$row->id)->first()->id;
            $btn = '<a href="'.route('sesi.index',$poliHari).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-eye"></i> Kelola Jadwal</a>';
            return $btn;
        })
        ->rawColumns(['action','jadwal'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('poli.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate(request(),
      [
          'poli' => 'required',
          'kode_poli' => 'required',
          'waktu' => 'required',
          'icon' => 'mimes:png,jpg,jpeg|max:800'
      ]
      );
      DB::beginTransaction();
      $poli = new Poli();
      $poli->kode = $request->kode_poli;
      $poli->poli = $request->poli;
      $poli->waktu = $request->waktu;
      if(!empty($request->icon)){
          $file = $request->file('icon');
          $extension = strtolower($file->getClientOriginalExtension());
          $filename = Carbon::now()->format('ymd').rand(1000,9999) . '.' . $extension;
          $img = Image::make($file)->resize(141, 141);
          Storage::put('public/images/icon-poli/' . $filename, $img->encode());
          $poli->icon = $filename;
      }
      $poli->save();

      $hari = Hari::all();
      foreach ($hari as $key => $value) {
        $poliHari = new PoliHari();
        $poliHari->poli_id = $poli->id;
        $poliHari->hari_id = $value->id;
        $poliHari->save();
      }
      DB::commit();
      return redirect()->route('poli.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Poli::find($id);
      return view('poli.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(),
      [
          'poli' => 'required',
          'kode_poli' => 'required',
          'waktu' => 'required'
      ]
      );

      $poli = Poli::find($id);
      $poli->poli = $request->poli;
      $poli->kode = $request->kode_poli;
      $poli->waktu = $request->waktu;
      if (!empty($request->icon)) {
        if (!empty($poli->icon)) {
          Storage::delete('public/images/icon-poli/'.$poli->icon);
        }
        if (!empty($poli->icon)) {
          Storage::delete('public/images/icon-poli/'.$poli->icon);
        }
        $file = $request->file('icon');
        $extension = strtolower($file->getClientOriginalExtension());
        $filename = Carbon::now()->format('ymd').rand(1000,9999) . '.' . $extension;
        $img = Image::make($file)->resize(141, 141);
        Storage::put('public/images/icon-poli/' . $filename, $img->encode());
        $poli->icon = $filename;
      }
      $poli->save();
      return redirect()->route('poli.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $poli = Poli::find($id);
      $cek_reservasi = Reservasi::where('poli_id',$id)->first();
      if (!empty($cek_reservasi)) {
        return response()->json(['status' => 'failed','message' => 'Data terkait dengan data lain.'],200);
      }
      $poli_hari = PoliHari::where('poli_id',$id)->get();
      foreach ($poli_hari as $key => $value) {
        $value->delete();
      }
      $poli->delete();
      return response()->json(['status' => 'success'],200);
    }

    public function api_index()
    {
        $data = Poli::all();
        return response()->json([
          'status'      => 'success',
          'result'      => $data
        ]);
    }
}
