<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalonPasien;
use DB;
use Auth;
use App\Imports\CalonPasienImport;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

class CalonPasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pasien.index');
    }

    public function getData(Request $request)
    {
        $data = CalonPasien::all();
        return datatables()->of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pasien.calon-pasien.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),
        [
        'nama' => 'required',
        'nomor_telepon' => 'required|between:10,14',
        'nik' => 'required|digits:16',
        'kartu_bpjs_atau_kartu_sehat' => 'required',
        // 'nomor_bpjs_atau_kartu_sehat' => '',
        ]
        );
        if ($request->kartu_bpjs_atau_kartu_sehat == 'ada') {
        $nomor_bpjs_atau_kartu_sehat = $request->nomor_bpjs_atau_kartu_sehat;
        } else {
        $nomor_bpjs_atau_kartu_sehat = NULL;
        }

        $calonPasien = new CalonPasien();
        $calonPasien->nama = $request->nama;
        $calonPasien->no_telepon = $request->nomor_telepon;
        $calonPasien->nik = $request->nik;
        $calonPasien->no_bpjs = $nomor_bpjs_atau_kartu_sehat;
        $calonPasien->tipe = 'child';
        $calonPasien->created_by = Auth::user()->id;
        $calonPasien->save();

        return redirect()->route('pasien.list');
    }

    public function import(Request $request)
    {
        $this->validate(request(),
        [
            'file' => 'required',
        ]);
        $file = $request->file('file');
        $import = new CalonPasienImport();
        $import->import($file);
        return redirect()->route('pasien.index');
    }

    public function download()
    {
        return response()->download(public_path('file/Template Import Data Pasien.xlsx'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $data = CalonPasien::find($id);
         return view('pasien.calon-pasien.edit',compact('data'));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(),
      [
          'nama' => 'required',
          'nomor_telepon' => 'required|between:10,14',
          'nik' => 'required|digits:16',
          'kartu_bpjs_atau_kartu_sehat' => 'required',
          // 'nomor_bpjs_atau_kartu_sehat' => '',
      ]
      );
      if ($request->kartu_bpjs_atau_kartu_sehat == 'ada') {
        $nomor_bpjs_atau_kartu_sehat = $request->nomor_bpjs_atau_kartu_sehat;
      } else {
        $nomor_bpjs_atau_kartu_sehat = NULL;
      }

      $calonPasien = CalonPasien::find($id);
      $calonPasien->nama = $request->nama;
      $calonPasien->no_telepon = $request->nomor_telepon;
      $calonPasien->nik = $request->nik;
      $calonPasien->no_bpjs = $nomor_bpjs_atau_kartu_sehat;
      $calonPasien->save();

      return redirect()->route('pasien.list');
    }

    public function isiData($id)
    {
        $data = CalonPasien::find($id);
        return view('pasien.calon-pasien.isi-data',compact('data'));
    }

    public function isiDataPut(Request $request, $id)
    {
      $pasien = $id;
      $this->validate(request(),
      [
          'nama' => 'required',
          'nomor_telepon' => 'required|between:10,14',
          'nik' => 'required|digits:16',
          'kartu_bpjs_atau_kartu_sehat' => 'required',
          // 'nomor_bpjs_atau_kartu_sehat' => '',
      ]
      );
      if ($request->kartu_bpjs_atau_kartu_sehat == 'ada') {
        $nomor_bpjs_atau_kartu_sehat = $request->nomor_bpjs_atau_kartu_sehat;
      } else {
        $nomor_bpjs_atau_kartu_sehat = NULL;
      }

      $calonPasien = CalonPasien::find($id);
      $calonPasien->nama = $request->nama;
      $calonPasien->no_telepon = $request->nomor_telepon;
      $calonPasien->nik = $request->nik;
      $calonPasien->no_bpjs = $nomor_bpjs_atau_kartu_sehat;
      $calonPasien->save();

      return redirect()->route('reservasi.create', compact('pasien'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $user = CalonPasien::find($id);
         $user->delete();
         return response()->json(['data'=>'success delete data']);
     }


     // START OF atoumAssertException

     public function api_index()
     {
         $data = CalonPasien::where('created_by',Auth::user()->id)->get();
         return response()->json([
              'status'      => 'success',
              'result'      => $data
          ]);
     }

     public function api_store(Request $request)
     {
          $this->validate(request(),
          [
            'nama' => 'required',
            'nomor_telepon' => 'required|between:10,14',
            'nik' => 'required|digits:16',
             // 'nomor_bpjs' => 'required',
          ]
          );

          $calonPasien = new CalonPasien();
          $calonPasien->nama = $request->nama;
          $calonPasien->no_telepon = $request->nomor_telepon;
          $calonPasien->nik = $request->nik;
          $calonPasien->no_bpjs = $request->nomor_bpjs_atau_kartu_sehat;
          $calonPasien->tipe = 'child';
          $calonPasien->created_by = Auth::user()->id;
          $calonPasien->save();

          return response()->json([
              'status'      => 'success',
              'result'      => $calonPasien
          ]);
     }

     public function api_update(Request $request, $id)
     {
          $this->validate(request(),
          [
            'nama' => 'required',
            'nomor_telepon' => 'required|between:10,14',
            'nik' => 'required|digits:16',
             // 'nomor_bpjs' => 'required',
          ]
          );

          $calonPasien = CalonPasien::find($id);
          $calonPasien->nama = $request->nama;
          $calonPasien->no_telepon = $request->nomor_telepon;
          $calonPasien->nik = $request->nik;
          $calonPasien->no_bpjs = $request->nomor_bpjs_atau_kartu_sehat;
          $calonPasien->tipe = 'child';
          $calonPasien->created_by = Auth::user()->id;
          $calonPasien->save();

          return response()->json([
              'status'      => 'success',
              'result'      => $calonPasien
          ]);
     }

     public function api_destroy($id)
     {
         $user = CalonPasien::find($id);
         if ($user->tipe == 'root') {
           return response()->json([
               'status'      => 'failed',
               'message'      => 'Tidak bisa menghapus data.'
           ]);
         } else {
           $user->delete();
           return response()->json([
             'status'       => 'success',
             'message'      => 'Data berhasil dihapus.'
           ]);
         }
     }
}
