<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalonPasien;
use App\Reservasi;
use App\JawabanCalonPasien;
use App\HasilSkor;
use App\JamSesi;
use App\Jam;
use App\Sesi;
use App\Poli;
use App\Hari;
use App\PoliHari;
use App\HasilScreening;
use Auth;
use DB;
use Redirect;
use Carbon\Carbon;
Use PDF;

class ReservasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getData(Request $request)
    {
        $data = CalonPasien::where('created_by',Auth::user()->id)->get();
        return datatables()->of($data)
        ->addColumn('action', function($row){
            $btn = '<a href="'.route('calon-pasien.edit',$row->id).'" class="btn border-info btn-xs text-info-600 btn-flat btn-icon"><i class="icon-pencil6"></i></a>';
            if ($row->tipe == 'child') {
                $btn = $btn.'  <button id="delete" class="btn border-warning btn-xs text-warning-600 btn-flat btn-icon"><i class="icon-trash"></i></button>';
            }
            return $btn;
        })
        ->addColumn('antrian', function($row){
            $btn = '<a href="'.route('reservasi.create',$row->id).'" class="btn border-primary btn-xs text-primary-600 btn-flat btn-icon"><i class=" icon-checkmark4"></i></a>';
            return $btn;
        })
        ->rawColumns(['antrian','action'])
        ->make(true);
    }

    public function listCalonPasien()
    {
        $pasien = CalonPasien::where('created_by',Auth::user()->id)->orderBy('nama')->get();
        return view('pasien.list', compact('pasien'));
    }

    public function index()
    {
        $reservasi = Reservasi::whereHas('calonPasien', function ($query){
                                    $query->where('created_by', Auth::user()->id);
                                })->with('calonPasien')->orderBy('tanggal','DESC')->orderBy('antrian','DESC')->orderByRaw('status = ? desc','Reservasi')
                                ->paginate(5);
                                // ->get();
        foreach ($reservasi as $key => $value) {
          // $value->skor_screening = JawabanCalonPasien::
          //             whereHas('reservasi.calonPasien', function ($query){
          //               $query->where('created_by', Auth::user()->id);
          //             })
          //           ->groupBy('reservasi_id')
          //           ->selectRaw('reservasi_id, sum(skor) as skor')
          //           ->first()->skor;
          // $value->hasil_screening = HasilSkor::
          //             where('batas_bawah','<=',$value->skor_screening)
          //           ->where('batas_atas','>=',$value->skor_screening)
          //           ->first()->hasil;
          $value->hasil_screening = HasilScreening::with('hasilSkor')->where('reservasi_id',$value->id)->first();
          $value->day = Carbon::parse($value->tanggal)->format('l');
          $value->sesi_mulai = Sesi::whereHas('poliHari.poli', function ($query) use ($value){
                                    $query->where('id', $value->poli_id);
                    })->whereHas('poliHari.hari', function ($query) use ($value){
                                    $query->where('day', $value->day);
                    })->orderBy('mulai','ASC')->first()->mulai;
        }
        return view('pasien.reservasi.index', compact('reservasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $data = CalonPasien::find($request->pasien);
      $lastReservasi = Reservasi::where('pasien_id', $data->id)->orderBy('created_at', 'DESC')->first();
      $poli = Poli::all();
      Carbon::setLocale('id');

      foreach ($poli as $p) {
        $counter = 0;
        for ($i=1; $i <=7 ; $i++) {
          if (date('l',strtotime(' +'.$i.' day')) === 'Sunday') {
            ${'hari'.$i} = date('Y-m-d', strtotime(' +'.($i+1).' day'));
            $counter += 1;
          } else {
            ${'hari'.$i} = date('Y-m-d', strtotime(' +'.($i+$counter).' day'));
          }
          ${'reservasi_hari'.$i} = Reservasi::where('poli_id',$p->id)->where('tanggal',${'hari'.$i})->get();
          ${'cek_hari'.$i} = Hari::where('day',date('l',strtotime(${'hari'.$i})))->first();
          ${'cek_jadwal_hari'.$i} = PoliHari::where('poli_id',$p->id)->where('hari_id',${'cek_hari'.$i}->id)->pluck('id');
          ${'cek_pelayanan_hari'.$i} = Sesi::whereIn('poli_hari_id',${'cek_jadwal_hari'.$i})->get();
          ${'sesi_hari'.$i} = Sesi::where('poli_hari_id',${'cek_jadwal_hari'.$i})->get();
          ${'max_pelayanan_hari'.$i} = 0;
        }
      
        foreach ($sesi_hari1 as $value) {
          $max_pelayanan_hari1 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari2 as $value) {
          $max_pelayanan_hari2 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari3 as $value) {
          $max_pelayanan_hari3 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari4 as $value) {
          $max_pelayanan_hari4 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari5 as $value) {
          $max_pelayanan_hari5 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari6 as $value) {
          $max_pelayanan_hari6 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari7 as $value) {
          $max_pelayanan_hari7 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        $max_pelayanan = 0;
        $jml_reservasi = 0;
        
        for ($i=1; $i <=7 ; $i++) { 
          $max_pelayanan += ${'max_pelayanan_hari'.$i};
          $jml_reservasi += ${'reservasi_hari'.$i}->count();
        }

        if ($jml_reservasi == $max_pelayanan) {
          $p->color = 'bg-grey-300';
        } else {
          $p->color = 'bg-teal-300';
        }
      }
      if ($data->nik) {
        return view('pasien.reservasi.create', compact('data','poli','lastReservasi'));
      } else {
        return redirect()->route('calon-pasien.isiData',$data->id);
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate(request(),
        [
            'tanggal' => 'required',
            // 'jam' => 'required',
            // 'sesi' => 'required',
            'keluhan' => 'required',
            // 'kekhawatiran' => 'required',
            // 'riwayat_penyakit_menahun' => 'required',
            // 'upaya_pengobatan' => 'required',
            'poli'=>'required'
        ]);
        DB::beginTransaction();
        $cek = Reservasi::where('pasien_id',$id)->where('tanggal',$request->tanggal)->whereNotIn('status',['Batal'])->first();
        if ($cek) {
          return Redirect::back()->withErrors(['Anda sudah melakukan pendaftaran pada tanggal yang dipilih.']);
        }
        $poli = Poli::find($request->poli);
        $antrian = Reservasi::where('tanggal',$request->tanggal)->where('poli_id',$request->poli)->count() + 1;
        $reservasi = new Reservasi;
        $reservasi->pasien_id = $id;
        $reservasi->keluhan = $request->keluhan;
        $reservasi->kekhawatiran = $request->kekhawatiran;
        $reservasi->poli_id = $request->poli;
        $reservasi->riwayat_penyakit_menahun = $request->riwayat_penyakit_menahun;
        $reservasi->upaya_pengobatan = $request->upaya_pengobatan;
        $reservasi->status = 'Reservasi';
        $reservasi->tanggal = $request->tanggal;
        $reservasi->antrian = $poli->kode.$antrian;
        $reservasi->kode = $poli->kode.'RSV'.strtoupper(substr(md5(microtime()),rand(0,26),5));
        $reservasi->save();

        $reservasi->perkiraan_jam_pelayanan = $this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id);
        $reservasi->save();
        DB::commit();
        return redirect()->route('screening.create',$reservasi->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservasi = Reservasi::with('calonPasien')->find($id);
        $hasil = HasilScreening::with('hasilSkor')->where('reservasi_id',$reservasi->id)->first();
        return view('pasien.reservasi.antrian', compact('reservasi','hasil'));
    }

    public static function perkiraan_pelayanan($antrian,$tanggal,$poli)
    {
        $hari = Hari::where('day',date('l', strtotime($tanggal)))->first();
        $cek_jam = PoliHari::where('poli_id',$poli)->where('hari_id',$hari->id)->pluck('id');
        $cek_perkiraan_pelayanan = Sesi::whereIn('poli_hari_id',$cek_jam)->orderBy('mulai')->get();

        $sorting_reservasi = Reservasi::where('tanggal',$tanggal)->where('poli_id',$poli)
                            ->where('status','Reservasi')->orderBy('antrian','asc')
                            ->pluck('antrian')->toArray();

        //sorting nomor antrian dari paling kecil
        //sort($sorting_reservasi, SORT_FLAG_CASE);

        $jumlah_max_pasien = [];
        $jumlah_pasien = 0;
        $antrian_ke_index = array_search($antrian, $sorting_reservasi); //cari antrian pada index berapa di array

        $waktu = Poli::find($poli);
        //cari jumlah pasien yg dilayani tiap sesi
        foreach ($cek_perkiraan_pelayanan as $key => $value) {
          $jumlah_pasien += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
          $jumlah_max_pasien[$value->sesi] = $jumlah_pasien;
        }

        //dibalik arraynya dari yg paling banyak valuenya
        $reverse_jumlah_max_pasien = array_reverse($jumlah_max_pasien);

        //cek index antrian yang didapat pada sesi berapa
        foreach ($reverse_jumlah_max_pasien as $key => $value) {
          if ($antrian_ke_index < $value ) {
            $cek_antrian_pada_sesi = $key;
            unset($reverse_jumlah_max_pasien[$key]);
            $urutan = $antrian_ke_index - array_sum($reverse_jumlah_max_pasien);
          }
        }

        $jam_buka = Sesi::whereIn('poli_hari_id',$cek_jam)->where('sesi',$cek_antrian_pada_sesi)->first();

        if ($urutan < $jam_buka->jumlah_dokter) {
          $menit = 0 * $waktu->waktu / $jam_buka->jumlah_dokter;
        } else {
          $menit = $urutan * $waktu->waktu / $jam_buka->jumlah_dokter;
        }

        $perkiraan = date('H:i', strtotime('+'.$menit.' minutes', strtotime($jam_buka->mulai)));
        return $perkiraan;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function batal($reservasi_id)
    {
        $reservasi = Reservasi::find($reservasi_id);
        $day = Carbon::parse($reservasi->tanggal)->format('l');
        $sesi_mulai = Sesi::whereHas('poliHari.poli', function ($query) use ($reservasi){
                                            $query->where('id', $reservasi->poli_id);
                             })->whereHas('poliHari.hari', function ($query) use ($day){
                                            $query->where('day', $day);
                             })->orderBy('mulai','ASC')->first()->mulai;
        if (Carbon::parse($reservasi->tanggal . $sesi_mulai)->addMinutes(-120) < Carbon::now()){
          return response()->json(['data'=>'error']);
        }
        $reservasi->status = "Batal";
        if ($reservasi->calonPasien->created_by == 0) {
          $reservasi->status_updated_by = 0;
        } else {
          $reservasi->status_updated_by = Auth::user()->id;
        }
        $reservasi->save();
        return response()->json(['data'=>'success updated data']);
    }

    public function poli($poli)
    {
      Carbon::setLocale('id');
      $counter = 0;
      for ($i=1; $i <=7 ; $i++) {
        if (date('l',strtotime(' +'.$i.' day')) === 'Sunday') {
          ${'hari'.$i} = date('Y-m-d', strtotime(' +'.($i+1).' day'));
          $counter += 1;
        } else {
          ${'hari'.$i} = date('Y-m-d', strtotime(' +'.($i+$counter).' day'));
        }
      }

      for ($i=1; $i <=7 ; $i++) {
        ${'reservasi_hari'.$i} = Reservasi::where('poli_id',$poli)->where('tanggal',${'hari'.$i})->get();
      }
      $data = [];

      for ($i=1; $i <=7 ; $i++) {
        ${'cek_hari'.$i} = Hari::where('day',date('l',strtotime(${'hari'.$i})))->first();
      }

      for ($i=1; $i <=7 ; $i++) {
        ${'cek_jadwal_hari'.$i} = PoliHari::where('poli_id',$poli)->where('hari_id',${'cek_hari'.$i}->id)->pluck('id');
      }

      for ($i=1; $i <=7 ; $i++) {
        ${'cek_pelayanan_hari'.$i} = Sesi::whereIn('poli_hari_id',${'cek_jadwal_hari'.$i})->get();
      }

      for ($i=1; $i <=7 ; $i++) {
        ${'sesi_hari'.$i} = Sesi::where('poli_hari_id',${'cek_jadwal_hari'.$i})->get();
      }

      for ($i=1; $i <=7 ; $i++) {
        ${'max_pelayanan_hari'.$i} = 0;
      }

      $waktu = Poli::find($poli);
      foreach ($sesi_hari1 as $value) {
        $max_pelayanan_hari1 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari2 as $value) {
        $max_pelayanan_hari2 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari3 as $value) {
        $max_pelayanan_hari3 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari4 as $value) {
        $max_pelayanan_hari4 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari5 as $value) {
        $max_pelayanan_hari5 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari6 as $value) {
        $max_pelayanan_hari6 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari7 as $value) {
        $max_pelayanan_hari7 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      if (!$cek_pelayanan_hari1->isEmpty()) {
        if ($reservasi_hari1->count() <= $max_pelayanan_hari1) {
          if ($reservasi_hari1->count() == $max_pelayanan_hari1) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari1)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari1,'keterangan'=>Carbon::createFromDate($hari1)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari1)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if (!$cek_pelayanan_hari2->isEmpty()) {
        if ($reservasi_hari2->count() <= $max_pelayanan_hari2) {
          if ($reservasi_hari2->count() == $max_pelayanan_hari2) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari2)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari2,'keterangan'=>Carbon::createFromDate($hari2)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari2)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if (!$cek_pelayanan_hari3->isEmpty()) {
        if ($reservasi_hari3->count() <= $max_pelayanan_hari3) {
          if ($reservasi_hari3->count() == $max_pelayanan_hari3) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari3)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari3,'keterangan'=>Carbon::createFromDate($hari3)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari3)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if (!$cek_pelayanan_hari4->isEmpty()) {
        if ($reservasi_hari4->count() <= $max_pelayanan_hari4) {
          if ($reservasi_hari4->count() == $max_pelayanan_hari4) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari4)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari4,'keterangan'=>Carbon::createFromDate($hari4)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari4)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if (!$cek_pelayanan_hari5->isEmpty()) {
        if ($reservasi_hari5->count() <= $max_pelayanan_hari5) {
          if ($reservasi_hari5->count() == $max_pelayanan_hari5) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari5)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari5,'keterangan'=>Carbon::createFromDate($hari5)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari5)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if (!$cek_pelayanan_hari6->isEmpty()) {
        if ($reservasi_hari6->count() <= $max_pelayanan_hari6) {
          if ($reservasi_hari6->count() == $max_pelayanan_hari6) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari6)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari6,'keterangan'=>Carbon::createFromDate($hari6)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari6)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if (!$cek_pelayanan_hari7->isEmpty()) {
        if ($reservasi_hari7->count() <= $max_pelayanan_hari7) {
          if ($reservasi_hari7->count() == $max_pelayanan_hari7) {
            $color = 'bg-grey-300';
          } else {
            $color = 'bg-teal-300';
          }
          $hari = Hari::where('day',date('l', strtotime($hari7)))->first()->id;
          $dokter = PoliHari::with('sesi.dokterSesi.dokter')->where('poli_id',$poli)->where('hari_id',$hari)->get();
          $data[] = ['color'=>$color,'tanggal'=>$hari7,'keterangan'=>Carbon::createFromDate($hari7)->translatedFormat('j F Y'),'hari'=>Carbon::createFromDate($hari7)->translatedFormat('l'),'dokter'=>$dokter];
        }
      }

      if ($cek_pelayanan_hari1->isEmpty() && $cek_pelayanan_hari2->isEmpty() && $cek_pelayanan_hari3->isEmpty() && $cek_pelayanan_hari4->isEmpty() && $cek_pelayanan_hari5->isEmpty() && $cek_pelayanan_hari6->isEmpty() && $cek_pelayanan_hari7->isEmpty()) {
          $data = 'Reservasi untuk 7 hari kedepan sudah penuh.';
      }

      return response()->json($data);

    }

    public function tanggal($poli,$tanggal)
    {
      $reservasi = Reservasi::where('tanggal',$tanggal)->where('poli_id',$poli)->pluck('waktu');
      $data = JamSesi::whereNotIn('id',$reservasi)->distinct()->get(['jam_id']);
      $data = Jam::whereIn('id',$data)->get();

      return response()->json($data);

    }

    public function jam($poli,$tanggal,$jam)
    {
        $reservasi = Reservasi::where('tanggal',$tanggal)->where('poli_id',$poli)->pluck('waktu');
        $data = JamSesi::whereNotIn('id',$reservasi)->where('jam_id',$jam)->distinct()->get(['sesi_id']);
        $data = Sesi::whereIn('id',$data)->get();

        return response()->json($data);
    }

    // PASIEN TANPA LOGIN
    public function reservasiNoLogin(Request $request)
    {
      $poli = Poli::all();
      Carbon::setLocale('id');

      foreach ($poli as $p) {
        $counter = 0;
        for ($i=1; $i <=7 ; $i++) {
          if (date('l',strtotime(' +'.$i.' day')) === 'Sunday') {
            ${'hari'.$i} = date('Y-m-d', strtotime(' +'.($i+1).' day'));
            $counter += 1;
          } else {
            ${'hari'.$i} = date('Y-m-d', strtotime(' +'.($i+$counter).' day'));
          }
          ${'reservasi_hari'.$i} = Reservasi::where('poli_id',$p->id)->where('tanggal',${'hari'.$i})->get();
          ${'cek_hari'.$i} = Hari::where('day',date('l',strtotime(${'hari'.$i})))->first();
          ${'cek_jadwal_hari'.$i} = PoliHari::where('poli_id',$p->id)->where('hari_id',${'cek_hari'.$i}->id)->pluck('id');
          ${'cek_pelayanan_hari'.$i} = Sesi::whereIn('poli_hari_id',${'cek_jadwal_hari'.$i})->get();
          ${'sesi_hari'.$i} = Sesi::where('poli_hari_id',${'cek_jadwal_hari'.$i})->get();
          ${'max_pelayanan_hari'.$i} = 0;
        }
      
        foreach ($sesi_hari1 as $value) {
          $max_pelayanan_hari1 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari2 as $value) {
          $max_pelayanan_hari2 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari3 as $value) {
          $max_pelayanan_hari3 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari4 as $value) {
          $max_pelayanan_hari4 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari5 as $value) {
          $max_pelayanan_hari5 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari6 as $value) {
          $max_pelayanan_hari6 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        foreach ($sesi_hari7 as $value) {
          $max_pelayanan_hari7 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $p->waktu * $value->jumlah_dokter;
        }

        $max_pelayanan = 0;
        $jml_reservasi = 0;
        
        for ($i=1; $i <=7 ; $i++) { 
          $max_pelayanan += ${'max_pelayanan_hari'.$i};
          $jml_reservasi += ${'reservasi_hari'.$i}->count();
        }

        if ($jml_reservasi == $max_pelayanan) {
          $p->color = 'bg-grey-300';
        } else {
          $p->color = 'bg-teal-300';
        }
      }
      return view('pasien.no-login.reservasi', compact('poli'));
    }

    public function reservasiNoLoginStore(Request $request)
    {
      $this->validate(request(),
      [
          'nama' => 'required',
          'nomor_telepon' => 'required|between:10,14',
          'nik' => 'required|digits:16',
          'kartu_bpjs_atau_kartu_sehat' => 'required',
          'tanggal' => 'required',
          'keluhan' => 'required',
          'poli'=>'required'
      ]);
      DB::beginTransaction();
      // START OF TAMBAH DATA CALON PASIEN
      if ($request->kartu_bpjs_atau_kartu_sehat == 'ada') {
        $nomor_bpjs_atau_kartu_sehat = $request->nomor_bpjs_atau_kartu_sehat;
      } else {
        $nomor_bpjs_atau_kartu_sehat = NULL;
      }
      $calonPasien = new CalonPasien();
      $calonPasien->nama = $request->nama;
      $calonPasien->no_telepon = $request->nomor_telepon;
      $calonPasien->nik = $request->nik;
      $calonPasien->no_bpjs = $nomor_bpjs_atau_kartu_sehat;
      $calonPasien->tipe = 'child';
      $calonPasien->created_by = 0;
      $calonPasien->save();
      // END OF TAMBAH DATA CALON PASIEN

      // START OF TAmBAH RESERVASI
      $poli = Poli::find($request->poli);
      $antrian = Reservasi::where('tanggal',$request->tanggal)->where('poli_id',$request->poli)->count() + 1;
      $reservasi = new Reservasi;
      $reservasi->pasien_id = $calonPasien->id;
      $reservasi->keluhan = $request->keluhan;
      $reservasi->kekhawatiran = $request->kekhawatiran;
      $reservasi->poli_id = $request->poli;
      $reservasi->riwayat_penyakit_menahun = $request->riwayat_penyakit_menahun;
      $reservasi->upaya_pengobatan = $request->upaya_pengobatan;
      $reservasi->status = 'Reservasi';
      $reservasi->tanggal = $request->tanggal;
      $reservasi->antrian = $poli->kode.$antrian;
      $reservasi->kode = $poli->kode.'RSV'.strtoupper(substr(md5(microtime()),rand(0,26),5));
      $reservasi->save();

      $reservasi->perkiraan_jam_pelayanan = $this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id);
      $reservasi->save();
      DB::commit();
      return redirect()->route('screening.create',$reservasi->id);
    }

    public function reservasiNoLoginShow($kode)
    {
        $reservasi = Reservasi::with('calonPasien')->where('kode',$kode)->first();
        $hasil = HasilScreening::with('hasilSkor')->where('reservasi_id',$reservasi->id)->first();
        return view('pasien.no-login.antrian', compact('reservasi','hasil'));
    }

    public function reservasiNoLoginUnduh($kode){
       $data = Reservasi::with('calonPasien')->where('kode',$kode)->first();
       $hasil = HasilScreening::with('hasilSkor')->where('reservasi_id',$data->id)->first();
       $pdf = PDF::setPaper(array(0,0,300,410),'potrait')->loadView('pasien.no-login.antrian-unduh', compact('data','hasil'));
       return $pdf->stream('Antrian-'.$data->kode.'.pdf');
     }


    public function api_list()
    {
        $reservasi = Reservasi::whereHas('calonPasien', function ($query){
                                    $query->where('created_by', Auth::user()->id);
                                })->with('calonPasien','poli')->orderBy('tanggal','DESC')->orderBy('perkiraan_jam_pelayanan','DESC')->get();
        foreach ($reservasi as $key => $value) {
          $value->skor_screening = JawabanCalonPasien::
                      whereHas('reservasi.calonPasien', function ($query){
                        $query->where('created_by', Auth::user()->id);
                      })
                    ->groupBy('reservasi_id')
                    ->selectRaw('reservasi_id, sum(skor) as skor')
                    ->first()->skor;
          $value->hasil_screening = HasilSkor::
                      where('batas_bawah','<=',$value->skor_screening)
                    ->where('batas_atas','>=',$value->skor_screening)
                    ->first()->hasil;
        }
        return response()->json([
          'status'      => 'success',
          'reservasi'      => $reservasi,
        ]);
    }

    public function api_detail($id)
    {
        $reservasi = Reservasi::with('calonPasien','poli')->find($id);
        $skor = JawabanCalonPasien::where('calon_pasien_id',$reservasi->pasien_id)
                                  ->where('reservasi_id', $reservasi->id)
                                  ->groupBy('reservasi_id','skor')
                                  ->selectRaw('reservasi_id, sum(skor) as skor')
                                  ->first();
        $hasil = HasilSkor::where('batas_bawah','<=',$skor->skor)
                           ->where('batas_atas','>=',$skor->skor)
                           ->first();
         return response()->json([
           'status'      => 'success',
           'reservasi'      => $reservasi,
           'screening'      => [
                'skor' => $skor->skor,
                'hasil' => $hasil->hasil,
              ],
         ]);
    }

    public function api_store(Request $request, $id)
    {
        $this->validate(request(),
        [
            'tanggal' => 'required',
            'keluhan' => 'required',
            'poli'=>'required'
        ]);
        $cek = Reservasi::where('pasien_id',$id)->where('tanggal',$request->tanggal)->whereNotIn('status',['Batal'])->first();
        if ($cek) {
          return response()->json([
            'status'       => 'failed',
            'message'      => 'Anda sudah melakukan pendaftaran pada tanggal yang dipilih.',
          ]);
        }
        $poli = Poli::find($request->poli);
        $antrian = Reservasi::where('tanggal',$request->tanggal)->where('poli_id',$request->poli)->count() + 1;
        $reservasi = new Reservasi;
        $reservasi->pasien_id = $id;
        $reservasi->keluhan = $request->keluhan;
        $reservasi->kekhawatiran = $request->kekhawatiran;
        $reservasi->poli_id = $request->poli;
        $reservasi->riwayat_penyakit_menahun = $request->riwayat_penyakit_menahun;
        $reservasi->upaya_pengobatan = $request->upaya_pengobatan;
        $reservasi->status = 'Reservasi';
        $reservasi->tanggal = $request->tanggal;
        $reservasi->antrian = $poli->kode.$antrian;
        $reservasi->kode = $poli->kode.'RSV'.strtoupper(substr(md5(microtime()),rand(0,26),5));
        $reservasi->save();

        $reservasi->perkiraan_jam_pelayanan = $this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id);
        $reservasi->save();
        DB::commit();
        $reservasi = Reservasi::find($reservasi->id);
        return response()->json([
          'status'      => 'success',
          'reservasi'      => $reservasi,
        ]);
    }

    public function api_tanggal($poli)
    {
      $hari1 = date('l',strtotime(' +1 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +2 day')) : date('Y-m-d', strtotime(' +1 day'));
      $hari2 = date('l',strtotime(' +2 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +3 day')) : date('Y-m-d', strtotime(' +2 day'));
      $hari3 = date('l',strtotime(' +3 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +4 day')) : date('Y-m-d', strtotime(' +3 day'));
      $hari4 = date('l',strtotime(' +4 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +5 day')) : date('Y-m-d', strtotime(' +4 day'));
      $hari5 = date('l',strtotime(' +5 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +6 day')) : date('Y-m-d', strtotime(' +5 day'));
      $hari6 = date('l',strtotime(' +6 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +7 day')) : date('Y-m-d', strtotime(' +6 day'));
      $hari7 = date('l',strtotime(' +7 day')) === 'Sunday' ? date('Y-m-d', strtotime(' +8 day')) : date('Y-m-d', strtotime(' +7 day'));

      $reservasi_hari1 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari1)->get();
      $reservasi_hari2 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari2)->get();
      $reservasi_hari3 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari3)->get();
      $reservasi_hari4 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari4)->get();
      $reservasi_hari5 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari5)->get();
      $reservasi_hari6 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari6)->get();
      $reservasi_hari7 = Reservasi::where('poli_id',$poli)->where('tanggal',$hari7)->get();
      $data = [];

      $cek_hari1 = Hari::where('day',date('l',strtotime($hari1)))->first();
      $cek_hari2 = Hari::where('day',date('l',strtotime($hari2)))->first();
      $cek_hari3 = Hari::where('day',date('l',strtotime($hari3)))->first();
      $cek_hari4 = Hari::where('day',date('l',strtotime($hari4)))->first();
      $cek_hari5 = Hari::where('day',date('l',strtotime($hari5)))->first();
      $cek_hari6 = Hari::where('day',date('l',strtotime($hari6)))->first();
      $cek_hari7 = Hari::where('day',date('l',strtotime($hari7)))->first();

      $cek_jadwal_hari1 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari1->id)->pluck('id');
      $cek_jadwal_hari2 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari2->id)->pluck('id');
      $cek_jadwal_hari3 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari3->id)->pluck('id');
      $cek_jadwal_hari4 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari4->id)->pluck('id');
      $cek_jadwal_hari5 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari5->id)->pluck('id');
      $cek_jadwal_hari6 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari6->id)->pluck('id');
      $cek_jadwal_hari7 = PoliHari::where('poli_id',$poli)->where('hari_id',$cek_hari7->id)->pluck('id');

      $cek_pelayanan_hari1 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari1)->get();
      $cek_pelayanan_hari2 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari2)->get();
      $cek_pelayanan_hari3 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari3)->get();
      $cek_pelayanan_hari4 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari4)->get();
      $cek_pelayanan_hari5 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari5)->get();
      $cek_pelayanan_hari6 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari6)->get();
      $cek_pelayanan_hari7 = Sesi::whereIn('poli_hari_id',$cek_jadwal_hari7)->get();

      $sesi_hari1 = Sesi::where('poli_hari_id',$cek_jadwal_hari1)->get();
      $sesi_hari2 = Sesi::where('poli_hari_id',$cek_jadwal_hari2)->get();
      $sesi_hari3 = Sesi::where('poli_hari_id',$cek_jadwal_hari3)->get();
      $sesi_hari4 = Sesi::where('poli_hari_id',$cek_jadwal_hari4)->get();
      $sesi_hari5 = Sesi::where('poli_hari_id',$cek_jadwal_hari5)->get();
      $sesi_hari6 = Sesi::where('poli_hari_id',$cek_jadwal_hari6)->get();
      $sesi_hari7 = Sesi::where('poli_hari_id',$cek_jadwal_hari7)->get();

      $max_pelayanan_hari1 = 0;
      $max_pelayanan_hari2 = 0;
      $max_pelayanan_hari3 = 0;
      $max_pelayanan_hari4 = 0;
      $max_pelayanan_hari5 = 0;
      $max_pelayanan_hari6 = 0;
      $max_pelayanan_hari7 = 0;

      $waktu = Poli::find($poli);
      foreach ($sesi_hari1 as $value) {
        $max_pelayanan_hari1 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari2 as $value) {
        $max_pelayanan_hari2 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari3 as $value) {
        $max_pelayanan_hari3 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari4 as $value) {
        $max_pelayanan_hari4 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari5 as $value) {
        $max_pelayanan_hari5 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari6 as $value) {
        $max_pelayanan_hari6 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      foreach ($sesi_hari7 as $value) {
        $max_pelayanan_hari7 += (strtotime($value->selesai) - strtotime($value->mulai)) / 60 / $waktu->waktu * $value->jumlah_dokter;
      }

      if (!$cek_pelayanan_hari1->isEmpty()) {
        if ($reservasi_hari1->count() < $max_pelayanan_hari1) {
          $data[] = ['tanggal'=>$hari1,'keterangan'=>date('d F Y', strtotime($hari1))];
        }
      }

      if (!$cek_pelayanan_hari2->isEmpty()) {
        if ($reservasi_hari2->count() < $max_pelayanan_hari2) {
          $data[] = ['tanggal'=>$hari2,'keterangan'=>date('d F Y', strtotime($hari2))];
        }
      }

      if (!$cek_pelayanan_hari3->isEmpty()) {
        if ($reservasi_hari3->count() < $max_pelayanan_hari3) {
          $data[] = ['tanggal'=>$hari3,'keterangan'=>date('d F Y', strtotime($hari3))];
        }
      }

      if (!$cek_pelayanan_hari4->isEmpty()) {
        if ($reservasi_hari4->count() < $max_pelayanan_hari4) {
          $data[] = ['tanggal'=>$hari4,'keterangan'=>date('d F Y', strtotime($hari4))];
        }
      }

      if (!$cek_pelayanan_hari5->isEmpty()) {
        if ($reservasi_hari5->count() < $max_pelayanan_hari5) {
          $data[] = ['tanggal'=>$hari5,'keterangan'=>date('d F Y', strtotime($hari5))];
        }
      }

      if (!$cek_pelayanan_hari6->isEmpty()) {
        if ($reservasi_hari6->count() < $max_pelayanan_hari6) {
          $data[] = ['tanggal'=>$hari6,'keterangan'=>date('d F Y', strtotime($hari6))];
        }
      }

      if (!$cek_pelayanan_hari7->isEmpty()) {
        if ($reservasi_hari7->count() < $max_pelayanan_hari7) {
          $data[] = ['tanggal'=>$hari7,'keterangan'=>date('d F Y', strtotime($hari7))];
        }
      }

      if ($cek_pelayanan_hari1->isEmpty() && $cek_pelayanan_hari2->isEmpty() && $cek_pelayanan_hari3->isEmpty() && $cek_pelayanan_hari4->isEmpty() && $cek_pelayanan_hari5->isEmpty() && $cek_pelayanan_hari6->isEmpty() && $cek_pelayanan_hari7->isEmpty()) {
          $data = 'Reservasi untuk 7 hari kedepan sudah penuh.';
          return response()->json([
            'status'      => 'failed',
            'result'      => $data,
          ]);
      }

      return response()->json([
        'status'      => 'success',
        'result'      => $data,
      ]);
    }

    public function api_batal($reservasi_id)
    {
        $reservasi = Reservasi::find($reservasi_id);
        $day = Carbon::parse($reservasi->tanggal)->format('l');
        $sesi_mulai = Sesi::whereHas('poliHari.poli', function ($query) use ($reservasi){
                                            $query->where('id', $reservasi->poli_id);
                             })->whereHas('poliHari.hari', function ($query) use ($day){
                                            $query->where('day', $day);
                             })->orderBy('mulai','ASC')->first()->mulai;
        if (Carbon::parse($reservasi->tanggal . $sesi_mulai)->addMinutes(-120) < Carbon::now()){
          return response()->json([
            'status'      => 'failed',
            'result'      => 'Pembatalan hanya bisa dilakukan 2 jam sebelum klinik dibuka.',
          ]);
        }
        $reservasi->status = "Batal";
        if ($reservasi->calonPasien->created_by != 0) {
          $reservasi->status_updated_by = Auth::user()->id;
        } else {
          $reservasi->status_updated_by = 0;
        }
        $reservasi->save();
        return response()->json([
          'status'      => 'success',
          'result'      => $reservasi,
        ]);
    }

    public function api_reservasiNoLoginStore(Request $request)
    {
      $this->validate(request(),
      [
          'nama' => 'required',
          'nomor_telepon' => 'required|between:10,14',
          'nik' => 'required|digits:16',
          'tanggal' => 'required',
          'keluhan' => 'required',
          'poli'=>'required'
      ]);
      DB::beginTransaction();
      // START OF TAMBAH DATA CALON PASIEN
      $calonPasien = new CalonPasien();
      $calonPasien->nama = $request->nama;
      $calonPasien->no_telepon = $request->nomor_telepon;
      $calonPasien->nik = $request->nik;
      $calonPasien->no_bpjs = $request->nomor_bpjs_atau_kartu_sehat;
      $calonPasien->tipe = 'child';
      $calonPasien->created_by = 0;
      $calonPasien->save();
      // END OF TAMBAH DATA CALON PASIEN

      // START OF TAmBAH RESERVASI
      $poli = Poli::find($request->poli);
      $antrian = Reservasi::where('tanggal',$request->tanggal)->where('poli_id',$request->poli)->count() + 1;
      $reservasi = new Reservasi;
      $reservasi->pasien_id = $calonPasien->id;
      $reservasi->keluhan = $request->keluhan;
      $reservasi->kekhawatiran = $request->kekhawatiran;
      $reservasi->poli_id = $request->poli;
      $reservasi->riwayat_penyakit_menahun = $request->riwayat_penyakit_menahun;
      $reservasi->upaya_pengobatan = $request->upaya_pengobatan;
      $reservasi->status = 'Reservasi';
      $reservasi->tanggal = $request->tanggal;
      $reservasi->antrian = $poli->kode.$antrian;
      $reservasi->kode = $poli->kode.'RSV'.strtoupper(substr(md5(microtime()),rand(0,26),5));
      $reservasi->save();

      $reservasi->perkiraan_jam_pelayanan = $this->perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id);
      $reservasi->save();
      $data = Reservasi::with('calonPasien')->where('id',$reservasi->id)->first();
      DB::commit();
      return response()->json([
        'status'      => 'success',
        'reservasi'      => $data,
      ]);
    }

    public function api_findByCode($kode)
    {
        $reservasi = Reservasi::with('calonPasien','poli')->where('kode',$kode)->first();
        $skor = JawabanCalonPasien::where('calon_pasien_id',$reservasi->pasien_id)
                                  ->where('reservasi_id', $reservasi->id)
                                  ->groupBy('reservasi_id','skor')
                                  ->selectRaw('reservasi_id, sum(skor) as skor')
                                  ->first();
        $hasil = HasilSkor::where('batas_bawah','<=',$skor->skor)
                           ->where('batas_atas','>=',$skor->skor)
                           ->first();
         return response()->json([
           'status'      => 'success',
           'reservasi'      => $reservasi,
           'screening'      => [
                'skor' => $skor->skor,
                'hasil' => $hasil->hasil,
              ],
         ]);
    }
}
