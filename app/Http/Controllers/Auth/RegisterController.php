<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\CalonPasien;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Auth;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'bpjs_atau_kartu_sehat' => ['required'],
            'nik' => ['required','digits:16'],
            'nomor_telepon' => ['required','between:10,14'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        if ($data['bpjs_atau_kartu_sehat'] == 'ada') {
          $nomor_bpjs_atau_kartu_sehat = $data['nomor_bpjs_atau_kartu_sehat'];
        } else {
          $nomor_bpjs_atau_kartu_sehat = NULL;
        }
        $user = User::create([
            'name' => $data['nama'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ])->assignRole('Calon Pasien');

        $calonPasien = CalonPasien::where('nik',$data['nik'])->first();
        if (!empty($calonPasien)) {
            $calonPasien->created_by = $user->id;
            $calonPasien->tipe = 'root';
            $calonPasien->no_bpjs = $nomor_bpjs_atau_kartu_sehat;
            $calonPasien->save();
        } else {
            CalonPasien::create([
                'nama' => $data['nama'],
                'no_bpjs' => $nomor_bpjs_atau_kartu_sehat,
                'tipe' => 'root',
                'no_telepon' => $data['nomor_telepon'],
                'nik' => $data['nik'],
                'created_by' => $user->id
            ]);
        }
        DB::commit();
        return $user;
    }

    protected function redirectTo()
    {
        $data = Auth::user();
        return route('pasien.list', compact('data'));
    }
}
