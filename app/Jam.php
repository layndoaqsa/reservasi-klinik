<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jam extends Model
{
    protected $table = 'jam';
    protected $fillable = [
        'jam','created_at','updated_at'
    ];
}
