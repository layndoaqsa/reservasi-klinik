<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DokterSesi extends Model
{
  protected $table = 'dokter_sesi';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function sesi()
  {
      return $this->belongsTo('App\Sesi', 'sesi_id', 'id');
  }
  public function dokter()
  {
      return $this->belongsTo('App\User', 'dokter_id', 'id');
  }
}
