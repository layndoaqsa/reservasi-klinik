<?php

namespace App\Imports;

use App\CalonPasien;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\Importable;

class CalonPasienImport implements ToModel, WithValidation, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new CalonPasien([
            'no_telepon'  => $row['no_hp'],
            'nama' => $row['nama'],
            'nik' => $row['nik'],
            'no_bpjs' => $row['no_bpjs'],
            'created_by' => 0,
            'tipe' => 'child'
        ]);
    }

    public function rules(): array
    {
        return [
            'no_hp' => 'required',
            'nama' => 'required',
            'nik' => 'required|unique:calon_pasien,nik',
            'no_bpjs' => 'nullable|unique:calon_pasien,no_bpjs'
        ];
    }
}
