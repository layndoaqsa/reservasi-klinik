<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Reservasi;
use Carbon\Carbon;

class DisplayAntrianEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $message;

    public function __construct()
    {
        $display1 = Reservasi::with('poli','calonPasien','updatedBy')->whereDate('tanggal', Carbon::today())
                      ->whereNotNull('counter')
                      ->whereIn('status',['Sedang Pemeriksaan','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                      ->orderBy('updated_at','DESC')
                      ->first();
        $display2 = Reservasi::with('poli','calonPasien','updatedBy')->whereDate('tanggal', Carbon::today())
                      ->whereNotNull('counter')
                      ->whereIn('status',['Sedang Pemeriksaan','Menunggu Pemeriksaan','Terlewat Pemanggilan'])
                      ->orderBy('updated_at','DESC')
                      ->skip(1)->first();
        $this->message = response()->json(['first'=>$display1,'second'=>$display2]);
    }

    public function broadcastOn()
    {
        return ['display-antrian'];
    }

    public function broadcastAs()
    {
        return 'display-antrian-event';
    }
}
