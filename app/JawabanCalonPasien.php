<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JawabanCalonPasien extends Model
{
  protected $table = 'jawaban_calon_pasien';
  protected $guarded = ['created_at','updated_at'];
  public $timestamps = true;

  public function pertanyaan()
  {
    return $this->belongsTo('App\Pertanyaan', 'pertanyaan_id', 'id');
  }

  public function reservasi()
  {
      return $this->belongsTo('App\reservasi', 'reservasi_id', 'id');
  }
}
