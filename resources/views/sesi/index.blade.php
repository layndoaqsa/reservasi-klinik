@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Jadwal Poli {{$poliHari->poli['poli']}}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{route('poli.index')}}">Poli</a></li>
            <li class="active">Jadwal Poli {{$poliHari->poli['poli']}} </li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="btn-group col-md-12" data-toggle="buttons" id="poli_hari_id" style="margin-bottom:5px;padding:0px">
        @foreach ($poliHariAll as $key => $value)
          <label class="btn btn-default" style="width:14.3%">
            <input id="{{$value->id}}" type="radio" name="poli_hari_id" value="{{$value->id}}"> {{$value->hari['hari']}}
          </label>
        @endforeach
      </div>
      <div>
        <form action="{{route('sesi.create')}}" method="get" enctype="multipart/form-data" files=true>
          <input type="hidden" name="sesi_id" value="" id="sesi_id">
          <button type="submit" class="btn btn-primary btn-sm bg-primary-800"><i class="icon-add position-left"></i>Tambah Sesi</button>
        </form>
      </div>
      <table id="table-sesi" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th width="10px">No</th>
            <th>Sesi</th>
            <th>Waktu Mulai</th>
            <th>Waktu Selesai</th>
            <th>Jumlah Dokter</th>
  					<th class="col-md-2">Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
      <div>
          <a href="{{route('poli.index')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
      </div>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tableSesi;
  $(document).ready(function(){
    $('#{{$poliHari->id}}').trigger('click').parent().removeClass('btn-default').addClass('bg-pink-300');
    $('#sesi_id').val({{$poliHari->id}});

    $("input[name='poli_hari_id']").change(function(){
      checked = $("input[name='poli_hari_id']:checked").val();
      $('#sesi_id').val(checked);
      $("input[name='poli_hari_id']:checked").parent().removeClass('btn-default').addClass('bg-pink-300');
      $("input[name='poli_hari_id']:unchecked").parent().removeClass('bg-pink-300').addClass('btn-default');
      tableSesi.draw(true);
    });
		/* tabel user */
    tableSesi = $('#table-sesi').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-sesi') }}",
          type: "GET",
          data: function (d) {
                // d.poli_hari_id = {{$poliHari->id}}
                d.poli_hari_id = $("input[name='poli_hari_id']:checked").val()
              }
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'DT_RowIndex', name:'DT_RowIndex', visible:true},
          { data: 'sesi', name:'sesi', visible:true},
          { data: 'mulai', name:'mulai', visible:true},
          { data: 'selesai', name:'selesai', visible:true},
          { data: 'jumlah_dokter', name:'jumlah_dokter', visible:true},
          { data: 'action', name:'action', visible:true},
      ],
    });

    $('#table-sesi tbody').on( 'click', 'button', function () {
        var data = tableSesi.row( $(this).parents('tr') ).data();
          swal({
          text: "Apakah Anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('delete/data-sesi') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                tableSesi.ajax.reload();
                swal("Data yang dipilih berhasil dihapus!", {
                  icon: "success",
                });
              }
            });
          } else {
            swal("Data Anda aman!");
          }
        });
      });
  });

</script>
@endpush
