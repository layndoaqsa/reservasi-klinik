@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Jadwal Poli {{$poliHari->poli['poli']}}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{route('poli.index')}}">Poli</a></li>
            <li><a href="{{route('sesi.index',$poliHari->id)}}">Jadwal Poli {{$poliHari->poli['poli']}} </a></li>
            <li class="active">Ubah Sesi</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('sesi.update',$data->id)}}" method="post" enctype="multipart/form-data" files=true>
            @method('PUT')
            @csrf
                <fieldset class="content-group">
                <legend class="text-bold">Ubah Sesi</legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Sesi <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="sesi" class="form-control" value="{{old('sesi') ? old('sesi') : $data->sesi}}">
                        @if ($errors->has('sesi'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('sesi') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Waktu Mulai <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="time" name="waktu_mulai" class="form-control" value="{{old('waktu_mulai') ? old('waktu_mulai') : $data->mulai}}">
                        @if ($errors->has('waktu_mulai'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('waktu_mulai') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Waktu Selesai <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="time" name="waktu_selesai" class="form-control" value="{{old('waktu_selesai') ? old('waktu_selesai') : $data->selesai}}">
                        @if ($errors->has('waktu_selesai'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('waktu_selesai') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Jumlah Dokter <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" name="jumlah_dokter" class="form-control" value="{{old('jumlah_dokter') ? old('jumlah_dokter') : $data->jumlah_dokter}}">
                        @if ($errors->has('jumlah_dokter'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('jumlah_dokter') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Dokter <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <div class="multi-select-full">
                            <select id="dokter" name="dokter[]" class="form-control select-search" multiple="multiple">
                            @foreach(\App\User::where('poli_id',$poliHari->poli_id)->get() as $value)
                            <option value="{{$value->id}}" {{ in_array($value->id, \App\DokterSesi::where('sesi_id',$data->id)->get()->pluck('dokter_id')->toArray()) ? 'selected':'' }}>{{$value->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        @if ($errors->has('role'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('role') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                </fieldset>
            <div>

            <div class="col-md-4">
                <a href="{{route('sesi.index',$poliHari->id)}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-md-8 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
