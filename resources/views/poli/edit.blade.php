@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Poli</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{route('poli.index')}}">Poli</a></li>
            <li class="active">Ubah</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('poli.update',$data->id)}}" method="post" enctype="multipart/form-data" files=true>
            @method('PUT')
            @csrf
                <fieldset class="content-group">
                <legend class="text-bold">Ubah Poli</legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Poli <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="poli" class="form-control" value="{{$data->poli}}">
                        @if ($errors->has('poli'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('poli') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Kode Poli <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="kode_poli" class="form-control" value="{{$data->kode}}">
                        @if ($errors->has('kode_poli'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('kode_poli') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Waktu Pemeriksaan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" name="waktu" id="waktu" min="1" class="form-control" value="{{$data->waktu}}">
                        @if ($errors->has('waktu'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('waktu') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Icon <span class="text-danger">*</span>(Rasio 1:1)</label>
                    <div class="col-lg-9">
                        @if(!empty($data->icon))
                        <img style="padding:10px" class="img-responsive" src="{{asset('storage/images/icon-poli/'.$data->icon.'')}}" alt="icon" title="Ubah icon poli" width="100" height="50">
                        @endif
                        <input type="file" name="icon" class="file-input-custom" data-show-caption="true" data-show-upload="false" accept="image/*">
                        @if ($errors->has('icon'))
                        <label style="padding-top:7px;color:#F44336;">
                          <strong><i class="fa fa-times-circle"></i> {{$errors->first('icon')}}</strong>
                        </label>
                        @endif
                    </div>
                </div>


                </fieldset>
            <div>

            <div class="col-md-4">
                <a href="{{route('poli.index')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-md-8 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
