@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Poli</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Poli</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="col-md-1">
        <a href="{{route('poli.create')}}" class="btn btn-primary btn-sm bg-primary-800"><i class="icon-add position-left"></i>Tambah Data</a>
      </div>

      <table id="table-poli" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th width="10px">No</th>
            <th>Poli</th>
            <th>Kode</th>
            <th>Waktu (Menit)</th>
            <th class="col-md-2">Jadwal</th>
  					<th class="col-md-2">Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tablePoli;
  $(document).ready(function(){
		/* tabel user */
    tablePoli = $('#table-poli').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-poli') }}",
          type: "GET",
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'DT_RowIndex', name:'DT_RowIndex', visible:true},
          { data: 'poli', name:'poli', visible:true},
          { data: 'kode', name:'kode', visible:true},
          { data: 'waktu', name:'waktu', visible:true},
          { data: 'jadwal', name:'jadwal', visible:true},
          { data: 'action', name:'action', visible:true},
      ],
    });

    $('#table-poli tbody').on( 'click', 'button', function () {
        var data = tablePoli.row( $(this).parents('tr') ).data();
          swal({
          text: "Apakah Anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('delete/data-poli') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                if (result.status == 'success') {
                  tablePoli.ajax.reload();
                  swal("Data yang dipilih berhasil dihapus!", {
                    icon: "success",
                  });
                } else {
                  swal(result.message, {
                    icon: "error",
                  });
                }
              }
            });
          } else {
            swal("Data Anda aman!");
          }
        });
      });
  });

</script>
@endpush
