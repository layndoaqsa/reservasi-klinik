@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Reservasi</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="">Reservasi</a></li>
            <li class="active">Tindakan</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('reservasiadmin.update',$data->id)}}" method="post" enctype="multipart/form-data" files=true>
            @method('PUT')
            @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Tindakan</legend>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Nama Pasien <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input disabled type="" name="" class="form-control" value="{{$data->calonPasien['nama']}}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Nomor Telepon <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input disabled type="" name="" class="form-control" value="{{$data->calonPasien['no_telepon']}}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Nomor BPJS / Kartu Sehat <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input disabled type="" name="" class="form-control" value="{{$data->calonPasien['no_bpjs']}}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Keluhan Fisik<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea type="text" name="" rows="3" class="form-control"  placeholder="" disabled>{{$data->keluhan}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kekhawatiran<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea type="text" name="" rows="3" class="form-control"  placeholder="" disabled>{{$data->kekhawatiran}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Riwayat Penyakit Menurun<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea type="text" name="" rows="3" class="form-control"  placeholder="" disabled>{{$data->riwayat_penyakit_menahun}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Upaya Pengobatan<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea type="text" name="" rows="3" class="form-control"  placeholder="" disabled>{{$data->upaya_pengobatan}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Ubah Status<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                        @php
                          $status = ['Reservasi','Menunggu Pemeriksaan','Sedang Pemeriksaan','Selesai Pemeriksaan','Batal'];
                        @endphp
                        <select name="perubahan_status" class="form-control select-search" id="">
                            @foreach ($status as $value)
                                <option value="{{$value}}" {{$data->status == $value ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('perubahan_status'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('perubahan_status') }}</strong>
                        </label>
                        @endif
                    </div>
                    </div>

                </fieldset>
            <div>

            <div class="col-xs-6">
                <a href="{{route('reservasiadmin.index')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-xs-6 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
