@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Reservasi</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Reservasi</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="form-group">
          <div class="col-md-6" style="padding:0px">
            <div class="text-center input-group form-group">
              <span class="input-group-addon"><i class="icon-calendar"></i></span>
              <input type="text" class="form-control" placeholder="Pilih tanggal" name="tanggal" id="tanggal" style="width:50%">
            </div>
          </div>
          <div class="col-md-6" style="padding:0px">
              <div class="col-md-5 pull-right" style="padding:0px">
                  <select name="poli" class="form-control select-search" id="status">
                      @php
                      $status = ['Reservasi','Menunggu Pemeriksaan','Sedang Pemeriksaan','Selesai Pemeriksaan','Terlewat Pemanggilan','Batal'];
                      @endphp
                      <option value="">Semua Status</option>
                      @foreach ($status as $value)
                      <option value="{{$value}}">{{$value}}</option>
                      @endforeach
                  </select>
                </div>
                <div class="col-md-5 pull-right" style="padding-right:15px" id="div-poli">
                  <select name="poli" class="form-control select-search" id="poli">
                      <option value="">Semua Poli</option>
                      @foreach ($poli as $value)
                      <option value="{{$value->id}}">{{$value->poli}}</option>
                      @endforeach
                  </select>
                </div>
          </div>
      </div>

      <table id="table-reservasiadmin" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th class="col-md-1">Kode</th>
            <th class="col-md-1">Poli</th>
            <th class="col-md-2">Antrian</th>
            <th class="col-md-2">Peserta</th>
            <th class="col-md-2">Nama</th>
            <th class="col-md-2">No Telepon</th>
  					<th class="col-md-2">Status</th>
  					<th>Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tableReservasiadmin;
  $(document).ready(function(){
    $('#tanggal').datepicker({
      format: "dd-mm-yyyy",
      todayHighlight: true,
      autoclose: true
    }).datepicker("setDate", new Date());
    // $('#status').val('Reservasi').trigger('change');
    @if (Auth::user()->hasRole('Super Admin'))
      // $('#poli_0').trigger('click').parent().removeClass('btn-default').addClass('bg-pink-300');
      // $("input[name='poli']").change(function(){
      //   $("input[name='poli']:checked").parent().removeClass('btn-default').addClass('bg-pink-300');
      //   $("input[name='poli']:unchecked").parent().removeClass('bg-pink-300').addClass('btn-default');
      //   tableReservasiadmin.draw(true);
      // });
    @else
    $('#poli').hide();
    $('#div-poli').hide();
    @endif
    $("input[name='tanggal']").change(function(){
      tableReservasiadmin.draw(true);
    });
    $('#poli').change(function() {
      tableReservasiadmin.draw(true);
    });
    $('#status').change(function() {
      tableReservasiadmin.draw(true);
    });
    // $('#status_0').trigger('click').parent().removeClass('btn-default').addClass('bg-pink-300');
    // $("input[name='status']").change(function(){
    //   $("input[name='status']:checked").parent().removeClass('btn-default').addClass('bg-pink-300');
    //   $("input[name='status']:unchecked").parent().removeClass('bg-pink-300').addClass('btn-default');
    //   tableReservasiadmin.draw(true);
    // });
    /* tabel user */
    tableReservasiadmin = $('#table-reservasiadmin').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-reservasiadmin') }}",
          type: "GET",
          data: function (d) {
                d.tanggal = $("input[name='tanggal']").val();
                d.status = $('#status').find(":selected").val();
                d.poli = $('#poli').find(":selected").val();
              }
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'kode', name:'kode', visible:true},
          { data: 'poli.poli', name:'poli.poli', visible:true},
          { data: 'antrian', name:'antrian', visible:true},
          { data: 'peserta', name:'peserta', visible:true},
          { data: 'calon_pasien.nama', name:'calon_pasien.nama', visible:true},
          { data: 'calon_pasien.no_telepon', name:'calon_pasien.no_telepon', visible:true},
          { data: 'status', name:'status', visible:true},
          { data: 'action', name:'action', visible:true},
      ],
    });

    $('#table-reservasiadmin tbody').on( 'click', 'button', function () {
        var data = tableReservasiadmin.row( $(this).parents('tr') ).data();
          swal({
          text: "Apakah Anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('delete/data-reservasiadmin') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                tableReservasiadmin.ajax.reload();
                swal("Data yang dipilih berhasil dihapus!", {
                  icon: "success",
                });
              }
            });
          } else {
            swal("Data Anda aman!");
          }
        });
      });
  });


</script>
@endpush
