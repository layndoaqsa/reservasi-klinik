@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Reservasi</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Reservasi</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-flat">
				<div class="panel-body">
					<div class="tabbable">
						<ul class="nav nav-tabs nav-tabs-highlight">
							<li class="active"><a href="#pemanggilan" data-toggle="tab"><i class="icon-menu7 position-left"></i> Pemanggilan</a></li>
							<li><a href="#antrian" data-toggle="tab"><i class="icon-mention position-left"></i> Daftar Pasien</a></li>
						</ul>
						<div class="tab-content">
              <div class="tab-pane active" id="pemanggilan">
      					<div class="row">
      						<div class="col-lg-7">
                      <!-- Antrian -->
                        <div class="row">
              						<div class="col-lg-12">
              							<div class="thumbnail">
                                <div class="panel-heading" style="padding-bottom:0px">
                									<h6 class="panel-title">Pasien Sedang / Akan Diperiksa</h6> <!-- Kalo Panggilan berarti Akan, Kalau lagi berari Sedang -->
                								</div>
              						    	<div class="caption text-center row">
                                  <div class="col-md-12" id="box-tombol">
                                    @if ($antrian_selanjutnya)
                                    <button type="button" class="btn btn-primary" name="button" id="panggilan1">Panggilan 1</button>
                                    <button type="button" class="btn btn-primary" name="button" id="panggilan2" style="display:none">Panggilan 2</button>
                                    <button type="button" class="btn btn-primary" name="button" id="panggilan3" style="display:none">Panggilan 3</button>
                                    @endif
                                    <button type="button" class="btn btn-primary" name="button" id="panggilan-datang" style="display:none">Datang</button>
                                    <button type="button" class="btn btn-primary" name="button" id="panggilan-lewati" style="display:none">Lewati</button>
                                    <button type="button" class="btn btn-primary" name="button" id="panggilan-selesai" style="display:none">Selesai</button>
                                  </div>
                                  <div id="box-pasien-dipanggil" style="display:none">
                                    <input type="hidden" name="antrian_pasien_dipanggil" value="">
                                    <input type="hidden" name="kode_pasien_dipanggil" value="">
                                    <h1 style="font-size:48px;margin-top:0px"><b id="antrian-pasien-dipanggil"></b></h1>
                                    <h6 class="text-semibold no-margin" id="nama-pasien-dipanggil"></h6><br>
                                    <div class="col-md-12" style="margin-top:-10px">
                                      <h6 class="text-semibold no-margin">Hasil Sreening : <large class="" id="status-pasien-dipanggil"></small></h6>
                                    </div>
                                    <div class="col-md-6">
                                      <h6 style="padding-top:10px" class="text-semibold no-margin">BPJS <small class="display-block" id="bpjs-pasien-dipanggil"></small></h6>
                                    </div>
                                    <div class="col-md-6">
                                      <h6 style="padding-top:10px" class="text-semibold no-margin">NIK <small class="display-block" id="nik-pasien-dipanggil"></small></h6>
                                    </div>
                                    <div class="col-md-6">
                                      <h6 style="padding-top:10px" class="text-semibold no-margin">Keluhan <small class="display-block" id="keluhan-pasien-dipanggil"></small></h6>
                                    </div>
                                    <div class="col-md-6">
                                      <h6 style="padding-top:10px" class="text-semibold no-margin">Riwayat Penyakit <small class="display-block" id="riwayat-penyakit-pasien-dipanggil"></small></h6>
                                    </div>
                                    <div class="col-md-6">
                                      <h6 style="padding-top:10px" class="text-semibold no-margin">Kekhawatiran <small class="display-block" id="kekhawatiran-pasien-dipanggil"></small></h6>
                                    </div>
                                    <div class="col-md-6">
                                      <h6 style="padding-top:10px" class="text-semibold no-margin">Upaya Pengobatan <small class="display-block" id="upaya-pengobatan-pasien-dipanggil"></small></h6>
                                    </div>
                                  </div>

              						    	</div>
              					    	</div>
                            </div>
              						</div>
        							<!-- /antrian -->
                  </div>
                  <div class="col-lg-5">

                      <!-- Info Antrian -->
        							<div class="panel panel-flat">
        								<div class="panel-heading">
        									<h6 class="panel-title">Info Antrian</h6>
        									<div class="heading-elements">
        										<span class="heading-text"><i class="icon-users4 text-warning position-left"></i></span>
        									</div>
        								</div>
        								<div class="container-fluid">
        									<div class="row text-center">
        										<div class="col-md-4">
        											<div class="content-group">
        												<h6 class="text-semibold no-margin" id="jml-pasien">{{$reservasi->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count()}}</h6>
        												<span class="text-muted text-size-small">Total Pasien</span>
        											</div>
        										</div>
        										<div class="col-md-4">
        											<div class="content-group">
        												<h6 class="text-semibold no-margin" id="jml-pasien-sdh-diperiksa">{{$reservasi->where('status','Selesai Pemeriksaan')->count()}}</h6>
                                <span class="text-muted text-size-small">Sudah Diperiksa</span>
        											</div>
        										</div>
        										<div class="col-md-4">
        											<div class="content-group">
        												<h6 class="text-semibold no-margin" id="jml-pasien-blm-diperiksa">{{$reservasi->whereIn('status',['Menunggu Pemeriksaan','Terlewat Pemanggilan'])->count()}}</h6>
                                <span class="text-muted text-size-small">Belum Diperiksa</span>
        											</div>
        										</div>
        									</div>
        								</div>
        							</div>
        							<!-- /Info Antrian -->

                      <!-- Antrian Selanjutnya -->
                      <!-- Daily sales -->
        							<div class="panel panel-flat">
        								<div class="panel-heading">
                            <h6 class="panel-title">Antrian Selanjutnya</h6>
        								</div>
        								<div class="table-responsive">

        									<table class="table text-nowrap">
        										<tbody>
        											<tr>
        												<td>
        													<div class="media-left media-middle" id="media-left-antrian-selanjutnya">
                                    <input type="hidden" name="antrian_pasien_selanjutnya" value="{{@$antrian_selanjutnya->antrian}}">
                                    <input type="hidden" name="kode_pasien_selanjutnya" value="{{@$antrian_selanjutnya->kode}}">
                                    <h1 style="margin:0px" id="antrian-pasien-selanjutnya">{{@$antrian_selanjutnya->antrian}}</h1>
        													</div>

        													<div class="media-body" id="media-body-antrian-selanjutnya">
                                    @if ($antrian_selanjutnya)
        														<div class="media-heading">
                                      <h5 id="nama-pasien-selanjutnya">{{@$antrian_selanjutnya->calonPasien->nama}}</h5>
        														</div>

        														<div class="text-muted text-size-small" id="nik-pasien-selanjutnya"> NIK : {{@$antrian_selanjutnya->calonPasien->nik}}</div>
        													  @else
                                      <h5 id="antrian-selanjutnya-tidak-ada">Tidak ada pasien</h5>
                                    @endif
                                  </div>
        												</td>
        											</tr>
        										</tbody>
        									</table>
        								</div>
        							</div>
        							<!-- /daily sales -->
        							<!-- /Antrian Selanjutnya -->
                  </div>
                </div>
							</div>

							<div class="tab-pane" id="antrian">

                      <div class="col-md-6" style="padding:0px">
                        <div class="text-center input-group form-group">
                          <span class="input-group-addon"><i class="icon-calendar"></i></span>
                          <input type="text" class="form-control" placeholder="Pilih tanggal" name="tanggal" id="tanggal" style="width:50%">
                        </div>
                      </div>
                      <div class="col-md-6" style="padding:0px">
                          <div class="col-md-5 pull-right" style="padding:0px">
                              <select name="poli" class="form-control select-search" id="status">
                                @php
                                $status = ['Reservasi','Menunggu Pemeriksaan','Sedang Pemeriksaan','Selesai Pemeriksaan','Terlewat Pemanggilan','Batal'];
                                @endphp
                                  <option value="">Semua Status</option>
                                  @foreach ($status as $value)
                                  <option value="{{$value}}">{{$value}}</option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="col-md-5 pull-right" style="padding-right:15px" id="div-poli">
                              <select name="poli" class="form-control select-search" id="poli">
                                  <option value="">Semua Poli</option>
                                  @foreach ($poli as $value)
                                  <option value="{{$value->id}}">{{$value->poli}}</option>
                                  @endforeach
                              </select>
                            </div>
                      </div>

                  <table id="table-reservasiadmin" class="table" style="width:100%">
              			<thead>
              				<tr>
                        <th>Id</th>
                        <th class="col-md-1">Kode</th>
                        <th class="col-md-1">Poli</th>
                        <th class="col-md-2">Antrian</th>
                        <th class="col-md-2">Peserta</th>
                        <th class="col-md-2">Nama</th>
                        <th class="col-md-2">No Telepon</th>
              					<th class="col-md-2">Status</th>
              					<th>Aksi</th>
              				</tr>
              			</thead>
              			<tbody>
              			</tbody>
              		</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tableReservasiadmin;
  $(document).ready(function(){
    $('#tanggal').datepicker({
      format: "dd-mm-yyyy",
      todayHighlight: true,
      autoclose: true
    }).datepicker("setDate", new Date());

    $('#status').val('Menunggu Pemeriksaan').trigger('change');
    @if (Auth::user()->hasRole('Super Admin'))
      // $('#poli_0').trigger('click').parent().removeClass('btn-default').addClass('bg-pink-300');
      // $("input[name='poli']").change(function(){
      //   $("input[name='poli']:checked").parent().removeClass('btn-default').addClass('bg-pink-300');
      //   $("input[name='poli']:unchecked").parent().removeClass('bg-pink-300').addClass('btn-default');
      //   tableReservasiadmin.draw(true);
      // });
    @else
    $('#poli').hide();
    $('#div-poli').hide();
    @endif
    $("input[name='tanggal']").change(function(){
      tableReservasiadmin.draw(true);
    });
    $('#poli').change(function() {
      tableReservasiadmin.draw(true);
    });
    $('#status').change(function() {
      tableReservasiadmin.draw(true);
    });
    // $('#status_0').trigger('click').parent().removeClass('btn-default').addClass('bg-pink-300');
    // $("input[name='status']").change(function(){
    //   $("input[name='status']:checked").parent().removeClass('btn-default').addClass('bg-pink-300');
    //   $("input[name='status']:unchecked").parent().removeClass('bg-pink-300').addClass('btn-default');
    //   tableReservasiadmin.draw(true);
    // });
    /* tabel user */
    tableReservasiadmin = $('#table-reservasiadmin').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-reservasiadmin') }}",
          type: "GET",
          data: function (d) {
                d.tanggal = $("input[name='tanggal']").val();
                d.status = $('#status').find(":selected").val();
                d.poli = $('#poli').find(":selected").val();
              }
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'kode', name:'kode', visible:true},
          { data: 'poli.poli', name:'poli.poli', visible:true},
          { data: 'antrian', name:'antrian', visible:true},
          { data: 'peserta', name:'peserta', visible:true},
          { data: 'calon_pasien.nama', name:'calon_pasien.nama', visible:true},
          { data: 'calon_pasien.no_telepon', name:'calon_pasien.no_telepon', visible:true},
          { data: 'status', name:'status', visible:true},
          { data: 'action', name:'action', visible:true},
      ],
    });

    $('#table-reservasiadmin tbody').on( 'click', 'button', function () {
        var data = tableReservasiadmin.row( $(this).parents('tr') ).data();
          swal({
          text: "Apakah Anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('delete/data-reservasiadmin') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                tableReservasiadmin.ajax.reload();
                swal("Data yang dipilih berhasil dihapus!", {
                  icon: "success",
                });
              }
            });
          } else {
            swal("Data Anda aman!");
          }
        });
      });

      $(document).on( "click", "#panggilan1,#panggilan2,#panggilan3", function() {
        console.log($(this).text())
        if ($(this).text() == 'Panggilan 1') {
          $('#panggilan1').hide();
          $('#panggilan2').show();
        } else if ($(this).text() == 'Panggilan 2') {
          $('#panggilan2').hide();
          $('#panggilan3').show();
        } else {
          $('#panggilan3').hide();
          $('#panggilan-lewati').show();
        }
        $('#panggilan-datang').show();
        $('#box-pasien-dipanggil').show();

        if ($('input[name="antrian_pasien_dipanggil"]').val() && $('input[name="kode_pasien_dipanggil"]').val()) {
          antrian = $('input[name="antrian_pasien_dipanggil"]').val();
          kode = $('input[name="kode_pasien_dipanggil"]').val();
        } else {
          antrian = $('input[name="antrian_pasien_selanjutnya"]').val();
          kode = $('input[name="kode_pasien_selanjutnya"]').val();
        }

        $.ajax({
          type: 'GET',
          url: "{{ url('admin/panggil-antrian') }}" + "/" + antrian + "/" + kode,
          dataType: 'json',
          success: function(data) {
            $('input[name="antrian_pasien_dipanggil"]').val(data.antrian);
            $('input[name="kode_pasien_dipanggil"]').val(data.kode);
            $('#antrian-pasien-dipanggil').text(data.antrian);
            $('#status-pasien-dipanggil').text(data.screening.hasil_skor.hasil);
            $('#nama-pasien-dipanggil').text(data.calon_pasien.nama);
            $('#kode-pasien-dipanggil').text(data.kode);
            $('#bpjs-pasien-dipanggil').text(data.bpjs);
            $('#nik-pasien-dipanggil').text(data.calon_pasien.nik);
            $('#keluhan-pasien-dipanggil').text(data.keluhan);
            $('#riwayat-penyakit-pasien-dipanggil').text(data.riwayat_penyakit_menahun);
            $('#kekhawatiran-pasien-dipanggil').text(data.kekhawatiran);
            $('#upaya-pengobatan-pasien-dipanggil').text(data.upaya_pengobatan);

          },
          error: function(data) {
          }
        });

      });

      $('#panggilan-datang').click(function(){
        $.ajax({
          type: 'GET',
          url: "{{ url('admin/antrian/datang') }}" + "/" + antrian + "/" + kode,
          dataType: 'json'
        });
        $('#panggilan-lewati').hide();
        $('#panggilan1').hide();
        $('#panggilan2').hide();
        $('#panggilan3').hide();
        $(this).hide();
        $('#panggilan-selesai').show();
      });

      $('#panggilan-selesai').click(function(){
        $.ajax({
          type: 'GET',
          url: "{{ url('admin/antrian/selesai') }}" + "/" + antrian + "/" + kode,
          dataType: 'json'
        });
        $('#panggilan1').show();
        $('#panggilan-datang').hide();
        $('#box-pasien-dipanggil').hide();
        $(this).hide();
        $('input[name="antrian_pasien_dipanggil"]').val('');
        $('input[name="kode_pasien_dipanggil"]').val('');
        tableReservasiadmin.draw(true);
      });

      $('#panggilan-lewati').click(function(){
        $.ajax({
          type: 'GET',
          url: "{{ url('admin/panggil-antrian') }}" + "/" + antrian + "/" + kode,
          dataType: 'json'
        });
        $('#panggilan1').show();
        $('#panggilan-datang').hide();
        $('#box-pasien-dipanggil').hide();
        $(this).hide();
        $('input[name="antrian_pasien_dipanggil"]').val('');
        $('input[name="kode_pasien_dipanggil"]').val('');
      });

  });


</script>
<script src="https://js.pusher.com/5.1/pusher.min.js"></script>
<script>

  // Enable pusher logging - don't include this in production

  var pusher = new Pusher('604e964eea1b99d82ad2', {
    cluster: 'ap1',
    forceTLS: true
  });

  var channel = pusher.subscribe('panggil-antrian');
  channel.bind('panggil-antrian-event', function(data) {
    console.log(data)
    if (data.message != null) {
      
      $('#media-left-antrian-selanjutnya').show();
      $('#antrian-pasien-selanjutnya').text(data.message.antrian);
      $('input[name="antrian_pasien_selanjutnya"]').val(data.message.antrian);
      $('input[name="kode_pasien_selanjutnya"]').val(data.message.kode);
      $('#media-body-antrian-selanjutnya').html('<div class="media-heading">'+
                                      '<h5 id="nama-pasien-selanjutnya">'+data.message.calon_pasien.nama+'</h5>'+
        														  '</div>'+
        														  '<div class="text-muted text-size-small" id="nik-pasien-selanjutnya"> NIK : '+data.message.calon_pasien.nik+'</div>')
    } else {
      $('#panggilan1').hide();
      $('#media-left-antrian-selanjutnya').hide();
      $('#media-body-antrian-selanjutnya').html('<h5 id="antrian-selanjutnya-tidak-ada">Tidak ada pasien</h5>');
    }

  });

  var infoAntrian = pusher.subscribe('info-antrian');
  infoAntrian.bind('info-antrian-event', function(data) {
    $('#jml-pasien').text(data.message.total_pasien);
    $('#jml-pasien-blm-diperiksa').text(data.message.pasien_blm_diperiksa);
    $('#jml-pasien-sdh-diperiksa').text(data.message.pasien_sdh_diperiksa);
    if (data.message.pasien_blm_diperiksa == 0) {
        $('#panggilan1').hide();
    } else {
      $('#panggilan1').show();
    }
  });
</script>
@endpush
