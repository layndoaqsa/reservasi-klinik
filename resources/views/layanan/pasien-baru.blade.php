@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Layanan</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('layanan.index')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Pasien Baru</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <form class="form-horizontal form-validate-jquery" action="{{route('layanan.pasienBaruStore')}}" method="post" enctype="multipart/form-data" files=true>

            <div class="panel-body">
                {{ csrf_field() }}
                <fieldset class="content-group">
                <legend class="text-bold">Buat Reservasi</legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') ? old('nama') : '' }}" placeholder="">
                        @if ($errors->has('nama'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nama') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Nomor Telepon <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" name="nomor_telepon" class="form-control" value="{{ old('nomor_telepon') ? old('nomor_telepon') : '' }}" placeholder="">
                        @if ($errors->has('nomor_telepon'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_telepon') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">NIK <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" name="nik" class="form-control" value="{{ old('nik') ? old('nik') : '' }}" placeholder="">
                        @if ($errors->has('nik'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nik') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Nomor BPJS / Kartu Sehat (bila ada)</label>
                    <div class="col-lg-9">
                        <input type="number" name="nomor_bpjs_atau_kartu_sehat" class="form-control" value="{{ old('nomor_bpjs_atau_kartu_sehat') ? old('nomor_bpjs_atau_kartu_sehat') : '' }}" placeholder="">
                        @if ($errors->has('nomor_bpjs_atau_kartu_sehat'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_bpjs_atau_kartu_sehat') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                @if($errors->any())
                <div class="alert alert-danger alert-styled-left alert-bordered">
                  <span class="text-semibold">Oops! </span> {{$errors->first()}}
                </div>
                @endif
                <div class="form-group">
                    <label class="control-label col-lg-3">Poli <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <div class="btn-group col-md-12" data-toggle="buttons" style="margin:0px;padding:0px">
                          @foreach ($poli as $value)
                            <label class="btn btn-default btn-ligh text-center col-md-2 col-xs-6" style="margin:5px 0px 5px 0px">
                                <!-- <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i> -->
                                <img src="{{asset('storage/images/icon-poli/'.$value->icon.'')}}" alt="error" style="width:60px">
                                <br>
                                <input type="radio" name="poli" id="poli" value="{{$value->id}}">{{$value->poli}}
                            </label>
                          @endforeach
                        </div>
                        <div style="padding-top:7px" class="btn-group" data-toggle="buttons" id="tanggal">

                        </div>
                        @if ($errors->has('poli'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('poli') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="control-label col-lg-3">Tanggal <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <div class="btn-group" data-toggle="buttons" id="tanggal">

                        </div>
                        @if ($errors->has('tanggal'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('tanggal') }}</strong>
                        </label>
                        @endif
                    </div>
                </div> -->

                <div class="form-group">
                    <label class="control-label col-lg-3">Keluhan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea type="text" name="keluhan" rows="3" class="form-control"  placeholder="">{{ old('keluhan') }}</textarea>
                        @if ($errors->has('keluhan'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('keluhan') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-3">Kekhawatiran</label>
                    <div class="col-lg-9">
                        <textarea type="text" name="kekhawatiran" rows="3" class="form-control"  placeholder="">{{ old('kekhawatiran') }}</textarea>
                        @if ($errors->has('kekhawatiran'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('kekhawatiran') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-3">Riwayat Penyakit Menahun</label>
                    <div class="col-lg-9">
                        <textarea type="text" name="riwayat_penyakit_menahun" rows="3" class="form-control"  placeholder="">{{ old('riwayat_penyakit_menahun') }}</textarea>
                        @if ($errors->has('riwayat_penyakit_menahun'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('riwayat_penyakit_menahun') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-3">Upaya Pengobatan Yang Dilakukan</label>
                    <div class="col-lg-9">
                        <textarea type="text" name="upaya_pengobatan" rows="3" class="form-control"  placeholder="">{{ old('upaya_pengobatan') }}</textarea>
                        @if ($errors->has('upaya_pengobatan'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('upaya_pengobatan') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                </fieldset>
            <div>

                <div class="col-xs-12 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>

            </div>

        </form>
    </div>
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
    $(document).ready(function(){
      $("input[name='poli']").change(function(){
          poli = $(this).val();
          $('#jam').html('');
          $('#sesi').html('');
          $.ajax({
          url: "{{url('admin/layanan/cek') }}"+"/"+poli
          }).done(function(data) {
              var tanggal = '';
              if (Array.isArray(data)) {
                  $.each(data, function(i, item) {
                  tanggal += '<span class="label bg-success label-rounded">Berhasil memilih poli. Total antrian '+data[i].total_antrian+', slot '+data[i].slot_antrian+'</span>';
                  $('#tanggal').html(tanggal);
                  });
              } else {
                  $('#tanggal').html('<span class="label bg-danger label-rounded">'+data+'</span>');
              }
          });

      })
      $(document).on("change", "input[name='poli']", function () {
          poli = $("input[name='poli']:checked").val()
          $("input[name='poli']:checked").parent().removeClass('btn-default').addClass('bg-teal-300');
          $("input[name='poli']:unchecked").parent().removeClass('bg-teal-300').addClass('btn-default');
      })
    });
</script>
@endpush
