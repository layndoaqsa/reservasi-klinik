@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Layanan</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('layanan.index')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Pasien Lama</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <table id="table-pasien-lama" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th width="25%">Nama</th>
            <th width="20%">NIK</th>
  					<th width="20%">No Telepon</th>
            <th width="20%">No BPJS/Kartu Sehat</th>
  					<th width="20%">Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_style')
  <style>


  </style>
@endpush

@push('after_script')
@push('after_script')
<script>
var tablePasienLama;
  $(document).ready(function(){
		/* tabel calon-pasien */
    tablePasienLama = $('#table-pasien-lama').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-reservasi/pasien-lama') }}",
          type: "GET",
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'nama', name:'nama', visible:true},
          { data: 'nik', name:'nik', visible:true},
          { data: 'no_telepon', name:'no_telepon', visible:true},
          { data: 'no_bpjs', name:'no_bpjs', visible:true},
          { data: 'antrian', name:'antrian', visible:true},
          // { data: 'action', name:'action', visible:true},
      ],
    });
  });

</script>
@endpush
@endpush
