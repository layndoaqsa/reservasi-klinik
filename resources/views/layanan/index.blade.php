@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Layanan</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i> Dashboard</li>
            <li class="active">Layanan</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="form-group">
          <div class="col-md-6" style="padding:0px">
            <div class="text-center input-group form-group">
              <span class="input-group-addon"><i class="icon-calendar"></i></span>
              <input type="text" class="form-control" placeholder="Pilih tanggal" name="tanggal" id="tanggal" style="width:50%">
            </div>
          </div>
          <div class="col-md-6" style="padding:0px">
              <div class="col-md-5 pull-right" style="padding:0px">
                  <select name="status" class="form-control select-search" id="status">
                      @php
                      $status = ['Reservasi','Menunggu Pemeriksaan','Sedang Pemeriksaan','Selesai Pemeriksaan','Terlewat Pemanggilan','Batal'];
                      @endphp
                      <option value="">Semua Status</option>
                      @foreach ($status as $value)
                      <option value="{{$value}}">{{$value}}</option>
                      @endforeach
                  </select>
                </div>
          </div>
      </div>
      <table id="table-reservasi" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th class="col-md-1">Kode</th>
            <th class="col-md-1">Antrian</th>
            <th class="col-md-1">Poli</th>
            <th class="col-md-2">Nama</th>
            <th class="col-md-2">NIK</th>
            <th class="col-md-2">Peserta</th>
            <th class="col-md-2">Status</th>
            <!-- <th>Aksi</th> -->
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tableReservasi;
  $(document).ready(function(){
    $('#tanggal').datepicker({
      format: "dd-mm-yyyy",
      todayHighlight: true,
      autoclose: true
    }).datepicker("setDate", new Date());

    $("input[name='tanggal']").change(function(){
      tableReservasi.draw(true);
    });

    $('#status').val('Reservasi').trigger('change');
    $('#status').change(function() {
      tableReservasi.draw(true);
    });
		/* tabel user */
    tableReservasi = $('#table-reservasi').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-reservasilayanan') }}",
          type: "GET",
          data: function (d) {
                d.tanggal = $("input[name='tanggal']").val()
                d.status = $('#status').find(":selected").val()
                d.poli = $('#poli').find(":selected").val()
              }
      },

      columns: [
        { data: 'id', name:'id', visible:false},
        { data: 'kode', name:'kode', visible:true},
        { data: 'antrian', name:'antrian', visible:true},
        { data: 'poli.poli', name:'poli.poli', visible:true},
        { data: 'calon_pasien.nama', name:'calon_pasien.nama', visible:true},
        { data: 'calon_pasien.nik', name:'calon_pasien.nik', visible:true},
        { data: 'peserta', name:'peserta', visible:true},
        { data: 'status', name:'status', visible:true},
        // { data: 'action', name:'action', visible:true},
      ],
    });

    $('#table-reservasi tbody').on( 'click', '#reservasi', function () {
        var data = tableReservasi.row( $(this).parents('tr') ).data();
          swal({
          text: 'Status akan diubah ke "Menunggu Pemeriksaan". Apakah anda yakin?',
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('admin/layanan/ubah-status') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                tableReservasi.ajax.reload();
                swal("Status reservasi berhasil diubah.", {
                  icon: "success",
                });
              }
            });
          } else {
            swal("Gagal mengubah status reservasi.");
          }
        });
      });
  });

</script>
@endpush
