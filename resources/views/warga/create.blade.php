@extends('warga.layouts.app')
@section('content')
<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('warga.store',$warga->no_telepon)}}" method="post" enctype="multipart/form-data" files=true>
              {{ csrf_field() }}
                <fieldset class="content-group col-md-5 col-sm-6">
                    <legend class="text-bold">Data Pelapor</legend>
                    <div class="form-group nama" id="">
                        <label class="control-label col-lg-3">Nomor Telepon <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="" class="form-control" name="" value="{{$warga->no_telepon}}" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group nik" id="nik">
                        <label class="control-label col-lg-3">NIK <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="NIK" class="form-control" name="" value="{{$warga->nik}}" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group nama" id="nama">
                      <label class="control-label col-lg-3">Nama <span class="text-danger">*</span></label>
                      <div class="col-lg-9">
                        <input type="text" placeholder="Nama" class="form-control" name="" value="{{$warga->nama}}" required autofocus disabled>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Jenis Kelamin <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select name="" class="form-control select-search" id="" disabled>
                                <option value="laki-laki" @if($warga->jenis_kelamin == 'laki-laki') selected='selected' @endif>Laki-laki</option>
                                <option value="perempuan" @if($warga->jenis_kelamin == 'perempuan') selected='selected' @endif>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="alert alert-info no-border">
                      <span class="text-semibold"></span> Alamat berikut diisi sesuai dengan alamat pada KTP.
            		    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Provinsi <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="" class="form-control" name="" value="{{$warga->provinsi['name']}}" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kota / Kabupaten <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="" class="form-control" name="" value="{{$warga->kabupaten['name']}}" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="" class="form-control" name="" value="{{$warga->kecamatan['name']}}" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kelurahan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="" class="form-control" name="" value="{{$warga->kelurahan['name']}}" required autofocus disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Status Kependudukan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select name="" class="form-control select-search" id="" disabled>
                                <option value="setempat" @if($warga->status_kependudukan == 'setempat') selected='selected' @endif>Warga Setempat</option>
                                <option value="luar" @if($warga->status_kependudukan == 'luar') selected='selected' @endif>Perantau</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="content-group col-md-7 col-sm-6">
                    <legend class="text-bold">Data yang ditambah</legend>
                    <div class="form-group nama" id="">
                        <label class="control-label col-lg-3">Nomor Telepon <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="number" placeholder="" class="form-control" name="nomor_telepon" value="{{ old('nomor_telepon') }}" required autofocus>
                            @if ($errors->has('nomor_telepon'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_telepon') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group nik" id="nik">
                      <label class="control-label col-lg-3">NIK <span class="text-danger">*</span></label>
                      <div class="col-lg-9">
                        <input type="text" placeholder="NIK" class="form-control" name="nik" value="{{ old('nik') }}" required autofocus>
                        @if ($errors->has('nik'))
                        <label style="padding-top:7px;color:#F44336;">
                          <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nik') }}</strong>
                        </label>
                        @endif
                      </div>
                    </div>
                    <div class="form-group nama" id="nama">
                        <label class="control-label col-lg-3">Nama <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="Nama" class="form-control" name="nama" value="{{ old('nama') }}" required autofocus>
                            @if ($errors->has('nama'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nama') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Jenis Kelamin <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select name="jenis_kelamin" class="form-control select-search" id="">
                                <option value="">Pilih Jenis Kelamin</option>
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                            @if ($errors->has('jenis_kelamin'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('jenis_kelamin') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="alert alert-info no-border">
                      <span class="text-semibold"></span> Alamat berikut diisi sesuai dengan alamat pada KTP.
            		    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Provinsi <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="provinsi" id="provinsi">
                                @foreach ($provinsi as $data)
                                <option value="{{$data->id}}" {{ ($data->name == 'JAWA TENGAH') ? 'selected' : '' }}>{{$data->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('provinsi') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kota / Kabupaten <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="kota_kabupaten" id="kabupaten">
                            </select>
                            @if ($errors->has('kota_kabupaten'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('kota_kabupaten') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kecamatan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="kecamatan" id="kecamatan">
                            </select>
                            @if ($errors->has('kecamatan'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('kecamatan') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Kelurahan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="kelurahan" id="kelurahan">
                            </select>
                            @if ($errors->has('kelurahan'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('kelurahan') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Status Kependudukan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select name="status_kependudukan" class="form-control select-search" id="">
                                <option value="">Pilih Status Kependudukan</option>
                                <option value="setempat">Warga Setempat</option>
                                <option value="luar">Perantau</option>
                            </select>
                            @if ($errors->has('status_kependudukan'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('status_kependudukan') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                </fieldset>

                <div class="col-md-12">

                    <div class="col-xs-6">
                        <a href="{{route('warga.list',$warga->no_telepon)}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
@push('after_script')
<script>
  $(document).ready(function(){
    $('#provinsi').select2();
    $('#kabupaten').select2({
      ajax : {
          url :  "{{ url('select2/data-kabupaten') }}",
          dataType: 'json',
          data: function(params){
              return {
                  term: params.term,
              };
          },
          processResults: function(data){
              return {
                  results: data
              };
          },
          cache : true,
      },
    });
    ajaxChained('#provinsi','#kabupaten','kabupaten');
    ajaxChained('#kabupaten','#kecamatan','kecamatan');
    ajaxChained('#kecamatan','#kelurahan','kelurahan');

    function ajaxChained(source,target,slug){
      $(source).on('change',function(){
      var pid = $(source+' option:selected').val();

      $.ajax({
            type: 'GET',
            url: "{{ url('select2/data') }}"+"/"+slug+"/"+pid,
            dataType: 'html',
            data: { id : pid },
            success: function(txt){
            }
        }).done(function(data){
            data = $.parseJSON(data);
            var list_html = '';
            $.each(data, function(i, item) {
                list_html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
            });
            $(target).html(list_html);
            $(target).select2({placeholder: data.length +' results'});
        });
      })
    }
  });

</script>
@endpush
