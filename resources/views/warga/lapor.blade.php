@extends('warga.layouts.app')
@section('content')
<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('warga.kirimLaporan',$warga->id)}}" method="post" enctype="multipart/form-data" files=true>
                {{ csrf_field() }}
                <fieldset class="content-group">
                  <legend class="text-bold">Laporan Harian</legend>
                  <table class="table">
              			<thead>
              				<tr>
                        <th>No</th>
              					<th>Pertanyaan</th>
              					<th class="col-md-2">Jawaban</th>
              				</tr>
              			</thead>
                    @foreach ($pertanyaan as $key => $value)
              			<tbody>
                        <td width="57px">{{$key+1}}</td>
                        <td>{{$value->pertanyaan}}</td>
                        <td>
                          <div class="form-group">
                            <input type="hidden" name="pertanyaan_id[{{$key}}]" value="{{$value->id}}">
        										<label class="radio-inline">
        											<input type="radio" name="jawaban[{{$key}}]" class="styled" value="ya">
        											Ya
        										</label>
        										<label class="radio-inline">
        											<input type="radio" name="jawaban[{{$key}}]" class="styled" value="tidak">
        											Tidak
        										</label>
        									</div>
                        </td>
              			</tbody>
                    @endforeach
              		</table>
                  <!-- <div class="form-group nama" id="nama">
                      <label class="control-label col-lg-3">Nama <span class="text-danger">*</span></label>
                      <div class="col-lg-9">
                          <input type="text" placeholder="Nama" class="form-control" name="nama" value="{{ old('nama') }}" required autofocus>
                          @if ($errors->has('nama'))
                          <label style="padding-top:7px;color:#F44336;">
                          <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nama') }}</strong>
                          </label>
                          @endif
                      </div>
                  </div>
                  <div class="form-group nik" id="nik">
                      <label class="control-label col-lg-3">NIK <span class="text-danger">*</span></label>
                      <div class="col-lg-9">
                          <input type="text" placeholder="NIK" class="form-control" name="nik" value="{{ old('nik') }}" required autofocus>
                          @if ($errors->has('nik'))
                          <label style="padding-top:7px;color:#F44336;">
                          <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nik') }}</strong>
                          </label>
                          @endif
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-lg-3">Jenis Kelamin <span class="text-danger">*</span></label>
                      <div class="col-lg-9">
                          <select name="jenis_kelamin" class="form-control select-search" id="">
                              <option value="laki-laki">Laki-laki</option>
                              <option value="perempuan">Perempuan</option>
                          </select>
                          @if ($errors->has('jenis_kelamin'))
                          <label style="padding-top:7px;color:#F44336;">
                          <strong><i class="fa fa-times-circle"></i> {{ $errors->first('jenis_kelamin') }}</strong>
                          </label>
                          @endif
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-lg-3">Status Kependudukan <span class="text-danger">*</span></label>
                      <div class="col-lg-9">
                          <select name="status_kependudukan" class="form-control select-search" id="">
                              <option value="setempat">Warga Setempat</option>
                              <option value="luar">Perantau</option>
                          </select>
                          @if ($errors->has('status_kependudukan'))
                          <label style="padding-top:7px;color:#F44336;">
                          <strong><i class="fa fa-times-circle"></i> {{ $errors->first('status_kependudukan') }}</strong>
                          </label>
                          @endif
                      </div>
                  </div> -->
                </fieldset>
            <div>

            <div class="col-xs-6">
                <a href="{{route('warga.list',$trigTel)}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-xs-6 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
@push('after_script')
@endpush
