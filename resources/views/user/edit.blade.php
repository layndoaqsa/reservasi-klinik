@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{url('admin/dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="">User</a></li>
            <li class="active">Ubah</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('user.update',$data->id)}}" method="post" enctype="multipart/form-data" files=true>
            @method('PUT')
            @csrf
                <fieldset class="content-group">
                <legend class="text-bold">Ubah User</legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="name" class="form-control" value="{{ old('name') ? old('name') : $data->name }}" placeholder="">
                        @if ($errors->has('name'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('name') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Email <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="email" name="email" class="form-control" value="{{ old('email') ? old('email') : $data->email }}" placeholder="">
                        @if ($errors->has('email'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('email') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Role <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <div class="multi-select-full">
                            <select id="role" name="role" class="form-control select-search">
                                @foreach($role as $item)
                                <option value="{{$item->id}}" {{ $data->roles->contains($item->id) ? 'selected' : '' }}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('role'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('role') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group" style="display:none" id="container-poli">
                    <label class="control-label col-lg-3">Poli <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="poli" class="form-control select-search" id="">
                            @foreach ($poli as $value)
                                <option value="{{$value->id}}" {{$value->id == $data->poli_id ? 'selected' : ''}}>{{$value->poli}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('poli'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('poli') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>

                <legend class="text-bold">
                  Ubah Password
                  <small>(Diisi jika ingin mengubah password)</small>
                </legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Password <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="password" name="password" class="form-control" value="" placeholder="">
                        @if ($errors->has('password'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('password') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Password Confirmation <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="password" name="password_confirmation" class="form-control" value="" placeholder="">
                        @if ($errors->has('password_confirmation'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('password_confirmation') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                </fieldset>
            <div>

            <div class="col-md-4">
                <a href="{{route('user.index')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-md-8 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection

@push('after_script')
    <script>
            $(document).ready(function(){
                $('#role').change(function(){
                    var role = $('#role').find(":selected").text();
                    if (role == 'Dokter') {
                        $('#container-poli').show();
                    } else {
                        $('#container-poli').hide();
                    }
                });
                if ($('#role').find(":selected").text() == 'Dokter') {
                    $('#container-poli').show();
                }
            });
    </script>
@endpush
