@extends('pasien.layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('pasien.list')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="">Calon Pasien</a></li>
            <li class="active">Ubah</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('calon-pasien.update',$data->id)}}" method="post" enctype="multipart/form-data" files=true>
            @method('PUT')
            @csrf
                <fieldset class="content-group">
                <legend class="text-bold">Ubah Data</legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') ? old('nama') : $data->nama }}" placeholder="" required>
                        @if ($errors->has('nama'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nama') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Nomor Telepon <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" maxlength="14" minlength="10" name="nomor_telepon" class="form-control" value="{{ old('nomor_telepon') ? old('nomor_telepon') : $data->no_telepon }}" placeholder="" required>
                        @if ($errors->has('nomor_telepon'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_telepon') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">NIK <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" maxlength="16" minlength="16" name="nik" class="form-control" value="{{ old('nik') ? old('nik') : $data->nik }}" placeholder="" required>
                        @if ($errors->has('nik'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nik') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Kartu BPJS / Kartu Sehat <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                      <div class="form-group">
                        <label class="radio-inline">
                          <input type="radio" name="kartu_bpjs_atau_kartu_sehat" class="styled" value="ada" required id="ada" {{ ($data->no_bpjs) ? 'checked' : '' }}>
                          Ada
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="kartu_bpjs_atau_kartu_sehat" class="styled" value="tidak" required id="tidak_ada" {{ (!$data->no_bpjs) ? 'checked' : '' }}>
                          Tidak Ada
                        </label>
                      </div>
                      @error('kartu_bpjs_atau_kartu_sehat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                      @enderror
                    </div>
                </div>
                <div class="form-group" style="display:none" id="bpjs">
                    <label class="control-label col-lg-3"></label>
                    <div class="col-lg-9">
                        <input type="number" name="nomor_bpjs_atau_kartu_sehat" class="form-control" value="{{ old('nomor_bpjs_atau_kartu_sehat') ? old('nomor_bpjs_atau_kartu_sehat') : $data->no_bpjs }}" placeholder="NO BPJS / KARTU SEHAT" id="nomor_bpjs_atau_kartu_sehat">
                        @if ($errors->has('nomor_bpjs_atau_kartu_sehat'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_bpjs_atau_kartu_sehat') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                </fieldset>
            <div>

            <div class="col-xs-6">
                <a href="{{route('pasien.list')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-xs-6 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>

            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
@push('after_script')
<script type="text/javascript">
  $(document).ready(function(){
    var val;
    val = $("input[name='kartu_bpjs_atau_kartu_sehat']:checked").val();
    if (val == "ada") {
      $("#bpjs").show();
      $("#nomor_bpjs_atau_kartu_sehat").attr('required', '');
    }
    $("input[name='kartu_bpjs_atau_kartu_sehat']").change(function(){
      val = $("input[name='kartu_bpjs_atau_kartu_sehat']:checked").val();
      if (val == 'ada') {
        $("#bpjs").show();
        $("#nomor_bpjs_atau_kartu_sehat").attr('required', '');
      } else {
        $("#bpjs").hide();
        $("#nomor_bpjs_atau_kartu_sehat").removeAttr('required');
      }

    });
  });
</script>
@endpush
