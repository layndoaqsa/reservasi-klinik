@extends('pasien.layouts.app')
@section('jadwal','active')
@section('informasi','active')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Informasi Jadwal Poli</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('pasien.list')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Informasi Jadwal</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="btn-group col-md-12" data-toggle="buttons" id="hari_id" style="margin-bottom:5px;padding:0px">
        @foreach (\App\Hari::all() as $key => $value)
          <label class="btn btn-default col-md-1 col-xs-3">
            <input id="{{$value->id}}" type="radio" name="hari_id" value="{{$value->id}}"> {{$value->hari}}
          </label>
        @endforeach
      </div>
      <table id="table-jadwal" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th style="width:1%">No</th>
            <th style="width:49%">Poli</th>
            <th  style="width:50%">Sesi dan Dokter</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
      <div>
          <a href="{{route('pasien.list')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
      </div>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tableSesi;
  $(document).ready(function(){
    $('#1').trigger('click').parent().removeClass('btn-default').addClass('bg-teal-300');
    $('#sesi_id').val(1);

    $("input[name='hari_id']").change(function(){
      checked = $("input[name='hari_id']:checked").val();
      $('#sesi_id').val(checked);
      $("input[name='hari_id']:checked").parent().removeClass('btn-default').addClass('bg-teal-300');
      $("input[name='hari_id']:unchecked").parent().removeClass('bg-teal-300').addClass('btn-default');
      tableSesi.draw(true);
    });
		/* tabel user */
    tableSesi = $('#table-jadwal').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('informasi-jadwal/get-jadwal') }}",
          type: "GET",
          data: function (d) {
                d.hari_id = $("input[name='hari_id']:checked").val()
              }
      },

      columns: [
          { data: 'id', name:'id', visible:false, orderable: false,},
          { data: 'DT_RowIndex', name:'DT_RowIndex', visible:true, orderable: false,},
          { data: 'poli.poli', name:'poli.poli', visible:true, orderable: false,},
          { data: 'waktu_sesi', name:'waktu_sesi', visible:true, orderable: false,},
      ],
    });
  });

</script>
@endpush
