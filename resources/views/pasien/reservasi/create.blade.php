@extends('pasien.layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('pasien.list')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Buat Antrian</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <form class="form-horizontal form-validate-jquery" action="{{route('reservasi.store',$data->id)}}" method="post" enctype="multipart/form-data" files=true>

            <div class="panel-body">

                {{ csrf_field() }}
                <fieldset class="content-group">
                <legend class="text-bold">Buat Antrian</legend>
                <div class="form-group">
                    <label class="control-label col-lg-2">Nama <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') ? old('nama') : $data->nama }}" placeholder="" readonly>
                        @if ($errors->has('nama'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nama') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Nomor Telepon <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="number" maxlength="14" minlength="10" name="nomor_telepon" class="form-control" value="{{ old('nomor_telepon') ? old('nomor_telepon') : $data->no_telepon }}" placeholder="" readonly>
                        @if ($errors->has('nomor_telepon'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_telepon') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">NIK <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="number" maxlength="16" minlength="16" name="nik" class="form-control" value="{{ old('nik') ? old('nik') : $data->nik }}" placeholder="" readonly>
                        @if ($errors->has('nik'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nik') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Nomor BPJS </label>
                    <div class="col-lg-10">
                        <input type="number" name="nomor_bpjs" class="form-control" value="{{ old('nomor_bpjs') ? old('nomor_bpjs') : $data->no_bpjs }}" placeholder="" readonly>
                        @if ($errors->has('nomor_bpjs'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('nomor_bpjs') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                @if($errors->any())
                <div class="alert alert-danger alert-styled-left alert-bordered">
                  <span class="text-semibold">Oops! </span> {{$errors->first()}}
                </div>
                @endif
                <div class="form-group">
                    <label class="control-label col-lg-2">Poli <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <div class="btn-group col-md-12" data-toggle="buttons" style="margin:0px;padding:0px">
                          @foreach ($poli as $value)
                            <label class="btn {{$value->color}} btn-ligh text-center col-md-2 col-xs-6" style="margin:5px 0px 5px 0px;outline: 1px solid #ddd;">
                                <!-- <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i> -->
                                <img src="{{asset('storage/images/icon-poli/'.$value->icon.'')}}" alt="error" style="width:60px">
                                <br>
                                <input type="radio" name="poli" id="poli" value="{{$value->id}}">{{$value->poli}}
                            </label>
                          @endforeach
                        </div>
                        @if ($errors->has('poli'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('poli') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Tanggal <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="hidden" name="tanggal">
                        <div class="btn-group btn-group-justified btn-group-lg my-btn-group-responsive" id="tanggal">

                        </div>
                        @if ($errors->has('tanggal'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('tanggal') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>

                <div id="hide">
                    <div class="form-group">
                        <label class="control-label col-lg-2">Keluhan <span class="text-danger">*</span></label>
                        <div class="col-lg-10">
                            <textarea type="text" name="keluhan" rows="3" class="form-control"  placeholder="">{{ old('keluhan') }}</textarea>
                            @if ($errors->has('keluhan'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('keluhan') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Kekhawatiran</label>
                        <div class="col-lg-10">
                            <textarea type="text" name="kekhawatiran" rows="3" class="form-control"  placeholder="">{{ old('kekhawatiran') }}</textarea>
                            @if ($errors->has('kekhawatiran'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('kekhawatiran') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Riwayat Penyakit Menahun</label>
                        <div class="col-lg-10">
                            <textarea type="text" name="riwayat_penyakit_menahun" rows="3" class="form-control"  placeholder="">{{ $lastReservasi ? $lastReservasi->riwayat_penyakit_menahun : '' }}</textarea>
                            @if ($errors->has('riwayat_penyakit_menahun'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('riwayat_penyakit_menahun') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Upaya Pengobatan Yang Dilakukan</label>
                        <div class="col-lg-10">
                            <textarea type="text" name="upaya_pengobatan" rows="3" class="form-control"  placeholder="">{{ old('upaya_pengobatan') }}</textarea>
                            @if ($errors->has('upaya_pengobatan'))
                            <label style="padding-top:7px;color:#F44336;">
                            <strong><i class="fa fa-times-circle"></i> {{ $errors->first('upaya_pengobatan') }}</strong>
                            </label>
                            @endif
                        </div>
                    </div>
                </div>
                
                </fieldset>
            <div>

                <div class="col-xs-6">
                    <a href="{{route('pasien.list')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="submit" id="buat-antrian" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>

            </div>

        </form>
    </div>
</div>
<!-- /content area -->
@endsection
@push('after_style')
<style>
    @media (max-width: 991px) {
    .btn-group.my-btn-group-responsive > .btn {
        display: block;
        width: 100%;
    }

    /* making the border-radius correct */
    .btn-group.my-btn-group-responsive > .btn:first-child {
        border-radius: 6px 6px 0 0;
    }
    .btn-group.my-btn-group-responsive > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
        border-top-right-radius: 6px;
    }
    .btn-group.my-btn-group-responsive > .btn:last-child:not(:first-child) {
        border-radius: 0 0 6px 6px;
    }

    /* fixing margin */
    .btn-group.my-btn-group-responsive .btn + .btn {
        margin-left: 0;
    }

    }
</style>
@endpush
@push('after_script')
<script>
    $(document).ready(function(){
        $("input[name='kartu_bpjs_atau_kartu_sehat']").change(function(){
          val = $("input[name='kartu_bpjs_atau_kartu_sehat']:checked").val();
          if (val == 'ada') {
            $("#bpjs").slideDown('fast');
            $("#nomor_bpjs_atau_kartu_sehat").attr('required', '');
          } else {
            $("#bpjs").slideUp('fast');
            $("#nomor_bpjs_atau_kartu_sehat").removeAttr('required');
          }
        });

        $("input[name='poli']").change(function(){
            poli = $(this).val();
            $('#jam').html('');
            $('#sesi').html('');
            $('#alert-sesi').hide();
            $.ajax({
            url: "{{url('reservasi/cek') }}"+"/"+poli
            }).done(function(data) {
                var tanggal = '';
                if (Array.isArray(data)) {
                    $.each(data, function(i, item) {
                        let waktu = '';
                        let arrayDokter = [];
                        $.each(data[i].dokter[0]['sesi'], function(j, sesi) {
                            let dokter = '';
                            $.each(sesi.dokter_sesi, function(j, nama) {
                                if (arrayDokter.includes(nama.dokter.name)) {
                                } else {
                                    dokter += '<li>'+nama.dokter.name+'</li>';
                                    arrayDokter.push(nama.dokter.name);
                                }
                            });
                            waktu += '<div style="text-align:left;white-space:break-spaces;padding-left:5px;">'+dokter+'</div>'
                        });
                    tanggal += '<div style="outline: 1px solid #ddd;padding:0;margin:0;vertical-align:top;" class="btn tanggal" data-value="'+data[i].tanggal+'">'+
                                '<div style="height:50px" class="header '+data[i].color+'"><p style="padding-top:15px">'+data[i].hari+', '+data[i].keterangan+'</p></div>'
                                +'<div style="margin-top:10px;margin-bottom:10px">'+waktu+'</div>'+
                            '</div>';
                    $('#hide').show();
                    $('#buat-antrian').show();
                    $('#tanggal').html(tanggal);
                    });
                } else {
                    $('#hide').hide();
                    $('#buat-antrian').hide();
                    $('#tanggal').parent().find('#notif-penuh').remove();
                    $('#tanggal').html('<span class="label bg-info label-rounded">'+data+'</span>');
                }
            });
        });

        $(document).on("click", ".tanggal", function () {
            tanggal = $(this).data('value');
            $("input[name='tanggal']").val(tanggal)
            $('.header').removeClass('bg-teal-700');
            $(this).find('.header').addClass('bg-teal-700');
            if ($(this).find('.header').hasClass('bg-grey-300')) {
                $('#hide').hide();
                $('#buat-antrian').hide();
                $('#tanggal').parent().find('#notif-penuh').remove();
                $('#tanggal').parent().append('<span style="margin-top:15px" id="notif-penuh" class="label bg-info label-rounded">Reservasi untuk tanggal tersebut sudah penuh</span>')
            } else {
                $('#hide').show();
                $('#buat-antrian').show();
                $('#tanggal').parent().find('#notif-penuh').remove();
            }
        });

        $(document).on("change", "input[name='poli']", function () {
            poli = $("input[name='poli']:checked").val()
            $("input[name='poli']:checked").parent().removeClass('bg-teal-300').addClass('bg-teal-700');
            $("input[name='poli']:unchecked").parent().removeClass('bg-teal-700').addClass('bg-teal-300');
        })
    });
</script>
@endpush