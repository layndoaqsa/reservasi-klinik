@extends('pasien.layouts.app')
@section('reservasiku','active')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('pasien.list')}}"><i class="icon-home2 position-left"></i>Dashboard</a></li>
            <li class="active">Reservasi Ku</li>
        </ul>

    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content" id="content-reservasiku">
    <div class="panel panel-flat col-md-12">
        <div class="panel-body" style="text-align:center">
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a id="reload" data-action="reload"></a></li>
            </ul>
          </div>
          {{ csrf_field() }}
          <fieldset class="content-group">
          <legend class="text-bold">Daftar Reservasi Puskesmas Kalibawang</legend>
            @foreach ($reservasi as $key => $value)
            <div class="content">
                <div class="panel panel-flat">
                      <div class="panel-body" style="text-align:center">
                      {{ csrf_field() }}
                      <fieldset class="content-group">
                        <legend class="text-bold">Reservasi Puskesmas Kalibawang</legend>
                        <div class="col-md-12">
                          <label class="text-bold" style="font-size:40pt">{{$value->antrian}}</label>
                        </div>
                        <div class="col-md-12">
                          <label class="text-bold" style="font-size:10pt">{{$value->kode}}</label>
                        </div>
                          <label class="control-label col-lg-12"><legend class="text-bold">{{$value->poli->poli}}</legend></label>
                        <div class="col-md-12">
                          <label class="control-label">{{\Carbon\Carbon::parse($value->tanggal)->format('j F Y')}}</label>
                        </div>
                        @if ($value->status == 'Reservasi' || $value->status == 'Menunggu Pemeriksaan' || $value->status == 'Sedang Pemeriksaan' || $value->status == 'Terlewat Pemanggilan')
                        <div class="col-md-12">
                          @if ($value->tanggal == date('Y-m-d'))
                          <label class="control-label">Perkiraan Pelayanan pukul: <b>{{App\Http\Controllers\LayananController::perkiraan_pelayanan($value->antrian,$value->tanggal,$value->poli_id)}}</b></label>
                          @else
                          <label class="control-label">Perkiraan Pelayanan pukul: <b>{{App\Http\Controllers\ReservasiController::perkiraan_pelayanan($value->antrian,$value->tanggal,$value->poli_id)}}</b></label>
                          @endif
                        </div>
                        @endif
                        <div class="col-md-12">
                          <label class="control-label">Hasil Screening: <b>{{$value->hasil_screening->hasilSkor['hasil']}}</b> </label>
                        </div>
                        <div class="col-md-12">
                          @if ($value->status == 'Batal')
                          <span class="label bg-danger label-rounded">Reservasi Dibatalkan</span>
                          @else
                            <span class="label bg-info label-rounded">{{$value->status}}</span>
                            @if ($value->status == 'Reservasi')
                              @if (\Carbon\Carbon::parse($value->tanggal . $value->sesi_mulai)->addMinutes(-120) > \Carbon\Carbon::now())
                                <br><button style="margin-top:3px" value="{{$value->id}}" id="batal-reservasi" class="label bg-danger label-rounded"><i class="icon-alarm-cancel position-left"></i>Batalkan Reservasi</button>
                              @endif
                            @endif
                          @endif
                        </div>
                      </fieldset>
                      @if ($value->status != 'Batal')
                      <div>
                        <div class="col-xs-12">
                          <a style="margin-top:3px" href="{{route('reservasi.noLoginUnduh',$value->kode)}}" id="" class="label bg-teal label-rounded"><i class="icon-download position-left"></i> Unduh Reservasi</a>
                        </div>
                      </div>
                      @endif
                    </div>
                </div>
            </div>
            @endforeach
            {{ $reservasi->links() }}
          </fieldset>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
@push('after_script')
<script>
$(document).ready(function(){
  $('button#batal-reservasi').on('click', function () {
    var reservasiID = $(this).val();
    swal({
      // title: "Are you sure?",
      text: " Apakah anda yakin? Reservasi tidak bisa dikembalikan lagi.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url: "{{ url('reservasi/batal') }}"+"/"+reservasiID,
          method: 'get',
          success: function(result){
            if (result.data === 'error') {
              swal("Pembatalan hanya dapat dilakukan 2 jam sebelum klinik buka.",{
                icon: "error",
              });
              location.reload();
            } else {
              swal("Reservasi berhasil dibatalkan.", {
                icon: "success",
              });
              location.reload();
            }
          }
        });
      } else {
        swal("Pembatalan reservasi gagal.");
      }
    });

    $('#reload').click(function(){
      $("#content-reservasiku").load(location.href + " #content-reservasiku");
    });
  });
});
</script>
@endpush
