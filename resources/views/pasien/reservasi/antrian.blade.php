@extends('pasien.layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i>Dashboard</a></li>
            <li class="active">Antrian</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body" style="text-align:center">
            {{ csrf_field() }}
            <fieldset class="content-group">
                <legend class="text-bold">Reservasi Puskesmas Kalibawang</legend>
                <div class="col-md-12">
                  <label class="text-bold" style="font-size:40pt">{{$reservasi->antrian}}</label>
                </div>
                <div class="col-md-12">
                  <label class="text-bold" style="font-size:10pt">{{$reservasi->kode}}</label>
                </div>
                  <label class="control-label col-lg-12"><legend class="text-bold">{{$reservasi->poli->poli}}</legend></label>
                <div class="col-md-12">
                  <label class="control-label">Perkiraan Pelayanan pukul: <b>{{$reservasi->perkiraan_jam_pelayanan}}, {{\Carbon\Carbon::parse($reservasi->tanggal)->format('j F Y')}} <b></label>
                </div>
                <div class="col-md-12">
                  <label class="control-label">Hasil Screening:<b> {{$hasil->hasilSkor['hasil']}}</b></label>
                </div>
                @if ($reservasi->status != 'Batal')
                <div>
                  <div class="col-xs-12">
                    <a style="margin-top:3px" href="{{route('reservasi.noLoginUnduh',$reservasi->kode)}}" id="" class="label bg-teal label-rounded"><i class="icon-download position-left"></i> Unduh Reservasi</a>
                  </div>
                </div>
                @endif
            </fieldset>
            <div>
              <div class="col-xs-12">
                  <a href="{{route('home')}}"type="button" class="btn btn-default" id=""> <i class="icon-home2"></i> Beranda</a>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
