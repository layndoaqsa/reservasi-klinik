@extends('pasien.layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Screening</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Screening</li>
            <li class="active">Hasil</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div>
              <fieldset class="content-group  text-center">
                  <legend class="text-bold">Hasil Skrining COVID-19 </legend>
                  <div class="alert alert-info no-border col-md-12">
                    <!-- Hasil dari screening mandiri sebagai berikut : <br> -->
                    <h4 style="font-family:sans-serif;">Anda termasuk dalam kategori <span class="text-semibold">{{$hasil->hasil}}.</span></h4>
                    @if ($hasil->gambar)
                      <img src='{{$gambar}}' alt="" style="width:150px"><br>
                    @endif
                    <h4 style="font-family:sans-serif;">Selanjutnya, yang harus Anda lakukan adalah: <br>
                    {{$hasil->tatalaksana}}</h4>

                  </div>
                  <div class="col-md-12" style="margin-bottom:10px">
                    <a href="{{route('home')}}" type="button" class="btn btn-default" id=""> <i class="icon-home5"></i> Beranda</a>
                  </div>
              </fieldset>
            </div>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
