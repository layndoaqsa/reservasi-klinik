<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            @if(Auth::user()->hasAnyRole('Dokter'))
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li><a href="{{route('reservasiadmin.index')}}"><i class="icon-book2"></i><span>Reservasi</span></a></li>
            @endif
            @if(Auth::user()->hasAnyRole('Layanan Reservasi'))
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li><a href="{{route('layanan.index')}}"><i class="icon-book2"></i><span>Reservasi</span></a></li>
            <li><a href="{{route('layanan.pasienLama')}}"><i class="icon-certificate"></i><span>Pasien Lama</span></a></li>
            <li><a href="{{route('layanan.pasienBaru')}}"><i class="icon-clipboard"></i><span>Pasien Baru</span></a></li>
            @endif
            @if(Auth::user()->hasAnyRole('Super Admin'))
            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <!-- <li><a href="#"><i class="icon-home4"></i><span>Dashboard</span></a></li> -->
            <li><a href="{{route('reservasiadmin.index')}}"><i class="icon-book2"></i><span>Reservasi</span></a></li>
            <li><a href="{{route('poli.index')}}"><i class="icon-magazine"></i><span>Jadwal Poli</span></a></li>
            <li><a href="{{route('laporan.index')}}"><i class="icon-file-text"></i><span>Laporan</span></a></li>
            <li>
                <a href="#"><i class="icon-question6"></i><span>Pertanyaan</span></a>
                <ul>
                    <li><a href="{{route('pertanyaan.index')}}">Pertanyaan</a></li>
                    <li><a href="{{route('skor.index')}}">Skor</a></li>
                </ul>
            </li>
            <li><a href="{{route('user.index')}}"><i class="icon-person"></i><span>User</span></a></li>
            <!-- /main -->
            @endif
        </ul>
    </div>
</div>
