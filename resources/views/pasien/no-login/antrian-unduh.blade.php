<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>RESERVASI PUSKESMAS KALIBAWANG</title>

    <style>
    .text-center{
      text-align: center;
    }
    li {
      text-align: center;
      list-style-type : none;
    }

    /* th, td {
      padding: 7px;
    } */
    .page-break {
    page-break-after: always;
    }
    </style>
  </head>
  <body>
    <div>
      <li>
        RESERVASI PUSKESMAS KALIBAWANG
      </li>
      <hr>
      <li style="font-size:72px">
        {{$data->antrian}}
      </li>
      <li style="font-size:24px">
        {{$data->kode}}
      </li>
      <li>
        Poli {{$data->poli['poli']}}
      </li>
      <br>
      <br>
      <li>
        Perkiraan pelayanan pukul: <br> {{$data->perkiraan_jam_pelayanan}}, {{\Carbon\Carbon::parse($data->tanggal)->format('j F Y')}}
      </li>
      <br>
      <br>
      <li>
        Hasil Screening:<br> {{$hasil->hasilSkor['hasil']}}
      </li>
      <br>
      <br>
      <p style="font-size:14px">
        nb: anda dapat mengecek reservasi melalui
          <a href="{{route('reservasi.lihat',$data->kode)}}"> link ini </a>
      </p>
    </div>
  </body>
</html>
