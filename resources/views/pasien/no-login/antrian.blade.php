@extends('pasien.no-login.layout')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Reservasi</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body" style="text-align:center">
            {{ csrf_field() }}
            <fieldset class="content-group">
              <legend class="text-bold">Reservasi Puskesmas Kalibawang</legend>
              <div class="col-md-12">
                <label class="text-bold" style="font-size:40pt">{{$reservasi->antrian}}</label>
              </div>
              <div class="col-md-12">
                <label class="text-bold" style="font-size:16pt">{{$reservasi->kode}}</label>
              </div>
                <label class="control-label col-lg-12"><legend class="text-bold">{{$reservasi->poli->poli}}</legend></label>
                @if ($reservasi->status == 'Reservasi' || $reservasi->status == 'Menunggu Pemeriksaan' || $reservasi->status == 'Sedang Pemeriksaan' || $reservasi->status == 'Terlewat Pemanggilan')
                <div class="col-md-12">
                  @if ($reservasi->tanggal == date('Y-m-d'))
                  <label class="control-label">Perkiraan Pelayanan pukul: <b>{{App\Http\Controllers\LayananController::perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id)}}, {{\Carbon\Carbon::parse($reservasi->tanggal)->format('j F Y')}}</b></label>
                  @else
                  <label class="control-label">Perkiraan Pelayanan pukul: <b>{{App\Http\Controllers\ReservasiController::perkiraan_pelayanan($reservasi->antrian,$reservasi->tanggal,$reservasi->poli_id)}}, {{\Carbon\Carbon::parse($reservasi->tanggal)->format('j F Y')}}</b></label>
                  @endif
                </div>
                @endif
              <div class="col-md-12">
                <label class="control-label">Hasil Screening:<b> {{$hasil->hasilSkor['hasil']}}</b></label>
              </div>
              <div class="col-md-12">
                @if ($reservasi->status == 'Batal')
                <span class="label bg-danger label-rounded">Reservasi Dibatalkan</span>
                @else
                  <span class="label bg-info label-rounded">{{$reservasi->status}}</span>
                  @if ($reservasi->status == 'Reservasi')
                    @if (\Carbon\Carbon::parse($reservasi->tanggal . $reservasi->sesi_mulai)->addMinutes(-120) > \Carbon\Carbon::now())
                      <br><button style="margin-top:3px" value="{{$reservasi->id}}" id="batal-reservasi" class="label bg-danger label-rounded"><i class="icon-alarm-cancel position-left"></i>Batalkan Reservasi</button>
                    @endif
                  @endif
                @endif
              </div>
            </fieldset>
            @if ($reservasi->status != 'Batal')
            <div>
              <div class="col-xs-12">
                <a style="margin-top:3px" href="{{route('reservasi.noLoginUnduh',$reservasi->kode)}}" id="" class="label bg-teal label-rounded"><i class="icon-download position-left"></i> Unduh Reservasi</a>
              </div>
              <div class="col-xs-12">
                <a style="margin-top:3px" href="{{url('/')}}" id="" class="label bg-orange label-rounded"><i class="icon-exit position-left"></i> Keluar</a>
              </div>
            </div>
            @endif
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
@push('after_script')
<script>
$(document).ready(function(){
  $('button#batal-reservasi').on('click', function () {
    var reservasiID = $(this).val();
    swal({
      // title: "Are you sure?",
      text: " Apakah anda yakin? Reservasi tidak bisa dikembalikan lagi.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          url: "{{ url('reservasi/batal') }}"+"/"+reservasiID,
          method: 'get',
          success: function(result){
            if (result.data === 'error') {
              swal("Pembatalan hanya dapat dilakukan 2 jam sebelum klinik buka.",{
                icon: "error",
              });
              location.reload();
            } else {
              swal("Reservasi berhasil dibatalkan.", {
                icon: "success",
              });
              location.reload();
            }
          }
        });
      } else {
        swal("Pembatalan reservasi gagal.");
      }
    });

    $('#reload').click(function(){
      $("#content-reservasiku").load(location.href + " #content-reservasiku");
    });
  });
});
</script>
@endpush
