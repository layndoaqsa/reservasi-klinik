@extends('pasien.no-login.layout')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Screening</span></h4>
        </div>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('screening.store',$reservasi->id)}}" method="post" enctype="multipart/form-data" files=true>
                {{ csrf_field() }}
                <fieldset class="content-group">
                  <legend class="text-bold">Screening COVID-19</legend>
                  <table class="table">
              			<thead>
              				<tr>
                        <th>No</th>
              					<th>Pertanyaan</th>
              					<th>Jawaban</th>
              				</tr>
              			</thead>
                    @foreach ($pertanyaan as $key => $value)
              			<tbody>
                        <td width="57px">{{$key+1}}</td>
                        <td>{{$value->pertanyaan}}</td>
                        <td class="col-md-2">
                          <div class="form-group col-md-12 row">
                            <input type="hidden" name="pertanyaan_id[{{$key}}]" value="{{$value->id}}">
                            <div class="col-md-6" style="margin:0px;padding:0px">
                              <label class="radio-inline">
                                <input type="radio" name="jawaban[{{$key}}]" class="styled" value="ya" required>
                                Ya
                              </label>
                            </div>
                            <div class="col-md-6" style="margin:0px;padding:0px">
                              <label class="radio-inline">
                                <input type="radio" name="jawaban[{{$key}}]" class="styled" value="tidak" required>
                                Tidak
                              </label>
                            </div>
        									</div>
                        </td>
              			</tbody>
                    @endforeach
              		</table>
                </fieldset>
            <div>

            <!-- <div class="col-xs-6">
                <a href=""type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div> -->
                <div class="col-xs-12 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
