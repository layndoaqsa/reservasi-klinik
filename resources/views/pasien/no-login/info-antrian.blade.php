@extends('pasien.no-login.layout')

@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Informasi Antrian Hari Ini</span></h4>
            <label for=""> Update {{\Carbon\Carbon::now()->format('H:i:s')}} - </label>
            <a href="" id="refresh"> <i class="fa fa-refresh"></i> refresh </a>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Info Antrian</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <div class="col-lg-12">
      <div class="btn-group col-xs-12" data-toggle="buttons" style="margin:0px;padding:0px">
        @foreach ($poli as $value)
          @if ($value->id == $data->id)
          <label class="btn bg-teal-800 btn-ligh text-center col-md-2 col-sm-3 col-xs-6" style="margin:5px 0px 5px 0px;outline: 1px solid #ddd;">
              <img src="{{asset('storage/images/icon-poli/'.$value->icon.'')}}" alt="error" style="width:60px">
              <br>
              <input type="radio" name="poli" id="poli" value="{{$value->kode}}">{{$value->poli}}
          </label>
          @else
          <label class="btn bg-teal-300 btn-ligh text-center col-md-2 col-sm-3 col-xs-6" style="margin:5px 0px 5px 0px;outline: 1px solid #ddd;">
            <img src="{{asset('storage/images/icon-poli/'.$value->icon.'')}}" alt="error" style="width:60px">
            <br>
            <input type="radio" name="poli" id="poli" value="{{$value->kode}}">{{$value->poli}}
          </label>
          @endif
        @endforeach
      </div>
      <div style="display:none">
        <form action="{{route('infoAntrian')}}" method="get" enctype="multipart/form-data" files=true>
          <input type="" name="kode_poli" value="" id="kode_poli">
          <button type="submit" id="btn-submit">Tambah Sesi</button>
        </form>
      </div>
  </div>
  <!-- State saving -->
  <div class="col-md-12">
      <div class="btn btn-default btn-ligh text-center col-md-12 col-xs-12" style="margin-top:5px">
          <div class="col-md-12">
              <h4 style="margin-bottom:0px;padding:bottom:0px"><b>Poli Umum</b></h4>
              {{$data->total_pasien_terlayani}} dari {{$data->total_pasien}} Pasien Terlayani
              <hr>
          </div>
          <div class="col-sm-12">
            <div class="col-sm-6" style="margin-bottom:10px">
              <span class="label label-flat border-info text-info-600 col-xs-6 col-xs-offset-3">
                <h1 style="margin:0 0 0 0;padding:0 0 0 0"><b>{{ $data->diperiksa ? $data->diperiksa->antrian : '-' }}</b></h1>
              </span>
              <div class="col-xs-12">
                (Sedang Diperiksa)
              </div>
            </div>
            <div class="col-sm-6">
              <span class="label label-flat border-warning text-warning-600 col-xs-6 col-xs-offset-3">
                <h1 style="margin:0 0 0 0;padding:0 0 0 0">
                  <b>
                    @if (!empty($data->antrian_selanjutnya[0]))
                        @if (!empty($data->antrian_selanjutnya->whereNotNull('counter')))
                            @if ($data->antrian_selanjutnya->whereNotNull('counter')->last())
                              {{$data->antrian_selanjutnya->whereNotNull('counter')->last()->antrian}}
                            @else
                              {{$data->antrian_selanjutnya[0]['antrian']}}
                            @endif
                        @else
                            {{$data->antrian_selanjutnya[0]['antrian']}}
                        @endif
                    @else
                        -
                    @endif                
                  </b>
                </h1>
              </span>
              <div class="col-xs-12">
                (Antrian Selanjutnya)
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <hr>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="table-responsive">
  							<table class="table table-bordered table-framed table-xxs">
  								<thead>
  									<tr class="bg-teal">
  										<th class="text-center">Daftar Antrian</th>
  									</tr>
  								</thead>
  								<tbody>
                    @if(!empty($data->antrian_selanjutnya[0]))
                      @foreach ($data->antrian_selanjutnya as $key => $value)
                      <tr>
                        @if ($value->status == 'Terlewat Pemanggilan')
                        <td>{{$value->antrian}} ({{$value->status}})</td>
                        @else
                        <td>{{$value->antrian}}</td>
                        @endif
                      </tr>
                      @endforeach
                    @else
                      <tr>
                        <td>-</td>
                      </tr>
                    @endif
  								</tbody>
  							</table>
  						</div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="table-responsive">
  							<table class="table table-bordered table-framed table-xxs">
  								<thead>
  									<tr class="bg-teal">
  										<th class="text-center">Belum Datang</th>
  									</tr>
  								</thead>
                  <tbody>
                    @if (!empty($data->antrian_belum_datang[0]))
                      @foreach ($data->antrian_belum_datang as $key => $value)
                      <tr>
                        <td>{{$value->antrian}}</td>
                      </tr>
                      @endforeach
                    @else
                      <tr>
                        <td>-</td>
                      </tr>
                    @endif
                  </tbody>
  							</table>
  						</div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="table-responsive">
  							<table class="table table-bordered table-framed table-xxs">
  								<thead>
  									<tr class="bg-teal">
  										<th class="text-center">Terlewati</th>
  									</tr>
  								</thead>
                  <tbody>
                    @if (!empty($data->antrian_terlewati[0]))
                      @foreach ($data->antrian_terlewati as $key => $value)
                      <tr>
                        <td>{{$value->antrian}}</td>
                      </tr>
                      @endforeach
                    @else
                      <tr>
                        <td>-</td>
                      </tr>
                    @endif
                  </tbody>
  							</table>
  						</div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="table-responsive">
  							<table class="table table-bordered table-framed table-xxs">
  								<thead>
  									<tr class="bg-teal">
  										<th class="text-center">Selesai</th>
  									</tr>
  								</thead>
                  <tbody>
                    @if (!empty($data->antrian_selesai[0]))
                      @foreach ($data->antrian_selesai as $key => $value)
                      <tr>
                        <td>{{$value->antrian}}</td>
                      </tr>
                      @endforeach
                    @else
                      <tr>
                        <td>-</td>
                      </tr>
                    @endif
                  </tbody>
  							</table>
  						</div>
            </div>
          </div>
      </div>
  </div>

	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_style')
@endpush

@push('after_script')
<script>
    $(document).ready(function(){
        $(document).on("change", "input[name='poli']", function () {
            poli = $("input[name='poli']:checked").val()
            $('#kode_poli').val(poli);
            jQuery('#btn-submit').click();
        })
        $( "#refresh" ).click(function() {
          location.reload()
        });
    });
</script>
@endpush
