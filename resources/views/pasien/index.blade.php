@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Pasien</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Pasien</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
        <div class="col-md-3">
            <button id="btn-import" class="btn btn-primary btn-sm bg-primary-800"><i class="icon-add position-left"></i>Import Data</button>
            <a href="{{route('pasien.download-template')}}" class="btn btn-primary btn-sm bg-primary-800"><i class="icon-download position-left"></i>Download Template</a>
        </div>
        <table id="table-pasien" class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>NIK</th>
                        <th>No HP</th>
                        <th>No BPJS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->

<div id="modal-import" class="modal fade">
	<div class="modal-dialog">
        <form class="form-horizontal form-validate-jquery" action="{{route('pasien.import')}}" method="post" enctype="multipart/form-data" files=true>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Import Data</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
  						<div class="row">
  							<div class="col-md-12">
                                <input type="file" name="file" class="form-control" required accept=".xls, .xlsx" id="">
  							</div>
  						</div>
  					</div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
	</div>
</div>
@endsection

@push('after_script')
<script>
var tablePasien;
  $(document).ready(function(){
    $('#btn-import').click(function(){
        $('#modal-import').modal('show');
    });
	/* tabel pasien */
    tablePasien = $('#table-pasien').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-pasien') }}",
          type: "GET",
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'nama', name:'nama', visible:true},
          { data: 'nik', name:'nik', visible:true},
          { data: 'no_telepon', name:'no_telepon', visible:true},
          { data: 'no_bpjs', name:'no_bpjs', visible:true},
      ],
    });
  });

</script>
@endpush
