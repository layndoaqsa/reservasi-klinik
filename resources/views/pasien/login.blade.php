<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PUSKESMAS KALIBAWANG</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/core.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/colors.min.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('js/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/libraries/bootstrap.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('js/app.min.js')}}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<form method="POST" action="{{ route('login') }}">
					@csrf
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
								<h5 class="content-group">Masuk ke akun Anda untuk reservasi<small class="display-block">Masukkan data-data dibawah ini</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
                            <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
								@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>

							<div class="form-group has-feedback has-feedback-left">
                            <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
								@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>

							<div class="form-group">
								<a href="{{ route('password.request') }}" class="pull-right" value="Login">Lupa Password?</a>
								<button type="submit" class="btn bg-primary btn-block">Masuk <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<div class="text-center">
								<small>Belum punya akun?</small>
							</div>
							<div class="form-group">
								<a href="{{route('register')}}" class="btn bg-teal-400 btn-block">Register<i class="icon-user-plus position-right"></i></a>
							</div>

							<div class="text-center">
								<small>Tidak mau ribet? Langsung buat reservasi disini.</small>
							</div>
							<div class="form-group">
								<a href="{{route('reservasi.noLogin')}}" class="btn bg-violet-400 btn-block">Buat Reservasi<i class="icon-book2 position-right"></i></a>
							</div>
							<div class="form-group">
								<a href="{{route('infoAntrian')}}" class="btn bg-indigo-400 btn-block">Antrian Hari Ini<i class="icon-mirror position-right"></i></a>
							</div>
						</div>
					</form>
					<!-- /simple login form -->

					<!-- Footer -->
					<div class="footer text-muted text-center">
						Copyright &copy; {{\Carbon\Carbon::now()->format('Y')}}. <b>Sistem Reservasi </b> by <a href="http://technow.id">Technow</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
