@extends('pasien.layouts.app')
@section('beranda','active')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{route('pasien.list')}}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">User</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="col-md-12">
        <form action="{{route('reservasi.create')}}" method="get" enctype="multipart/form-data" files=true>
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12" style="margin-top:5px;padding:0px 2px 0px 2px">
              <a style="width:100%" href="{{route('calon-pasien.create')}}" class="btn btn-primary btn-sm bg-primary-800"><i class="icon-add position-left"></i>Tambah Anggota</a>
          </div>
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12" style="margin-top:5px;padding:0px 2px 0px 2px">
            @if ($pasien->count() > 1)
              <a style="width:100%" class="btn btn-orange btn-sm bg-orange-800" id="btn-nama"><i class="icon-book2 position-left"></i>Buat Reservasi</a>
            @else
            <input type="hidden" name="pasien" value="{{$pasien[0]->id}}">
            <button style="width:100%" type="submit" class="btn btn-orange btn-sm bg-orange-800"><i class="icon-book2 position-left"></i>Buat Reservasi</button>
            @endif
          </div>
          <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12" style="margin-top:5px;padding:0px 2px 0px 2px">
            <a style="width:100%" href="{{route('screening')}}" class="btn btn-slate btn-sm bg-slate-800"><i class="icon-eye4 position-left"></i>Screening Covid19</a>
          </div>

        </form>
      </div>

      <table id="table-calon-pasien" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
  					<th width="70%">Nama</th>
            <!-- <th width="15%">Antrian</th> -->
  					<th width="30%">Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->

<!-- Vertical form modal -->
<div id="modal_name" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Pilih Nama</h5>
			</div>
        <form class="form-horizontal form-validate-jquery" action="{{route('reservasi.create')}}" method="get" enctype="multipart/form-data" files=true>
  				<div class="modal-body">
  					<div class="form-group">
  						<div class="row">
  							<div class="col-md-12">
                  <select name="pasien" class="form-control select-search" id="pasien">
                      @foreach ($pasien as $key => $value)
                          <option value="{{$value->id}}">{{$value->nama}}</option>
                      @endforeach
                  </select>
  							</div>
  						</div>
  					</div>
            <button type="submit" class="btn btn-primary pull-right">Next</button>
  				</div>
        </form>
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
				</div>
		</div>
	</div>
</div>
<!-- /vertical form modal -->
@endsection

@push('after_style')
  <style>


  </style>
@endpush

@push('after_script')
@push('after_script')
<script>
var tableCalonPasien;
  $(document).ready(function(){
    $('#btn-nama').click(function(){
      $('#modal_name').modal('show');
    });

		/* tabel calon-pasien */
    tableCalonPasien = $('#table-calon-pasien').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-calon-pasien') }}",
          type: "GET",
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'nama', name:'nama', visible:true},
          // { data: 'antrian', name:'antrian', visible:true},
          { data: 'action', name:'action', visible:true},
      ],
    });

    $('#table-calon-pasien tbody').on( 'click', 'button', function () {
        var data = tableCalonPasien.row( $(this).parents('tr') ).data();
          swal({
          text: "Apakah Anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('delete/data-calon-pasien') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                tableCalonPasien.ajax.reload();
                swal("Data yang dipilih berhasil dihapus!", {
                  icon: "success",
                });
              }
            });
          } else {
            swal("Data Anda aman!");
          }
        });
      });
  });

</script>
@endpush
@endpush
