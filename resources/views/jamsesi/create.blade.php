@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Jam dan Sesi</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="">Jam dan Sesi</a></li>
            <li class="active">Tambah</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div class="panel panel-flat">
        <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="{{route('jamsesi.store')}}" method="post" enctype="multipart/form-data" files=true>
                {{ csrf_field() }}
                <fieldset class="content-group">
                <legend class="text-bold">Tambah Jam dan Sesi</legend>
                <div class="form-group">
                    <label class="control-label col-lg-3">Jam <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="jam" id="" class="form-control select-search">
                            @foreach ($jam as $value)
                            <option value="{{$value->id}}" {{$value->id == old('jam') ? 'selected' : ''}}>{{$value->jam}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('jam'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('jam') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-3">Sesi <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="sesi[]" id="" multiple="multiple" class="form-control select select-search">
                            @foreach ($sesi as $value)
                            <option value="{{$value->id}}" {{$value->id == old('sesi') ? 'selected' : ''}}>{{$value->sesi}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('sesi'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('sesi') }}</strong>
                        </label>
                        @endif
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="control-label col-lg-3">Sesi <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="number" name="sesi" class="form-control" value="{{ old('sesi') }}" placeholder="">
                        @if ($errors->has('sesi'))
                        <label style="padding-top:7px;color:#F44336;">
                        <strong><i class="fa fa-times-circle"></i> {{ $errors->first('sesi') }}</strong>
                        </label>
                        @endif
                    </div>
                </div> -->
                </fieldset>
            <div>

            <div class="col-md-4">
                <a href="{{route('jamsesi.index')}}"type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
            </div>
                <div class="col-md-8 text-right">
                    <button type="submit" class="btn btn-primary bg-primary-800">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /content area -->
@endsection
