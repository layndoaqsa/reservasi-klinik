<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PUSKESMAS KALIBAWANG</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
		type="text/css">
	<link href="{{asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/core.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/colors.min.css')}}" rel="stylesheet" type="text/css">
	<style>
		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
		}

		/* Firefox */
		input[type=number] {
		-moz-appearance: textfield;
		}
	</style>
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('js/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/libraries/bootstrap.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('js/app.min.js')}}"></script>
	<!-- /theme JS files -->

	<script type="text/javascript">
		$(document).ready(function () {
			$("input[name='bpjs_atau_kartu_sehat']").change(function () {
				val = $("input[name='bpjs_atau_kartu_sehat']:checked").val();
				if (val == 'ada') {
					$("#bpjs").show();
					$("#nomor_bpjs_atau_kartu_sehat").attr('required', '');
				} else {
					$("#bpjs").hide();
					$("#nomor_bpjs_atau_kartu_sehat").removeAttr('required');;
				}
			});
		});
	</script>


</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<form method="POST" action="{{ route('register') }}">
						@csrf
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
								<h5 class="content-group">Buat akun Anda <small class="display-block">Masukkan data-data
										dibawah ini</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input id="nik" placeholder="NIK" type="number"
									class="form-control @error('nik') is-invalid @enderror" name="nik"
									value="{{ old('nik') }}" required autocomplete="nik" autofocus>
								<div class="form-control-feedback">
									<i class="icon-profile text-muted"></i>
								</div>
								@error('nik')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input id="nama" placeholder="Nama" type="nama"
									class="form-control @error('nama') is-invalid @enderror" name="nama"
									value="{{ old('nama') }}" required autocomplete="nama" autofocus>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
								@error('nama')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input id="nomor_telepon" placeholder="Nomor Telepon" type="number"
									class="form-control @error('nomor_telepon') is-invalid @enderror" name="nomor_telepon"
									value="{{ old('nomor_telepon') }}" required autocomplete="nomor_telepon" autofocus>
								<div class="form-control-feedback">
									<i class="icon-phone2 text-muted"></i>
								</div>
								@error('nomor_telepon')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input id="email" placeholder="Email" type="email"
									class="form-control @error('email') is-invalid @enderror" name="email"
									value="{{ old('email') }}" required autocomplete="email" autofocus>
								<div class="form-control-feedback">
									<i class="icon-mention text-muted"></i>
								</div>
								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input id="password" placeholder="Password" type="password"
									class="form-control @error('password') is-invalid @enderror" name="password"
									required autocomplete="current-password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input id="password-confirm" placeholder="Konfirmasi Password" type="password"
									class="form-control" name="password_confirmation" required
									autocomplete="new-password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<div class="form-group has-feedback has-feedback-left">
									<label class="control-label col-md-12">Ada BPJS / Kartu Sehat? <span
											class="text-danger">*</span></label>
									<div class="" style="padding-left:15px">
										<label class="radio-inline">
											<input type="radio" name="bpjs_atau_kartu_sehat" class="styled" value="ada"
												required id="ada">
											Ada
										</label>
										<label class="radio-inline">
											<input type="radio" name="bpjs_atau_kartu_sehat" class="styled"
												value="tidak" required id="tidak_ada">
											Tidak
										</label>
									</div>
									@error('bpjs_atau_kartu_sehat')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left" id="bpjs" style="display:none">
								<input id="nomor_bpjs_atau_kartu_sehat" placeholder="Nomor BPJS / Kartu Sehat"
									type="nomor_bpjs_atau_kartu_sehat"
									class="form-control @error('nomor_bpjs_atau_kartu_sehat') is-invalid @enderror"
									name="nomor_bpjs_atau_kartu_sehat" value="{{ old('nomor_bpjs_atau_kartu_sehat') }}"
									autocomplete="nomor_bpjs_atau_kartu_sehat" autofocus>
								<div class="form-control-feedback">
									<i class="icon-diff-added text-muted"></i>
								</div>
								@error('nomor_bpjs_atau_kartu_sehat')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>


							<div class="form-group">
								<button type="submit" class="btn bg-primary-800 btn-block">Buat Akun <i
										class="icon-circle-right2 position-right"></i></button>
							</div>

							<div class="text-center">
								Sudah punya akun? <a href="{{url('/')}}">Login</a>
							</div>

							<!-- <div class="text-center">
								<a href="{{route('password.request')}}">Forgot password?</a>
							</div> -->
						</div>
					</form>
					<!-- /simple login form -->

					<!-- Footer -->
					<div class="footer text-muted text-center">
						Copyright &copy; {{\Carbon\Carbon::now()->format('Y')}}. <b>Sistem Reservasi </b> by <a href="http://technow.id">Technow</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>

</html>