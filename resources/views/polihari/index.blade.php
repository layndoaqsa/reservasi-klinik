@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Jadwal Poli {{$poli->poli}}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{route('poli.index')}}"> Poli</a></li>
            <li class="active">Jadwal Poli {{$poli->poli}}</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <table id="table-poli-hari" class="table">
  			<thead>
  				<tr>
            <th>Id</th>
            <th width="10px">No</th>
            <th>Hari</th>
  					<th class="col-md-2">Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
      <div>
          <a href="{{route('poli.index')}}" type="button" class="btn btn-default" id=""> <i class="icon-arrow-left13"></i> Kembali</a>
      </div>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
var tablePoli;
  $(document).ready(function(){
		/* tabel user */
    tablePoli = $('#table-poli-hari').DataTable({
      processing	: true,
			serverSide	: true,
			stateSave: true,
      language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Cari data",
                  paginate: {
                    previous: "Sebelumnya",
                    next: "Berikutnya"
                  }
                },
      ajax		: {
          url: "{{ url('table/data-poli-hari') }}",
          type: "GET",
          data: function (d) {
                d.poli_id = {{$poli->id}}
              }
      },

      columns: [
          { data: 'id', name:'id', visible:false},
          { data: 'DT_RowIndex', name:'DT_RowIndex', visible:true},
          { data: 'hari.hari', name:'hari.hari', visible:true},
          { data: 'sesi', name:'sesi', visible:true},
      ],
    });

    $('#table-poli-hari tbody').on( 'click', 'button', function () {
        var data = tablePoli.row( $(this).parents('tr') ).data();
          swal({
          text: "Apakah Anda yakin ingin menghapus data ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "{{ url('delete/data-poli-hari') }}"+"/"+data['id'],
              method: 'get',
              success: function(result){
                tablePoli.ajax.reload();
                swal("Data yang dipilih berhasil dihapus!", {
                  icon: "success",
                });
              }
            });
          } else {
            swal("Data Anda aman!");
          }
        });
      });
  });

</script>
@endpush
