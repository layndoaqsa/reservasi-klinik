<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RESERVASI PEMERIKSAAN</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/colors.min.css')}}" rel="stylesheet" type="text/css">

	<link href="{{ asset('DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
	<link href="{{ asset('DataTables/Select-1.2.6/css/select.bootstrap4.min.css') }}" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" rel="stylesheet" />

	<link href="{{asset('css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	@stack('after_style')

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('js/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/libraries/bootstrap.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('js/app.min.js')}}"></script>
	<!-- /theme JS files -->

	<script src="{{ asset('DataTables/jquery.dataTables.min.js') }}" ></script>
  <script src="{{ asset('DataTables/dataTables.bootstrap4.min.js') }}" ></script>
	<!-- /theme JS files -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{asset('js/plugins/forms/validation/validate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/forms/inputs/touchspin.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/forms/styling/switch.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/libraries/jquery_ui/interactions.min.js')}}"></script>

	<script type="text/javascript" src="{{asset('js/pages/form_validation.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/pages/form_select2.js')}}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<!-- Input upload picture -->
	<script type="text/javascript" src="{{asset('js/plugins/uploaders/fileinput.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/pages/uploader_bootstrap.js')}}"></script>
  <script src="https://js.pusher.com/5.1/pusher.min.js"></script>
	<script>

	  // Enable pusher logging - don't include this in production
	  Pusher.logToConsole = true;

	  var pusher = new Pusher('604e964eea1b99d82ad2', {
	    cluster: 'ap1',
	    forceTLS: true
	  });

	  var channel = pusher.subscribe('display-antrian');
	  channel.bind('display-antrian-event', function(data) {
			if (data.message.original.first == null) {
				$('#antrian-display-1').text('');
				$('#poli-display-1').text('');
				$('#dokter-display-1').text('');
				$('#pasien-display-1').text('');
				$('#antrian-display-2').text('');
				$('#poli-display-2').text('');
				$('#dokter-display-2').text('');
				$('#pasien-display-2').text('');
			} else if (data.message.original.second == null) {
				$('#antrian-display-1').text(data.message.original.first.antrian);
				$('#poli-display-1').text(data.message.original.first.poli.poli);
				$('#dokter-display-1').text(data.message.original.first.updated_by.name);
				$('#pasien-display-1').text(data.message.original.first.calon_pasien.nama);
				$('#antrian-display-2').text('');
				$('#poli-display-2').text('');
				$('#dokter-display-2').text('');
				$('#pasien-display-2').text('');
			} else {
				$('#antrian-display-1').text(data.message.original.first.antrian);
				$('#poli-display-1').text(data.message.original.first.poli.poli);
				$('#dokter-display-1').text(data.message.original.first.updated_by.name);
				$('#pasien-display-1').text(data.message.original.first.calon_pasien.nama);
				$('#antrian-display-2').text(data.message.original.second.antrian);
				$('#poli-display-2').text(data.message.original.second.poli.poli);
				$('#dokter-display-2').text(data.message.original.second.updated_by.name);
				$('#pasien-display-2').text(data.message.original.second.calon_pasien.nama);
			}
	  });
		var channel = pusher.subscribe('info-antrian-poli');
	  channel.bind('info-antrian-poli-event', function(data) {
			console.log(data);
			var sisa = data.message.original.sisa;
			var antrian = data.message.original.antrian;
			$('#td-antrian-' + data.message.original.id).text('Sisa ' + sisa + ' / ' + antrian + ' Antrian');
			if (data.message.original.dipanggil1 == null) {
				$('#td-dipanggil-' + data.message.original.id).text('-');
			} else if (data.message.original.dipanggil2 == null) {
				$('#td-dipanggil-' + data.message.original.id).text(data.message.original.dipanggil1.antrian);
			} else {
				$('#td-dipanggil-' + data.message.original.id).text(data.message.original.dipanggil1.antrian + ' ' + data.message.original.dipanggil2.antrian);
			}
	  });
	</script>

	<!-- /Input upload picture -->
	@stack('after_script')

</head>

<body>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-primary-800">
		<div class="navbar-header" style="padding-left:25px">
			<!-- <a href="#"><img src="{{asset('img/web_transparent_fit_ruko_logo_v2_white.png')}}" alt="" style="height:47px"></a> -->
			<h5 style="height:20px">RESERVASI PEMERIKSAAN</h5>
			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">



			<!-- Main content -->
			<div class="content-wrapper">
        <div class="col-md-12" style="padding-top:15px">
          <div class="row">
            <div class="col-lg-7">
              <!-- Antrian -->
              <div class="row">
                <div class="col-lg-12">
                  <div class="thumbnail">
                    <div class="caption text-center">
                      <div id="box-pasien-dipanggil" class="row">
                          <div class="col-md-4">
                            <h1 style="font-size:64px;margin-top:0px"><b id="antrian-display-1">{{@$display[0]['antrian']}}</b></h1>
                          </div>
                          <div class="col-md-8">
                            <div class="panel panel-flat">
                              <h6 style="padding-top:10px" class="text-semibold no-margin">POLI</h6>
                              <h1 class="text-semibold no-margin" id="poli-display-1">{{@$display[0]['poli']['poli']}}</h1>
                            </div>
                            <div class="panel panel-flat">
                              <h6 style="padding-top:10px" class="text-semibold no-margin">DOKTER</h6>
                              <h1 class="text-semibold no-margin" id="dokter-display-1">{{@$display[0]['updatedBy']['name']}}</h1>
                            </div>
                            <div class="panel panel-flat">
                              <h6 style="padding-top:10px" class="text-semibold no-margin">PASIEN</h6>
                              <h1 class="text-semibold no-margin" id="pasien-display-1">{{@$display[0]['calonPasien']['nama']}}</h1>
                            </div>
                          </div>
                      </div>
                    </div>
                    <hr style="padding:0px;margin:0px">
										<div class="caption text-center">
                      <div id="box-pasien-dipanggil" class="row">
                          <div class="col-md-4">
                            <h1 style="font-size:64px;margin-top:0px"><b id="antrian-display-2">{{@$display[1]['antrian']}}</b></h1>
                          </div>
                          <div class="col-md-8">
                            <div class="panel panel-flat">
                              <h6 style="padding-top:10px" class="text-semibold no-margin">POLI</h6>
                              <h1 class="text-semibold no-margin" id="poli-display-2">{{@$display[1]['poli']['poli']}}</h1>
                            </div>
                            <div class="panel panel-flat">
                              <h6 style="padding-top:10px" class="text-semibold no-margin">DOKTER</h6>
                              <h1 class="text-semibold no-margin" id="dokter-display-2">{{@$display[1]['updatedBy']['name']}}</h1>
                            </div>
                            <div class="panel panel-flat">
                              <h6 style="padding-top:10px" class="text-semibold no-margin">PASIEN</h6>
                              <h1 class="text-semibold no-margin" id="pasien-display-2">{{@$display[1]['calonPasien']['nama']}}</h1>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /antrian -->
            </div>
            <div class="col-lg-5">
              <!-- Daily sales -->
              <div class="panel panel-flat">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                			<thead>
                        <tr class="bg-blue">
                          <th>
                            <h1 style="margin:0px;padding:0px">Poli</h1>
                          </th>
                          <th>
                            <h1 style="margin:0px;padding:0px">Antrian</h1>
                          </th>
                          <th>
                            <h1 style="margin:0px;padding:0px">Dipanggil</h1>
                          </th>
                				</tr>
                			</thead>
                			<tbody style="font-size:24px;">
												@foreach ($poli as $key => $value)
												<tr>
													<td>{{$value->poli}}</td>
													<td id="td-antrian-{{$value->id}}">Sisa {{$value->sisa}} / {{$value->antrian}} Antrian</td>
													<td id="td-dipanggil-{{$value->id}}">
														@foreach ($value->dipanggil as $key => $value)
															{{$value->antrian}}
														@endforeach
													</td>
												</tr>
											@endforeach
                			</tbody>
                		</table>
                </div>
              </div>
              <!-- /daily sales -->
              <!-- /Antrian Selanjutnya -->
            </div>
          </div>

        </div>
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	{{-- @include('sweetalert::alert') --}}
</body>
</html>
