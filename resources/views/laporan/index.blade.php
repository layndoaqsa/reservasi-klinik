@extends('layouts.app')
@section('content')
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class=""></i> <span class="text-semibold">Laporan</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Laporan</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
  <!-- State saving -->
	<div class="panel panel-flat">
    <div style="padding:20px">
      <div class="row">
        <div class="col-md-3">
          <div class="text-center input-group form-group">
            <span class="input-group-addon"><i class="icon-calendar"></i></span>
            <input type="text" class="form-control filter" placeholder="Pilih tanggal" id="tanggal-laporan">
          </div>
        </div>
        <div class="col-md-3">
          <div class="text-center input-group form-group">
            <span class="input-group-addon"><i class="icon-users"></i></span>
            <select name="tipe" id="tipe" class="form-control filter">
              <option value="all">Filter BPJS</option>
              <option value="bpjs">BPJS</option>
              <option value="non-bpjs">Non BPJS</option>
            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="text-center input-group form-group">
            <span class="input-group-addon"><i class="fa fa-medkit"></i></span>
            <select name="poli" id="poli" class="form-control filter">
              <option value="">Semua Poli</option>
              @foreach (\App\Poli::all() as $key => $value)
              <option value="{{$value->id}}">{{$value->poli}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-12">
          <form class="form-horizontal form-validate-jquery" id="form-laporan" action="{{route('reservasi.export')}}" method="get">
            @csrf
              <input type="hidden" id="download_tanggal" name="download_tanggal" value="">
              <input type="hidden" id="download_bpjs" name="download_bpjs" value="">
              <input type="hidden" id="download_poli" name="download_poli" value="">
              <div class="col-md-12">
                <div class="text-center input-group form-group">
                  <button type="submit" class="btn bg-teal-400 btn-labeled"><b><i class="icon-box-add"></i></b> Unduh Laporan</button>
                </div>
              </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="panel bg-primary text-center">
            <div class="panel-heading">Pasien Reservasi</div>
            <div class="panel-body"><span id="jml-reservasi">{{$reservasi}} Orang</span></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel bg-primary text-center">
            <div class="panel-heading">Pasien Tertangani</div>
            <div class="panel-body"><span id="jml-tertangani">{{$tertangani}} Orang</span></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel bg-primary text-center">
            <div class="panel-heading">Pasien Batal</div>
            <div class="panel-body"><span id="jml-batal">{{$batal}} Orang</span></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="panel bg-primary text-center">
            <div class="panel-heading">Pasien Terindikasi Covid-19</div>
            <div class="panel-body"><span id="jml-terindikasi">{{$terindikasi}} Orang</span></div>
          </div>
        </div>
      </div>
    </div>
	</div>
	<!-- /state saving -->
</div>
<!-- /content area -->
@endsection

@push('after_script')
<script>
  $(document).ready(function(){
    $("input[name='download_tanggal']").val($('#tanggal-laporan').val());
    $("input[name='download_bpjs']").val($('#tipe').find(":selected").val());
    $("input[name='download_poli']").val($('#poli').find(":selected").val());

    $('#tanggal-laporan').datepicker({
      format: "dd-mm-yyyy",
      todayHighlight: true,
      autoclose: true
    }).datepicker("setDate", new Date());

    $('#tipe').select2();
    $('#poli').select2();

    $('.filter').change(function(){
      // START FILTER DOWNLOAD
      $("input[name='download_tanggal']").val($('#tanggal-laporan').val());
      $("input[name='download_bpjs']").val($('#tipe').find(":selected").val());
      $("input[name='download_poli']").val($('#poli').find(":selected").val());
      // END FILTER DOWNLOAD
      $.ajax({
        type:'GET',
        url:'laporan-cari',
        data:'tanggal='+$('#tanggal-laporan').val()+'&tipe='+$('#tipe').val()+'&poli='+$('#poli').val(),
        success:function(data) {
          $('#jml-reservasi').text(data.reservasi + ' Orang');
          $('#jml-tertangani').text(data.tertangani + ' Orang');
          $('#jml-batal').text(data.batal + ' Orang');
          $('#jml-terindikasi').text(data.terindikasi + ' Orang');
        },
        error:function(data) {
        }
      });
    });

  });
</script>
@endpush
