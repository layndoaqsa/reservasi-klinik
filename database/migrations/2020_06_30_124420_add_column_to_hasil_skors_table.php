<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToHasilSkorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hasil_skors', function (Blueprint $table) {
            $table->text('kategori_hasil')->after('id')->nullable();
            $table->text('interpretasi')->after('hasil')->nullable();
            $table->text('tatalaksana')->after('interpretasi')->nullable();
            $table->string('gambar',255)->after('batas_atas')->nullable()->default('default.png');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hasil_skors', function (Blueprint $table) {
            //
        });
    }
}
