<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GantiUnikReservasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservasi', function (Blueprint $table) {
            $table->unique(['pasien_id','tanggal']);
            $table->dropColumn('waktu');
            $table->string('antrian',50)->after('poli_id');
            $table->string('perkiraan_jam_pelayanan',50)->after('antrian')->nullable();
            $table->integer('counter')->after('perkiraan_jam_pelayanan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservasi', function (Blueprint $table) {
            //
        });
    }
}
