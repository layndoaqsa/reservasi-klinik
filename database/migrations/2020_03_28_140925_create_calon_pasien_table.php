<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalonPasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calon_pasien', function (Blueprint $table) {
            $table->id();
            $table->string('no_telepon')->nullable();
            $table->string('nama');
            $table->string('nik')->nullable();
            $table->string('no_bpjs')->nullable();
            $table->bigInteger('created_by');
            $table->enum('tipe',['root','child']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calon_pasien');
    }
}
