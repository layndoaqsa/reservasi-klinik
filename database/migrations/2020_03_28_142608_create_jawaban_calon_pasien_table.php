<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawabanCalonPasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban_calon_pasien', function (Blueprint $table) {
            $table->id();
            $table->integer('reservasi_id')->unsigned()->nullable();
            $table->string('calon_pasien_id');
            $table->string('pertanyaan_id');
            $table->string('jawaban');
            $table->integer('skor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawaban_calon_pasien');
    }
}
