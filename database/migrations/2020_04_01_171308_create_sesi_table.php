<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSesiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('sesi', function (Blueprint $table) {
        //     $table->id();
        //     $table->string('sesi');
        //     $table->timestamps();
        // });
        Schema::create('sesi', function (Blueprint $table) {
            $table->id();
            $table->string('sesi');
            $table->integer('poli_hari_id');
            $table->string('mulai');
            $table->string('selesai');
            $table->tinyInteger('jumlah_dokter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sesi');
    }
}
