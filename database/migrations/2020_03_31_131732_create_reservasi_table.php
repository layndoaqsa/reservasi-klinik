<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasi', function (Blueprint $table) {
            $table->id();
            $table->string('kode');
            $table->integer('poli_id');
            $table->integer('pasien_id');
            $table->mediumText('keluhan');
            $table->enum('status',['Reservasi','Selesai Pemeriksaan','Sedang Pemeriksaan','Menunggu Pemeriksaan','Batal','Terlewat Pemanggilan']);
            $table->mediumText('kekhawatiran')->nullable();
            $table->mediumText('riwayat_penyakit_menahun')->nullable();
            $table->mediumText('upaya_pengobatan')->nullable();
            $table->string('tanggal');
            $table->string('waktu');
            $table->timestamps();
            $table->integer('status_updated_by')->unsigned()->nullable();

            // $table->unique(['poli_id','tanggal']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi');
    }
}
