<?php

use Illuminate\Database\Seeder;

class SesiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sesi')->delete();
        
        \DB::table('sesi')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sesi' => 'Pagi',
                'poli_hari_id' => 1,
                'mulai' => '08:00',
                'selesai' => '12:00',
                'jumlah_dokter' => 2,
                'created_at' => '2020-07-04 12:07:26',
                'updated_at' => '2020-07-04 12:07:26',
            ),
            1 => 
            array (
                'id' => 2,
                'sesi' => 'Siang',
                'poli_hari_id' => 1,
                'mulai' => '13:00',
                'selesai' => '17:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:07:56',
                'updated_at' => '2020-07-04 12:07:56',
            ),
            2 => 
            array (
                'id' => 3,
                'sesi' => 'Pagi',
                'poli_hari_id' => 2,
                'mulai' => '08:00',
                'selesai' => '12:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:09:42',
                'updated_at' => '2020-07-04 12:09:42',
            ),
            3 => 
            array (
                'id' => 4,
                'sesi' => 'Pagi',
                'poli_hari_id' => 3,
                'mulai' => '08:00',
                'selesai' => '12:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:09:56',
                'updated_at' => '2020-07-04 12:09:56',
            ),
            4 => 
            array (
                'id' => 5,
                'sesi' => 'Pagi',
                'poli_hari_id' => 4,
                'mulai' => '08:00',
                'selesai' => '12:00',
                'jumlah_dokter' => 2,
                'created_at' => '2020-07-04 12:10:13',
                'updated_at' => '2020-07-04 12:10:13',
            ),
            5 => 
            array (
                'id' => 6,
                'sesi' => 'Pagi',
                'poli_hari_id' => 5,
                'mulai' => '08:00',
                'selesai' => '11:00',
                'jumlah_dokter' => 2,
                'created_at' => '2020-07-04 12:10:38',
                'updated_at' => '2020-07-04 12:10:38',
            ),
            6 => 
            array (
                'id' => 7,
                'sesi' => 'Pagi',
                'poli_hari_id' => 6,
                'mulai' => '08:00',
                'selesai' => '12:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:10:50',
                'updated_at' => '2020-07-04 12:10:50',
            ),
            7 => 
            array (
                'id' => 8,
                'sesi' => 'Siang',
                'poli_hari_id' => 2,
                'mulai' => '13:00',
                'selesai' => '17:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:11:09',
                'updated_at' => '2020-07-04 12:11:09',
            ),
            8 => 
            array (
                'id' => 9,
                'sesi' => 'Siang',
                'poli_hari_id' => 3,
                'mulai' => '13:00',
                'selesai' => '17:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:11:28',
                'updated_at' => '2020-07-04 12:11:28',
            ),
            9 => 
            array (
                'id' => 10,
                'sesi' => 'Siang',
                'poli_hari_id' => 4,
                'mulai' => '13:00',
                'selesai' => '17:00',
                'jumlah_dokter' => 2,
                'created_at' => '2020-07-04 12:12:02',
                'updated_at' => '2020-07-04 12:12:02',
            ),
            10 => 
            array (
                'id' => 11,
                'sesi' => 'Siang',
                'poli_hari_id' => 5,
                'mulai' => '13:00',
                'selesai' => '17:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:12:26',
                'updated_at' => '2020-07-04 12:12:26',
            ),
            11 => 
            array (
                'id' => 12,
                'sesi' => 'Siang',
                'poli_hari_id' => 6,
                'mulai' => '13:00',
                'selesai' => '15:00',
                'jumlah_dokter' => 1,
                'created_at' => '2020-07-04 12:12:47',
                'updated_at' => '2020-07-04 12:12:47',
            ),
        ));
        
        
    }
}