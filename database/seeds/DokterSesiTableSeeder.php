<?php

use Illuminate\Database\Seeder;

class DokterSesiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('dokter_sesi')->delete();
        
        \DB::table('dokter_sesi')->insert(array (
            0 => 
            array (
                'id' => 1,
                'dokter_id' => 3,
                'sesi_id' => 1,
                'created_at' => '2020-07-04 12:07:26',
                'updated_at' => '2020-07-04 12:07:26',
            ),
            1 => 
            array (
                'id' => 2,
                'dokter_id' => 4,
                'sesi_id' => 1,
                'created_at' => '2020-07-04 12:07:26',
                'updated_at' => '2020-07-04 12:07:26',
            ),
            2 => 
            array (
                'id' => 3,
                'dokter_id' => 3,
                'sesi_id' => 2,
                'created_at' => '2020-07-04 12:07:56',
                'updated_at' => '2020-07-04 12:07:56',
            ),
            3 => 
            array (
                'id' => 4,
                'dokter_id' => 3,
                'sesi_id' => 3,
                'created_at' => '2020-07-04 12:09:42',
                'updated_at' => '2020-07-04 12:09:42',
            ),
            4 => 
            array (
                'id' => 5,
                'dokter_id' => 4,
                'sesi_id' => 4,
                'created_at' => '2020-07-04 12:09:56',
                'updated_at' => '2020-07-04 12:09:56',
            ),
            5 => 
            array (
                'id' => 6,
                'dokter_id' => 3,
                'sesi_id' => 5,
                'created_at' => '2020-07-04 12:10:14',
                'updated_at' => '2020-07-04 12:10:14',
            ),
            6 => 
            array (
                'id' => 7,
                'dokter_id' => 4,
                'sesi_id' => 5,
                'created_at' => '2020-07-04 12:10:14',
                'updated_at' => '2020-07-04 12:10:14',
            ),
            7 => 
            array (
                'id' => 8,
                'dokter_id' => 3,
                'sesi_id' => 6,
                'created_at' => '2020-07-04 12:10:38',
                'updated_at' => '2020-07-04 12:10:38',
            ),
            8 => 
            array (
                'id' => 9,
                'dokter_id' => 4,
                'sesi_id' => 6,
                'created_at' => '2020-07-04 12:10:38',
                'updated_at' => '2020-07-04 12:10:38',
            ),
            9 => 
            array (
                'id' => 10,
                'dokter_id' => 3,
                'sesi_id' => 7,
                'created_at' => '2020-07-04 12:10:50',
                'updated_at' => '2020-07-04 12:10:50',
            ),
            10 => 
            array (
                'id' => 11,
                'dokter_id' => 3,
                'sesi_id' => 8,
                'created_at' => '2020-07-04 12:11:09',
                'updated_at' => '2020-07-04 12:11:09',
            ),
            11 => 
            array (
                'id' => 12,
                'dokter_id' => 4,
                'sesi_id' => 9,
                'created_at' => '2020-07-04 12:11:28',
                'updated_at' => '2020-07-04 12:11:28',
            ),
            12 => 
            array (
                'id' => 13,
                'dokter_id' => 3,
                'sesi_id' => 10,
                'created_at' => '2020-07-04 12:12:02',
                'updated_at' => '2020-07-04 12:12:02',
            ),
            13 => 
            array (
                'id' => 14,
                'dokter_id' => 4,
                'sesi_id' => 10,
                'created_at' => '2020-07-04 12:12:02',
                'updated_at' => '2020-07-04 12:12:02',
            ),
            14 => 
            array (
                'id' => 15,
                'dokter_id' => 3,
                'sesi_id' => 11,
                'created_at' => '2020-07-04 12:12:26',
                'updated_at' => '2020-07-04 12:12:26',
            ),
            15 => 
            array (
                'id' => 16,
                'dokter_id' => 4,
                'sesi_id' => 12,
                'created_at' => '2020-07-04 12:12:47',
                'updated_at' => '2020-07-04 12:12:47',
            ),
        ));
        
        
    }
}