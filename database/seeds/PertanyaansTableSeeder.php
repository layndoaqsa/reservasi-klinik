<?php

use Illuminate\Database\Seeder;

class PertanyaansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pertanyaans')->delete();
        
        \DB::table('pertanyaans')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kategori' => '',
            'pertanyaan' => 'Dalam 14 hari terakhir, apakah ada demam (≥ 38oC) atau riwayat demam (≥ 38oC)?',
                'skor_ya' => 2,
                'skor_tidak' => 1,
                'created_at' => '2020-02-06 07:56:53',
                'updated_at' => '2020-02-06 07:56:53',
            ),
            1 => 
            array (
                'id' => 2,
                'kategori' => '',
            'pertanyaan' => 'Dalam 14 hari terakhir, apakah mengalami gejala & tanda gangguan pernapasan (batuk, pilek, sakit tenggorokan, sesak napas)?',
                'skor_ya' => 2,
                'skor_tidak' => 1,
                'created_at' => '2020-02-06 07:56:53',
                'updated_at' => '2020-02-06 07:56:53',
            ),
            2 => 
            array (
                'id' => 3,
                'kategori' => '',
            'pertanyaan' => 'Dalam 14 hari terakhir, apakah pernah kontak erat (kontak fisik atau berada dalam satu ruangan atau dalam radius 1 meter) dengan pasien positif COVID-19?',
                'skor_ya' => 2,
                'skor_tidak' => 1,
                'created_at' => '2020-02-06 07:56:53',
                'updated_at' => '2020-02-06 07:56:53',
            ),
            3 => 
            array (
                'id' => 4,
                'kategori' => '',
            'pertanyaan' => 'Dalam 14 hari terakhir, apakah pernah berkunjung atau tinggal diluar negeri yang melakukan penularan lokal (zona merah)?',
                'skor_ya' => 2,
                'skor_tidak' => 1,
                'created_at' => '2020-02-06 07:56:53',
                'updated_at' => '2020-02-06 07:56:53',
            ),
            4 => 
            array (
                'id' => 5,
                'kategori' => '',
            'pertanyaan' => 'Dalam 14 hari terakhir, apakah pernah berkunjung atau tinggal diarea penularan lokal di Indonesia (zona merah)?',
                'skor_ya' => 2,
                'skor_tidak' => 1,
                'created_at' => '2020-02-06 07:56:53',
                'updated_at' => '2020-02-06 07:56:53',
            ),
        ));
        
        
    }
}