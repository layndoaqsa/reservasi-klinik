<?php

use Illuminate\Database\Seeder;

class PoliTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('poli')->delete();
        
        \DB::table('poli')->insert(array (
            0 => 
            array (
                'id' => 1,
                'poli' => 'Umum',
                'kode' => 'U',
                'waktu' => 12,
                'icon' => '2007037278.png',
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-07-03 14:18:51',
            ),
            1 => 
            array (
                'id' => 2,
                'poli' => 'Gigi',
                'kode' => 'G',
                'waktu' => 12,
                'icon' => '2007036619.png',
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-07-03 14:20:51',
            ),
            2 => 
            array (
                'id' => 3,
                'poli' => 'KIA',
                'kode' => 'KIA',
                'waktu' => 12,
                'icon' => '2007036248.png',
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-07-03 14:21:05',
            ),
            3 => 
            array (
                'id' => 5,
                'poli' => 'UGD',
                'kode' => 'UGD',
                'waktu' => 12,
                'icon' => '2007038188.png',
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-07-03 14:21:28',
            ),
            4 => 
            array (
                'id' => 6,
                'poli' => 'Fisio',
                'kode' => 'F',
                'waktu' => 12,
                'icon' => '2007035144.png',
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-07-03 14:21:38',
            ),
            5 => 
            array (
                'id' => 7,
                'poli' => 'KB',
                'kode' => 'KB',
                'waktu' => 12,
                'icon' => '2007034128.png',
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-07-03 14:21:46',
            ),
            6 => 
            array (
                'id' => 9,
                'poli' => 'Mata',
                'kode' => 'M',
                'waktu' => 12,
                'icon' => '2007032722.png',
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-07-03 14:21:54',
            ),
            7 => 
            array (
                'id' => 10,
                'poli' => 'Kandungan',
                'kode' => 'K',
                'waktu' => 12,
                'icon' => '2007038246.png',
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-07-03 14:22:06',
            ),
        ));
        
        
    }
}