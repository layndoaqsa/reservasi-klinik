<?php

use Illuminate\Database\Seeder;

class PoliHariTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('poli_hari')->delete();
        
        \DB::table('poli_hari')->insert(array (
            0 => 
            array (
                'id' => 1,
                'poli_id' => 1,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            1 => 
            array (
                'id' => 2,
                'poli_id' => 1,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            2 => 
            array (
                'id' => 3,
                'poli_id' => 1,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            3 => 
            array (
                'id' => 4,
                'poli_id' => 1,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            4 => 
            array (
                'id' => 5,
                'poli_id' => 1,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            5 => 
            array (
                'id' => 6,
                'poli_id' => 1,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            6 => 
            array (
                'id' => 7,
                'poli_id' => 1,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:26:37',
                'updated_at' => '2020-04-08 21:26:37',
            ),
            7 => 
            array (
                'id' => 8,
                'poli_id' => 2,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            8 => 
            array (
                'id' => 9,
                'poli_id' => 2,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            9 => 
            array (
                'id' => 10,
                'poli_id' => 2,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            10 => 
            array (
                'id' => 11,
                'poli_id' => 2,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            11 => 
            array (
                'id' => 12,
                'poli_id' => 2,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            12 => 
            array (
                'id' => 13,
                'poli_id' => 2,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            13 => 
            array (
                'id' => 14,
                'poli_id' => 2,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:26:49',
                'updated_at' => '2020-04-08 21:26:49',
            ),
            14 => 
            array (
                'id' => 15,
                'poli_id' => 3,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            15 => 
            array (
                'id' => 16,
                'poli_id' => 3,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            16 => 
            array (
                'id' => 17,
                'poli_id' => 3,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            17 => 
            array (
                'id' => 18,
                'poli_id' => 3,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            18 => 
            array (
                'id' => 19,
                'poli_id' => 3,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            19 => 
            array (
                'id' => 20,
                'poli_id' => 3,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            20 => 
            array (
                'id' => 21,
                'poli_id' => 3,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:27:01',
                'updated_at' => '2020-04-08 21:27:01',
            ),
            21 => 
            array (
                'id' => 29,
                'poli_id' => 5,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            22 => 
            array (
                'id' => 30,
                'poli_id' => 5,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            23 => 
            array (
                'id' => 31,
                'poli_id' => 5,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            24 => 
            array (
                'id' => 32,
                'poli_id' => 5,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            25 => 
            array (
                'id' => 33,
                'poli_id' => 5,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            26 => 
            array (
                'id' => 34,
                'poli_id' => 5,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            27 => 
            array (
                'id' => 35,
                'poli_id' => 5,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:27:31',
                'updated_at' => '2020-04-08 21:27:31',
            ),
            28 => 
            array (
                'id' => 36,
                'poli_id' => 6,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            29 => 
            array (
                'id' => 37,
                'poli_id' => 6,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            30 => 
            array (
                'id' => 38,
                'poli_id' => 6,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            31 => 
            array (
                'id' => 39,
                'poli_id' => 6,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            32 => 
            array (
                'id' => 40,
                'poli_id' => 6,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            33 => 
            array (
                'id' => 41,
                'poli_id' => 6,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            34 => 
            array (
                'id' => 42,
                'poli_id' => 6,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:27:46',
                'updated_at' => '2020-04-08 21:27:46',
            ),
            35 => 
            array (
                'id' => 43,
                'poli_id' => 7,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            36 => 
            array (
                'id' => 44,
                'poli_id' => 7,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            37 => 
            array (
                'id' => 45,
                'poli_id' => 7,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            38 => 
            array (
                'id' => 46,
                'poli_id' => 7,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            39 => 
            array (
                'id' => 47,
                'poli_id' => 7,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            40 => 
            array (
                'id' => 48,
                'poli_id' => 7,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            41 => 
            array (
                'id' => 49,
                'poli_id' => 7,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:28:05',
                'updated_at' => '2020-04-08 21:28:05',
            ),
            42 => 
            array (
                'id' => 57,
                'poli_id' => 9,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            43 => 
            array (
                'id' => 58,
                'poli_id' => 9,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            44 => 
            array (
                'id' => 59,
                'poli_id' => 9,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            45 => 
            array (
                'id' => 60,
                'poli_id' => 9,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            46 => 
            array (
                'id' => 61,
                'poli_id' => 9,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            47 => 
            array (
                'id' => 62,
                'poli_id' => 9,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            48 => 
            array (
                'id' => 63,
                'poli_id' => 9,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:28:28',
                'updated_at' => '2020-04-08 21:28:28',
            ),
            49 => 
            array (
                'id' => 64,
                'poli_id' => 10,
                'hari_id' => 1,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
            50 => 
            array (
                'id' => 65,
                'poli_id' => 10,
                'hari_id' => 2,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
            51 => 
            array (
                'id' => 66,
                'poli_id' => 10,
                'hari_id' => 3,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
            52 => 
            array (
                'id' => 67,
                'poli_id' => 10,
                'hari_id' => 4,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
            53 => 
            array (
                'id' => 68,
                'poli_id' => 10,
                'hari_id' => 5,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
            54 => 
            array (
                'id' => 69,
                'poli_id' => 10,
                'hari_id' => 6,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
            55 => 
            array (
                'id' => 70,
                'poli_id' => 10,
                'hari_id' => 7,
                'created_at' => '2020-04-08 21:28:41',
                'updated_at' => '2020-04-08 21:28:41',
            ),
        ));
        
        
    }
}