<?php

use Illuminate\Database\Seeder;

class JamSesiTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('jam_sesi')->delete();

        \DB::table('jam_sesi')->insert(array (
            0 =>
            array (
                'id' => 1,
                'jam_id' => 1,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:32:42',
                'updated_at' => '2020-04-02 10:32:42',
            ),
            1 =>
            array (
                'id' => 2,
                'jam_id' => 1,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:32:42',
                'updated_at' => '2020-04-02 10:32:42',
            ),
            2 =>
            array (
                'id' => 3,
                'jam_id' => 1,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:32:42',
                'updated_at' => '2020-04-02 10:32:42',
            ),
            3 =>
            array (
                'id' => 4,
                'jam_id' => 1,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:32:42',
                'updated_at' => '2020-04-02 10:32:42',
            ),
            4 =>
            array (
                'id' => 5,
                'jam_id' => 2,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:32:55',
                'updated_at' => '2020-04-02 10:32:55',
            ),
            5 =>
            array (
                'id' => 6,
                'jam_id' => 2,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:32:55',
                'updated_at' => '2020-04-02 10:32:55',
            ),
            6 =>
            array (
                'id' => 7,
                'jam_id' => 2,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:32:55',
                'updated_at' => '2020-04-02 10:32:55',
            ),
            7 =>
            array (
                'id' => 8,
                'jam_id' => 2,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:32:55',
                'updated_at' => '2020-04-02 10:32:55',
            ),
            8 =>
            array (
                'id' => 9,
                'jam_id' => 3,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:33:13',
                'updated_at' => '2020-04-02 10:33:13',
            ),
            9 =>
            array (
                'id' => 10,
                'jam_id' => 3,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:33:13',
                'updated_at' => '2020-04-02 10:33:13',
            ),
            10 =>
            array (
                'id' => 11,
                'jam_id' => 3,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:33:13',
                'updated_at' => '2020-04-02 10:33:13',
            ),
            11 =>
            array (
                'id' => 12,
                'jam_id' => 3,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:33:13',
                'updated_at' => '2020-04-02 10:33:13',
            ),
            12 =>
            array (
                'id' => 13,
                'jam_id' => 4,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:34:03',
                'updated_at' => '2020-04-02 10:34:03',
            ),
            13 =>
            array (
                'id' => 14,
                'jam_id' => 4,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:34:03',
                'updated_at' => '2020-04-02 10:34:03',
            ),
            14 =>
            array (
                'id' => 15,
                'jam_id' => 4,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:34:03',
                'updated_at' => '2020-04-02 10:34:03',
            ),
            15 =>
            array (
                'id' => 16,
                'jam_id' => 4,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:34:03',
                'updated_at' => '2020-04-02 10:34:03',
            ),
            16 =>
            array (
                'id' => 17,
                'jam_id' => 6,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:34:22',
                'updated_at' => '2020-04-02 10:34:22',
            ),
            17 =>
            array (
                'id' => 18,
                'jam_id' => 6,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:34:22',
                'updated_at' => '2020-04-02 10:34:22',
            ),
            18 =>
            array (
                'id' => 19,
                'jam_id' => 6,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:34:22',
                'updated_at' => '2020-04-02 10:34:22',
            ),
            19 =>
            array (
                'id' => 20,
                'jam_id' => 6,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:34:22',
                'updated_at' => '2020-04-02 10:34:22',
            ),
            20 =>
            array (
                'id' => 21,
                'jam_id' => 7,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:34:36',
                'updated_at' => '2020-04-02 10:34:36',
            ),
            21 =>
            array (
                'id' => 22,
                'jam_id' => 7,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:34:36',
                'updated_at' => '2020-04-02 10:34:36',
            ),
            22 =>
            array (
                'id' => 23,
                'jam_id' => 7,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:34:36',
                'updated_at' => '2020-04-02 10:34:36',
            ),
            23 =>
            array (
                'id' => 24,
                'jam_id' => 7,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:34:36',
                'updated_at' => '2020-04-02 10:34:36',
            ),
            24 =>
            array (
                'id' => 25,
                'jam_id' => 8,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:34:51',
                'updated_at' => '2020-04-02 10:34:51',
            ),
            25 =>
            array (
                'id' => 26,
                'jam_id' => 8,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:34:51',
                'updated_at' => '2020-04-02 10:34:51',
            ),
            26 =>
            array (
                'id' => 27,
                'jam_id' => 8,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:34:51',
                'updated_at' => '2020-04-02 10:34:51',
            ),
            27 =>
            array (
                'id' => 28,
                'jam_id' => 8,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:34:51',
                'updated_at' => '2020-04-02 10:34:51',
            ),
            28 =>
            array (
                'id' => 29,
                'jam_id' => 9,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:35:21',
                'updated_at' => '2020-04-02 10:35:21',
            ),
            29 =>
            array (
                'id' => 30,
                'jam_id' => 9,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:35:21',
                'updated_at' => '2020-04-02 10:35:21',
            ),
            30 =>
            array (
                'id' => 31,
                'jam_id' => 9,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:35:21',
                'updated_at' => '2020-04-02 10:35:21',
            ),
            31 =>
            array (
                'id' => 32,
                'jam_id' => 9,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:35:21',
                'updated_at' => '2020-04-02 10:35:21',
            ),
            32 =>
            array (
                'id' => 33,
                'jam_id' => 10,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:35:40',
                'updated_at' => '2020-04-02 10:35:40',
            ),
            33 =>
            array (
                'id' => 34,
                'jam_id' => 10,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:35:40',
                'updated_at' => '2020-04-02 10:35:40',
            ),
            34 =>
            array (
                'id' => 35,
                'jam_id' => 11,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:36:03',
                'updated_at' => '2020-04-02 10:36:03',
            ),
            35 =>
            array (
                'id' => 36,
                'jam_id' => 11,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:36:03',
                'updated_at' => '2020-04-02 10:36:03',
            ),
            36 =>
            array (
                'id' => 37,
                'jam_id' => 12,
                'sesi_id' => 1,
                'created_at' => '2020-04-02 10:36:19',
                'updated_at' => '2020-04-02 10:36:19',
            ),
            37 =>
            array (
                'id' => 38,
                'jam_id' => 12,
                'sesi_id' => 2,
                'created_at' => '2020-04-02 10:36:19',
                'updated_at' => '2020-04-02 10:36:19',
            ),
            38 =>
            array (
                'id' => 39,
                'jam_id' => 12,
                'sesi_id' => 3,
                'created_at' => '2020-04-02 10:36:19',
                'updated_at' => '2020-04-02 10:36:19',
            ),
            39 =>
            array (
                'id' => 40,
                'jam_id' => 12,
                'sesi_id' => 4,
                'created_at' => '2020-04-02 10:36:19',
                'updated_at' => '2020-04-02 10:36:19',
            ),
        ));


    }
}
