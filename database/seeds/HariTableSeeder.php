<?php

use Illuminate\Database\Seeder;

class HariTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hari')->delete();
        
        \DB::table('hari')->insert(array (
            0 => 
            array (
                'id' => 1,
                'hari' => 'Senin',
                'day' => 'Monday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'hari' => 'Selasa',
                'day' => 'Tuesday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'hari' => 'Rabu',
                'day' => 'Wednesday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'hari' => 'Kamis',
                'day' => 'Thursday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'hari' => 'Jumat',
                'day' => 'Friday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'hari' => 'Sabtu',
                'day' => 'Saturday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'hari' => 'Minggu',
                'day' => 'Sunday',
                'created_at' => '2020-04-01 00:00:00',
                'updated_at' => '2020-04-01 00:00:00',
            ),
        ));
        
        
    }
}