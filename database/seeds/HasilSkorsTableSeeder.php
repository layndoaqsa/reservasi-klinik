<?php

use Illuminate\Database\Seeder;

class HasilSkorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hasil_skors')->delete();
        
        \DB::table('hasil_skors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kategori_hasil' => 'otg',
            'hasil' => 'Orang Tanpa Gejala (OTG)',
                'interpretasi' => 'Seseorang yang memiliki riwayat kontak dengan kasus konfirmasi COVID-19 namun tidak menunjukan gejala dan memiliki risiko tertular dari orang konfirmasi COVID-19.',
            'tatalaksana' => 'Melakukan karantina diri di rumah selama 14 hari (Rajin cuci tangan, menggunakan masker, menerapkan etika bersin dan batuk yang baik, menjaga jarak minimal 1 meter).',
                'batas_bawah' => 0,
                'batas_atas' => 100,
                'gambar' => '2.png',
                'created_at' => NULL,
                'updated_at' => '2020-05-26 13:06:37',
            ),
            1 => 
            array (
                'id' => 2,
                'kategori_hasil' => 'odp',
            'hasil' => 'Orang Dalam Pemantauan (ODP)',
            'interpretasi' => 'a.	Orang yang mengalami demam (≥380C) atau riwayat demam; atau gejala gangguan sistem pernapasan seperti pilek/sakit tenggorokan/batuk DAN pada 14 hari terakhir sebelum timbul gejala memiliki riwayat perjalanan atau tinggal di negara/wilayah yang melaporkan transmisi lokal
b.	Orang yang mengalami gejala gangguan sistem pernapasan seperti pilek/sakit tenggorokan/batuk DAN pada 14 hari terakhir sebelum timbul gejala memiliki riwayat kontak dengan kasus konfirmasi atau probabel COVID-19
',
            'tatalaksana' => 'Melaukan isolasi diri di rumah selama 14 hari (Rajin cuci tangan, menggunakan masker, menerapkan etika bersin dan batuk yang baik, menjaga jarak minimal 1 meter).',
                'batas_bawah' => NULL,
                'batas_atas' => NULL,
                'gambar' => '3.png',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'kategori_hasil' => 'pdp',
            'hasil' => 'Pasien Dalam Pemantauan (PDP)',
            'interpretasi' => 'a.	Orang dengan Infeksi Saluran Pernapasan Akut (ISPA) yaitu demam (≥38oC) atau riwayat demam; disertai salah satu gejala/tanda penyakit pernapasan seperti: batuk/sesak nafas/sakit tenggorokan/pilek/pneumonia ringan hingga berat DAN pada 14 hari terakhir sebelum timbul gejala memiliki riwayat perjalanan atau tinggal di negara/wilayah yang melaporkan transmisi lokal
b.	Orang dengan demam (≥38oC) atau riwayat demam atau ISPA DAN pada 14 hari terakhir sebelum timbul gejala memiliki riwayat kontak dengan kasus konfirmasi atau probabel COVID-19
c.	Orang dengan ISPA (Infeksi Saluran Pernapasan Akut) berat/pneumonia berat yang membutuhkan perawatan di rumah sakit DAN tidak ada penyebab lain berdasarkan gambaran klinis yang meyakinkan.',
                'tatalaksana' => 'Jika gejala ringan, lakukan Isolasi diri di rumah selama 14 hari, rajin cuci tangan, gunakan masker, terapkan etika bersin dan batuk yang baik, jaga jarak minimal 1 meter.
Jika gejala sedang dan ringan, Hubungi Fasilitas Kesehatan.',
                'batas_bawah' => NULL,
                'batas_atas' => NULL,
                'gambar' => '4.png',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'kategori_hasil' => 'sehat',
                'hasil' => 'Sehat',
                'interpretasi' => 'Sehat dari covid-19 namun HARUS WASPADA',
                'tatalaksana' => 'Tetap jaga kesehatan dengan menerapkan pola hidup sehat, rajin cuci tangan, menggunakan masker jika keluar rumah dan menjaga jarak minimal 1meter dengan orang lain.',
                'batas_bawah' => NULL,
                'batas_atas' => NULL,
                'gambar' => '1.png',
                'created_at' => NULL,
                'updated_at' => '2020-05-26 13:08:31',
            ),
            4 => 
            array (
                'id' => 5,
                'kategori_hasil' => 'waspada',
                'hasil' => 'Waspada',
                'interpretasi' => 'Sehat dari covid-19 namun HARUS WASPADA',
            'tatalaksana' => 'Melakukan isolasi diri di rumah selama 14 hari (Rajin cuci tangan, menggunakan masker, menerapkan etika bersin dan batuk yang baik, menjaga jarak minimal 1 meter).
Kunjungi FASYANKES untuk berobat jika dirasa perlu atau sakit yang dirasakan bertambah berat.',
                'batas_bawah' => NULL,
                'batas_atas' => NULL,
                'gambar' => '2.png',
                'created_at' => NULL,
                'updated_at' => '2020-05-26 13:09:07',
            ),
            5 => 
            array (
                'id' => 6,
                'kategori_hasil' => 'wajib_lapor',
                'hasil' => 'Wajib Lapor',
                'interpretasi' => 'Wajib melaporkan ke rt/rw atau pihak lain terkait riwayat perjalanan dan tinggal di zona merah',
                'tatalaksana' => 'Wajib melaporkan ke RT/RW atau pihak lain terkait riwayat perjalanan dan tinggal di zona merah.
Melakukan isolasi diri di rumah selama 14 hari (Rajin cuci tangan, menggunakan masker, menerapkan etika bersin dan batuk yang baik, menjaga jarak minimal 1 meter).',
                'batas_bawah' => NULL,
                'batas_atas' => NULL,
                'gambar' => '2.png',
                'created_at' => NULL,
                'updated_at' => '2020-05-26 13:09:47',
            ),
        ));
        
        
    }
}