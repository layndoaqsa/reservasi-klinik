<?php

use Illuminate\Database\Seeder;
use App\Jam;
use App\Sesi;

class JamWaktuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jam = [
            ['jam'=>'08:00'],
            ['jam'=>'09:00'],
            ['jam'=>'10:00'],
            ['jam'=>'11:00'],
            ['jam'=>'12:00'],
            ['jam'=>'13:00'],
            ['jam'=>'14:00'],
            ['jam'=>'15:00'],
            ['jam'=>'16:00'],
            ['jam'=>'17:00'],
            ['jam'=>'18:00'],
            ['jam'=>'19:00']
        ];
        $sesi = [
            ['sesi'=>'Sesi 1'],
            ['sesi'=>'Sesi 2'],
            ['sesi'=>'Sesi 3'],
            ['sesi'=>'Sesi 4'],
            ['sesi'=>'Sesi 5'],
        ];
        Jam::insert($jam);
        Sesi::insert($sesi);
    }
}
