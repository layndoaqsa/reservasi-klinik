<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Role::create(['name' => 'Super Admin']);
        Role::create(['name' => 'Calon Pasien']);
        Role::create(['name' => 'Dokter']);
        Role::create(['name' => 'Layanan Reservasi']);

        $admin = User::create([
            'name'      => 'Developer',
            'email'     => 'developer@dev.com',
            'password'  =>  bcrypt('devpass'),
        ]);
        $admin->assignRole('Super Admin');
        $layanan = User::create([
            'name'      => 'Layanan Developer',
            'email'     => 'layanan@dev.com',
            'password'  =>  bcrypt('devpass'),
        ]);
        $layanan->assignRole('Layanan Reservasi');
        $dokterUmum = User::create([
            'name'      => 'Dokter Umum',
            'email'     => 'umum@dev.com',
            'password'  =>  bcrypt('devpass'),
            'poli_id'   =>  1,
        ]);
        $dokterUmum->assignRole('Dokter');
        $dokterUmum2 = User::create([
            'name'      => 'Dokter Umum 2',
            'email'     => 'umum2@dev.com',
            'password'  =>  bcrypt('devpass'),
            'poli_id'   =>  1,
        ]);
        $dokterUmum2->assignRole('Dokter');
        // $this->call(JamWaktuSeeder::class);
        // $this->call(JamSesiTableSeeder::class);
        $this->call(PertanyaansTableSeeder::class);
        $this->call(HasilSkorsTableSeeder::class);
        $this->call(HariTableSeeder::class);
        $this->call(PoliTableSeeder::class);
        $this->call(PoliHariTableSeeder::class);
        $this->call(DokterSesiTableSeeder::class);
        $this->call(SesiTableSeeder::class);
    }
}
