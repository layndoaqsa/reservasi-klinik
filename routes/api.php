<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['api'],'prefix' => '/'], function () {
  Route::post('/register', 'UserController@api_register');
  Route::post('/login','UserController@api_login');

  // START RESERVASI NO LOGIN
  Route::get('/poli', 'PoliController@api_index');
  Route::get('/reservasi/{poli_id}', 'ReservasiController@api_tanggal');
  Route::post('/reservasi/no-login', 'ReservasiController@api_reservasiNoLoginStore');
  Route::get('/screening', 'ScreeningController@api_index');
  Route::post('/screening/tambah/{reservasi_id}', 'ScreeningController@api_store');
  Route::get('/reservasi/find/{kode}', 'ReservasiController@api_findByCode');
  Route::get('/reservasi-batal/no-login/{reservasi_id}', 'ReservasiController@api_batal');
  // END RESERVASI NO LOGIN
});

Route::group(['middleware' => ['auth:api'],'prefix' => '/'], function () {
  Route::get('/list-pasien', 'CalonPasienController@api_index');
  Route::post('/pasien/tambah', 'CalonPasienController@api_store');
  Route::post('/pasien/ubah/{pasien_id}', 'CalonPasienController@api_update');
  Route::get('/pasien/hapus/{pasien_id}', 'CalonPasienController@api_destroy');


  Route::get('/reservasi/{poli_id}', 'ReservasiController@api_tanggal');
  // Route::get('/reservasi/{tanggal}', 'JamSesiController@api_jam');
  // Route::get('/reservasi/{tanggal}/{jam}', 'JamSesiController@api_sesi');
  Route::post('/reservasi/store/{pasien_id}', 'ReservasiController@api_store');

  Route::post('/screening/tambah/{reservasi_id}', 'ScreeningController@api_store');
  Route::post('/screening-umum', 'ScreeningController@api_storeUmum');

  Route::get('/reservasi-list', 'ReservasiController@api_list');
  Route::get('/reservasi-detail/{reservasi_id}', 'ReservasiController@api_detail');
  Route::get('/reservasi-batal/{reservasi_id}', 'ReservasiController@api_batal');

  Route::get('/logout', 'UserController@api_logout');
});
