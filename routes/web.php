<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::group(['middleware' => ['auth','role:Super Admin|Dokter|Layanan Reservasi|Calon Pasien']], function () {
      Route::resource('user', 'UserController')->except('destroy');
      Route::put('user/profile/{id}', 'UserController@updateProfil')->name('user.update-profil');
      Route::put('user/profile/password/{id}', 'UserController@updatePassword')->name('user.update-password');
    });
    Route::group(['middleware' => ['auth','role:Super Admin|Dokter|Layanan Reservasi']], function () {
        Route::resource('dashboard', 'DashboardController');
        Route::resource('pertanyaan', 'PertanyaanController')->except('destroy');
        Route::resource('monitoring', 'MonitoringController')->except('destroy');
        Route::resource('skor', 'HasilSkorController')->except('destroy');
        Route::resource('jamsesi', 'JamSesiController')->except('destroy');
        Route::resource('reservasiadmin', 'ReservasiAdminController')->except('destroy');
        Route::resource('poli', 'PoliController')->except('destroy');


        Route::get('polihari/{poli_id}', 'PoliHariController@index')->name('polihari.index');

        Route::get('sesi/{poli_hari_id}', 'SesiController@index')->name('sesi.index');
        Route::get('sesi-create/create', 'SesiController@create')->name('sesi.create');
        Route::post('sesi/store', 'SesiController@store')->name('sesi.store');
        Route::get('sesi/edit/{id}', 'SesiController@edit')->name('sesi.edit');
        Route::put('sesi/update/{id}', 'SesiController@update')->name('sesi.update');

        Route::get('layanan', 'LayananController@index')->name('layanan.index');
        Route::get('layanan/reservasi/{id}', 'LayananController@reservasi')->name('layanan.reservasi');
        Route::get('layanan/ubah-status/{id}', 'LayananController@statusUpdate')->name('layanan.statusUpdate');

        Route::get('layanan/pasien-lama', 'LayananController@pasienLama')->name('layanan.pasienLama');
        Route::get('layanan/pasien-lama/create/{pasien_id}', 'LayananController@pasienLamaCreate')->name('layanan.pasienLamaCreate');
        Route::post('layanan/pasien-lama/store/{pasien_id}', 'LayananController@pasienLamaStore')->name('layanan.pasienLamaStore');

        Route::get('layanan/pasien-baru', 'LayananController@pasienBaru')->name('layanan.pasienBaru');
        Route::post('layanan/pasien-baru/store', 'LayananController@pasienBaruStore')->name('layanan.pasienBaruStore');

        Route::get('layanan/cek/{poli_id}', 'LayananController@poliCek')->name('layanan.poliCek');

        Route::get('panggil-antrian/{antrian}/{kode}', 'ReservasiAdminController@panggilAntrian');
        Route::get('antrian/{tipe}/{antrian}/{kode}', 'ReservasiAdminController@ubahStatusPasien');

        Route::get('laporan', 'LaporanController@index')->name('laporan.index');
        Route::get('laporan-cari', 'LaporanController@cari')->name('laporan.cari');
        Route::get('laporan/export/', 'ReservasiAdminController@export')->name('reservasi.export');

        Route::get('pasien', 'CalonPasienController@index')->name('pasien.index');
        Route::post('pasien/import', 'CalonPasienController@import')->name('pasien.import');
        Route::get('download/template-import', 'CalonPasienController@download')->name('pasien.download-template');
    });
});

Route::group(['middleware' => ['auth','role:Super Admin|Dokter|Layanan Reservasi'], 'prefix' => 'table'], function () {
    Route::get('data-user', 'UserController@getData');
    Route::get('data-pertanyaan', 'PertanyaanController@getData');
    Route::get('data-monitoring', 'MonitoringController@getData');
    Route::get('data-skor', 'HasilSkorController@getData');
    Route::get('data-jamsesi', 'JamSesiController@getData');
    Route::get('data-reservasiadmin', 'ReservasiAdminController@getData');
    Route::get('data-sesi', 'SesiController@getData');
    Route::get('data-poli', 'PoliController@getData');
    Route::get('data-poli-hari', 'PoliHariController@getData');
    Route::get('data-reservasilayanan', 'LayananController@getData');
    Route::get('data-reservasi/pasien-lama', 'LayananController@getDataPasienLama');
    Route::get('data-antrian', 'ReservasiAdminController@getAntrian');
    Route::get('data-pasien', 'CalonPasienController@getData');
});


Route::group(['middleware' => ['auth','role:Super Admin'],'prefix' => 'delete'], function () {
    Route::get('data-user/{id}', 'UserController@destroy')->name('user.destroy');
    Route::get('data-pertanyaan/{id}', 'PertanyaanController@destroy')->name('pertanyaan.destroy');
    Route::get('data-monitoring/{id}', 'MonitoringController@destroy')->name('monitoring.destroy');
    Route::get('data-skor/{id}', 'HasilSkorController@destroy')->name('skor.destroy');
    Route::get('data-jamsesi/{id}', 'JamSesiController@destroy')->name('jamsesi.destroy');
    Route::get('data-sesi/{id}', 'SesiController@destroy')->name('sesi.destroy');
    Route::get('data-poli/{id}', 'PoliController@destroy')->name('poli.destroy');
});

Route::group(['prefix'=>'select2'], function() {
    Route::get('data/{slug}/{id}', 'DaerahController@getData');
    Route::get('data-kabupaten', 'DaerahController@getDataKabupaten');
    Route::get('data-kecamatan', 'DaerahController@getDataKecamatan');
    Route::get('data-kelurahan', 'DaerahController@getDataKelurahan');
});

Route::get('/', function(){
    return view('pasien.login');
});

Route::group(['prefix'=>'/'], function(){
    Auth::routes();
    Route::group(['middleware' => ['auth','role:Calon Pasien|Layanan Reservasi']], function () {
        Route::get('list', 'ReservasiController@listCalonPasien')->name('pasien.list');

        Route::get('informasi-jadwal', 'SesiController@jadwal')->name('jadwal.index');
        Route::get('informasi-jadwal/get-jadwal', 'SesiController@getJadwal')->name('jadwal.getData');

        Route::get('reservasi', 'ReservasiController@index')->name('pasien.reservasi');
        Route::get('reservasi/create', 'ReservasiController@create')->name('reservasi.create');
        Route::post('reservasi/store/{id}', 'ReservasiController@store')->name('reservasi.store');
        Route::get('reservasi/{id}', 'ReservasiController@show')->name('reservasi.show');

        Route::resource('calon-pasien', 'CalonPasienController')->except('destroy');

        Route::put('calon-pasien/isi-data/{id}', 'CalonPasienController@isiDataPut')->name('calon-pasien.isiDataPut');
        Route::get('calon-pasien/isi-data/{id}', 'CalonPasienController@isiData')->name('calon-pasien.isiData');

        Route::get('delete/data-calon-pasien/{id}', 'CalonPasienController@destroy')->name('calon-pasien.destroy');
    });
    Route::get('table/data-calon-pasien', 'ReservasiController@getData');
});

Route::get('pasien/reservasi', 'ReservasiController@reservasiNoLogin')->name('reservasi.noLogin');
Route::post('pasien/reservasi-store', 'ReservasiController@reservasiNoLoginStore')->name('reservasi.reservasiNoLoginStore');
Route::get('pasien/reservasi/{kode}', 'ReservasiController@reservasiNoLoginShow')->name('reservasi.noLoginShow');
Route::get('reservasi/batal/{reservasi_id}', 'ReservasiController@batal')->name('reservasi.batal');
Route::get('pasien/reservasi-unduh/{kode}', 'ReservasiController@reservasiNoLoginUnduh')->name('reservasi.noLoginUnduh');
Route::get('reservasi-kode/{kode}', 'ReservasiController@reservasiNoLoginShow')->name('reservasi.lihat');

Route::get('info-antrian', 'ReservasiAdminController@infoAntrian')->name('infoAntrian');

Route::get('screening/create/{reservasi_id}', 'ScreeningController@create')->name('screening.create');
Route::post('screening/store/{reservasi_id}', 'ScreeningController@store')->name('screening.store');

Route::get('screening', 'ScreeningController@createUmum')->name('screening');
Route::post('screening/store', 'ScreeningController@storeUmum')->name('screeningUmum.store');

Route::get('hasil-screening/{reservasi_id}', 'ReservasiController@api_detail')->name('screening.show');

Route::get('reservasi/cek/{poli}', 'ReservasiController@poli')->name('reservasi.poli');
Route::get('reservasi/cek/{poli}/{tanggal}', 'ReservasiController@tanggal')->name('reservasi.tanggal');
Route::get('reservasi/cek/{poli}/{tanggal}/{jam}', 'ReservasiController@jam')->name('reservasi.jam');

Route::group(['middleware' => ['auth','role:Super Admin|Dokter|Calon Pasien|Layanan Reservasi']], function () {
      Route::get('/home', function(){
        if (Auth::user()->hasAnyRole(['Super Admin','Dokter'])) {
            return redirect()->route('reservasiadmin.index');
        } elseif(Auth::user()->hasRole('Calon Pasien')) {
            return redirect()->route('pasien.list');
        } elseif(Auth::user()->hasRole('Layanan Reservasi')) {
            return redirect()->route('layanan.index');
        } else {
            Auth::logout();
            return redirect()->route('login');
        }
      })->name('home');
});




// Route::get('antrian', function() {
//     return view('antrian');
// });
Route::get('antrian', 'ReservasiAdminController@antrian')->name('antrian');
Route::get('table-antrian', 'ReservasiAdminController@getAntrian')->name('antrian.table');

Route::get('voice-caller','ReservasiAdminController@voiceCaller');

Route::get('event', function(){
    event(new App\Events\InfoAntrianEvent('InfoAntrianEvent'));
});
