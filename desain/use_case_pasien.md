@startuml
left to right direction

actor "Pasien/Reservator" as admin

package "Sistem Reservasi" {
    usecase "Registrasi" as registrasi
    usecase "Kelola Anggota Keluarga" as anggota
    usecase "Membuat Reservasi" as reservasi
    usecase "Histori Reservasi" as histori
    usecase "Informasi Reservasi" as informasi
    usecase "Informasi Jadwal Poli" as jadwal
    usecase "Screening Coivd19" as screening
}
admin --> registrasi
admin --> anggota
admin --> reservasi
admin --> informasi
admin --> screening
admin --> jadwal
admin --> histori
@enduml