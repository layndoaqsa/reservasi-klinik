@startuml
|Pengguna|
start
:Pilih menu reservasi;
|Sistem|
:Menampilkan form reservasi;
|Pengguna|
repeat
    :Isi data reservasi;
    |Sistem|
    repeat while (valid?) is (no)
->yes;
:Menampilkan form screening covid;
|Pengguna|
repeat
    :Isi screening;
    |Sistem|
    repeat while (valid?) is (no)
->yes;
:Menampilkan nomor antrian dan hasil screening;
|Pengguna|
:Download hasil reservasi;
stop
@enduml