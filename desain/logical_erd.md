@startuml
hide circle
left to right direction

entity pasien {
    *id : bigint <<autoincrement>>
    --
    *no_telepon : varchar(20)
    *nama : varchar(50)
    *nik : varchar(16)
    no_bpjs : varchar(50)
}

entity dokter_sesi {
    *id : bigint <<autoincrement>>
    --
    *dokter_id : bigint <<FK>>
    *sesi_id : bigint <<FK>>
}

entity hari {
    *id : bigint <<autoincrement>>
    --
    *nama_hari : varchar(10)
}

entity hasil_screening {
    *id : bigint <<autoincrement>>
    --
    *reservasi_id : bigint <<FK>>
    *hasil_skors_id : bigint <<FK>>
}


entity hasil_skors {
    *id : bigint <<autoincrement>>
    --
    *kategori_hasil: varchar(50)
    hasil : text
    interpretasi : text
    tatalaksana : text
    *batas_bawah : integer
    *batas_atas : integer
    gambar : varchar(100)
}



entity jawaban_pasien {
    *id : bigint <<autoincrement>>
    --
    *reservasi_id : bigint <<FK>>
    *pasien_id : bigint <<FK>>
    *pertanyaan_id : bigint <<FK>>
    *jawaban : varchar (100)
    *skor     : integer
}

entity perrtanyaan {
    *id : bigint <<autoincrement>>
    --
    *kategori : varchar (100)
    *pertnayaan : varchar (100)
    *skor_ya : varchar (100)
    *skor_tidak : varchar (100)
}
entity poli {
    *id : bigint <<autoincrement>>
    --
    *poli : varchar (100)
    *kode : varchar (100)
    *waktu : varchar (100)
    *icon : varchar (100)
}
entity poli_hari {
    *id : bigint <<autoincrement>>
    --
    *poli_id : bigint <<FK>>
    *hari_id : bigint <<FK>>
}
entity reservasi {
    *id : bigint <<autoincrement>>
    --
    *poli_id : bigint <<FK>>
    *pasien_id : bigint <<FK>>
    *antrian : integer
    *perkiraan_pelayanan : timestamp
    *keluhan : varchar(100)
    kekhawatiran : varchar(100)
    riwayat_penyakit_menahun : varchar(100)
    upaya_pengobatan : varchar(100)
    *tanggal : date
    *status : varchar(100)
    counter : varchar(100)
}


entity roles {
    *id : bigint <<autoincrement>>
    --
    *name : varchar(100)
    '--
    '*created_at : timestamp
    'updated_at  : timestamp
}

entity sesi {
    *id : bigint <<autoincrement>>
    --
    *poli_hari_id : bigint <<FK>>
    *sesi : varchar(100)
    *mulai : time
    *selesai : time
    *jumlah_dokter : integer
}

entity users {
    *id : bigint <<autoincrement>>
    --
    *role_id : bigint <<FK>>
    poli_id : bigint <<FK>>
    *email : varchar (100)<<unique>>
    *username : varchar (100)<<unique>>
    *password : varchar (255)
}



dokter_sesi         |o..|{  users
dokter_sesi         |o..|{  sesi
hasil_screening         |o..|{  reservasi
hasil_screening         |o..|{  hasil_skors
jawaban_pasien         |o..|{  reservasi
jawaban_pasien         |o..|{  pasien
poli_hari          |o..|{  poli
poli_hari          |o..|{  hari
reservasi          |o..|{  poli
reservasi          |o..|{  pasien
users          |o..|{  roles
sesi          |o..|{  poli_hari

@enduml