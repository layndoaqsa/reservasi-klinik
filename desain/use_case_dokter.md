@startuml
left to right direction

actor "Dokter" as admin

package "Sistem Reservasi"{
    usecase "Pemanggilan Pasien" as pemanggilan
        usecase "Mengisi data tindak lanjut" as tindak_lanjut
    usecase "Lihat Daftar Pasien" as list_pasien
}
admin --> pemanggilan
admin --> list_pasien
pemanggilan <.. tindak_lanjut : <<include>>
@enduml