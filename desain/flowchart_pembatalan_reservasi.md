@startuml
|Pengguna|
start
:Pilih menu reservasi;
|Sistem|
repeat
:Menampilkan riwayat reservasi;
|Pengguna|
    :Pilih tombol "Batalkan Reservasi";
    |Sistem|
    :Menampilkan persetujuan pembatalan;
    |Pengguna|
    repeat while (pilih?) is (tidak)
->ya;
|Sistem|
:Pembatalan berhasil;
stop
@enduml