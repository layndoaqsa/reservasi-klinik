@startuml
|Pengguna|
start
:Pilih menu tambah anggota;
|Sistem|
:Menampilkan form tambah anggota;
|Pengguna|
repeat
    :Isi data anggota baru;
    |Sistem|
    repeat while (valid?) is (no)
->yes;
:Menampilkan form screening covid;
|Pengguna|
repeat
    :Isi screening;
    |Sistem|
    repeat while (valid?) is (no)
->yes;
:Menampilkan detail antrian dan hasil screening;
|Pengguna|
:Download hasil;
stop
@enduml