@startuml
left to right direction

actor "Admin" as admin

package "Sistem Reservasi"{
    usecase "Mengelola Reservasi" as reservasi
        usecase "Mengelola Status" as status
    usecase "Mengelola Jadwal Poli" as jadwal_poli
    usecase "Mengelola Data Pasien" as pasien
    usecase "Mengelola Data Pengguna Sistem" as pengguna
    usecase "Display Antrian" as display
    usecase "Laporan" as laporan
}
admin --> reservasi
admin --> jadwal_poli
admin --> pasien
admin --> pengguna
admin --> display
admin --> laporan
reservasi <.. status : <<include>>
@enduml