@startuml
left to right direction

actor "Layanan" as admin

package "Sistem Reservasi"{
    usecase "Mengelola Reservasi" as reservasi
        usecase "Mengubah Status Menunggu Pemeriksaan" as status
    usecase "Membuat Reservasi" as reservasi2
    usecase "Mendaftarkan Pasien Baru" as pendaftaran
}
admin --> reservasi
admin --> reservasi2
admin --> pendaftaran
reservasi <.. status : <<include>>
@enduml