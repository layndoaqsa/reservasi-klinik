@startuml
|Layanan Reservasi|
start
:Masuk menu kelola reservasi;
|Sistem|
:Menampilkan data reservasi;
|Layanan Reservasi|
:Pilih reservasi;
:Ubah status reservasi;
|Sistem|
:Menyimpan perubahan status;
stop
@enduml