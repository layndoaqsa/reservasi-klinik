@startuml
|Pengguna|
start
:Masuk aplikasi;
|Sistem|
:Tampil form login;
|Pengguna|
repeat
    :Input nid/email dan password;
    |Sistem|
    repeat while (valid?) is (no)
->yes;
:Tampil halaman dashboard;
stop
@enduml