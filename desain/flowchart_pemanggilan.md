@startuml
|Dokter|
start
:Masuk menu pemangglan;
|Sistem|
:Menampilkan urutan pemanggilan;
|Dokter|
repeat
repeat
:Panggilan pasien ke 1, 2, 3;
    repeat while (pasien datang?) is (tidak)
->ya;
:Melakukan pemeriksaan;
    repeat while (pasien selanjutnya?) is (ada)
->tidak;
stop
@enduml